module.exports = {
  /*
  ** Headers of the page
  */
  router: {
    middleware: 'delay'
  },
  modules: [
    // Or if you have custom bootstrap CSS...
    ['bootstrap-vue/nuxt'],
  ],
  css: [
    '~/assets/styles/main.scss'
  ],
  head: {
    title: 'Kenneth Geris',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Kenneth Geris web/application developer specialized in front-end development with back-end knowledge' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons'},
      { rel: 'stylesheet', href:'https://fonts.googleapis.com/css?family=Roboto'}
    ],
    script:[
      {src:'//code.jquery.com/jquery-3.3.1.min.js'},
      {src: 'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js'},
      {src: '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.js'},
      {src: 'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.js'}
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  // loading: '~/components/Loading.vue', 
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

