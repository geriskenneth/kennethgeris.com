(function (e) {
  function t(t) {
    for (var n, i, o = t[0], l = t[1], c = t[2], d = 0, p = []; d < o.length; d++) i = o[d], s[i] && p.push(s[i][0]), s[i] = 0;
    for (n in l) Object.prototype.hasOwnProperty.call(l, n) && (e[n] = l[n]);
    u && u(t);
    while (p.length) p.shift()();
    return r.push.apply(r, c || []), a()
  }

  function a() {
    for (var e, t = 0; t < r.length; t++) {
      for (var a = r[t], n = !0, o = 1; o < a.length; o++) {
        var l = a[o];
        0 !== s[l] && (n = !1)
      }
      n && (r.splice(t--, 1), e = i(i.s = a[0]))
    }
    return e
  }
  var n = {},
    s = {
      app: 0
    },
    r = [];

  function i(t) {
    if (n[t]) return n[t].exports;
    var a = n[t] = {
      i: t,
      l: !1,
      exports: {}
    };
    return e[t].call(a.exports, a, a.exports, i), a.l = !0, a.exports
  }
  i.m = e, i.c = n, i.d = function (e, t, a) {
    i.o(e, t) || Object.defineProperty(e, t, {
      enumerable: !0,
      get: a
    })
  }, i.r = function (e) {
    "undefined" !== typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
      value: "Module"
    }), Object.defineProperty(e, "__esModule", {
      value: !0
    })
  }, i.t = function (e, t) {
    if (1 & t && (e = i(e)), 8 & t) return e;
    if (4 & t && "object" === typeof e && e && e.__esModule) return e;
    var a = Object.create(null);
    if (i.r(a), Object.defineProperty(a, "default", {
        enumerable: !0,
        value: e
      }), 2 & t && "string" != typeof e)
      for (var n in e) i.d(a, n, function (t) {
        return e[t]
      }.bind(null, n));
    return a
  }, i.n = function (e) {
    var t = e && e.__esModule ? function () {
      return e["default"]
    } : function () {
      return e
    };
    return i.d(t, "a", t), t
  }, i.o = function (e, t) {
    return Object.prototype.hasOwnProperty.call(e, t)
  }, i.p = "/";
  var o = window["webpackJsonp"] = window["webpackJsonp"] || [],
    l = o.push.bind(o);
  o.push = t, o = o.slice();
  for (var c = 0; c < o.length; c++) t(o[c]);
  var u = l;
  r.push([0, "chunk-vendors"]), a()
})({
  0: function (e, t, a) {
    e.exports = a("56d7")
  },
  "00c6": function (e, t, a) {},
  "0510": function (e, t, a) {},
  "05f3": function (e, t, a) {},
  "05f9": function (e, t, a) {},
  "071a": function (e, t, a) {
    "use strict";
    var n = a("a838"),
      s = a.n(n);
    s.a
  },
  "0b3e": function (e, t, a) {},
  "0e49": function (e, t, a) {},
  "12e8": function (e, t, a) {},
  "18c5": function (e, t, a) {
    "use strict";
    var n = a("5b3b"),
      s = a.n(n);
    s.a
  },
  "1a77": function (e, t, a) {},
  "1fd6": function (e, t, a) {},
  "228e": function (e, t, a) {
    "use strict";
    var n = a("9d60"),
      s = a.n(n);
    s.a
  },
  "234d": function (e, t, a) {},
  2470: function (e, t, a) {},
  2473: function (e, t, a) {
    "use strict";
    var n = a("e2d9"),
      s = a.n(n);
    s.a
  },
  2733: function (e, t, a) {
    "use strict";
    var n = a("f868"),
      s = a.n(n);
    s.a
  },
  "2a04": function (e, t, a) {
    "use strict";
    var n = a("c227"),
      s = a.n(n);
    s.a
  },
  "2fa6": function (e, t, a) {
    "use strict";
    var n = a("eefd"),
      s = a.n(n);
    s.a
  },
  "303c": function (e, t, a) {},
  "310c": function (e) {
    e.exports = {
      black: "#1d1d1b",
      gray: "#686867",
      purple: "#8e224f",
      violet: "#00183a",
      teal: "#007385",
      orange: "#e5cba5",
      salmon: "#e19f9a",
      azure: "#77c1d0",
      green: "#b8d4cc",
      yellow: "#edda3a"
    }
  },
  "37af": function (e, t, a) {},
  "38f4": function (e, t, a) {
    "use strict";
    var n = a("05f9"),
      s = a.n(n);
    s.a
  },
  "430d": function (e, t, a) {
    "use strict";
    var n = a("f6e5"),
      s = a.n(n);
    s.a
  },
  "4d31": function (e, t, a) {
    "use strict";
    var n = a("c68d"),
      s = a.n(n);
    s.a
  },
  "4e01": function (e, t, a) {},
  "50d9": function (e, t, a) {
    "use strict";
    var n = a("cf35"),
      s = a.n(n);
    s.a
  },
  5160: function (e, t, a) {
    "use strict";
    var n = a("99a5"),
      s = a.n(n);
    s.a
  },
  "56d7": function (e, t, a) {
    "use strict";
    a.r(t);
    a("cadf"), a("551c"), a("097d");
    var n = a("2b0e"),
      s = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          attrs: {
            id: "app"
          }
        }, [a("div", {
          staticClass: "view-container"
        }, [a("transition", {
          attrs: {
            css: !1,
            appear: "",
            mode: "out-in"
          },
          on: {
            leave: e.routerLeave,
            enter: e.routerEnter
          }
        }, [a("router-view", {
          key: e.$route.fullPath,
          ref: "view"
        })], 1)], 1), a("SideHeadline"), a("transition", {
          attrs: {
            css: !1,
            mode: "out-in"
          },
          on: {
            leave: e.filterLeave
          }
        }, [a("PortfolioFilters", {
          directives: [{
            name: "show",
            rawName: "v-show",
            value: "Portfolio" === this.$route.name,
            expression: "this.$route.name === 'Portfolio'"
          }]
        })], 1), a("TopHeader"), a("Loader"), a("LandscapeError"), a("CookieLaw", {
          attrs: {
            transitionName: "fade"
          }
        }, [a("div", {
          staticClass: "law-message",
          attrs: {
            slot: "message"
          },
          slot: "message"
        }, [a("span", {
          domProps: {
            innerHTML: e._s(e.$translate("COOKIE_MESSAGE"))
          }
        })])]), a("svg", {
          attrs: {
            id: "mouse-dot"
          }
        }, [a("circle", {
          ref: "mouseDot",
          staticClass: "mouse-dot",
          attrs: {
            cx: "0",
            cy: "0",
            r: e.mouse.lerpedRadius.toFixed(2)
          }
        })])], 1)
      },
      r = [],
      i = (a("ac6a"), a("5df3"), a("7f7f"), a("7a32")),
      o = a.n(i),
      l = a("cffa"),
      c = a("fa78"),
      u = a.n(c),
      d = a("0342"),
      p = a.n(d),
      h = a("2f62"),
      m = {
        lang: "it",
        pageNegative: !1,
        navigationAllowed: !0,
        header: {
          negative: !1,
          filled: !1,
          hidden: !1
        },
        hasLoader: !0,
        isInTransition: !1,
        isScrolling: !1,
        isInHeader: !1,
        isMobileMenuOpen: !1,
        isLoaderMounted: !1,
        headline: {
          content: "",
          firstPaint: !0
        },
        currentPageId: null,
        currentPageChildId: null,
        pages: {},
        pagesMap: {},
        settings: {}
      },
      f = m,
      v = {
        SET_LANG: function (e, t) {
          e.lang = t
        },
        SET_NAVIGATION_ALLOWED: function (e, t) {
          e.navigationAllowed = t
        },
        SET_PAGE_NEGATIVE: function (e, t) {
          e.pageNegative = t
        },
        SET_HEADER_NEGATIVE: function (e, t) {
          e.header.negative = t
        },
        SET_HEADER_FILLED: function (e, t) {
          e.header.filled = t
        },
        SET_HEADER_HIDDEN: function (e, t) {
          e.header.hidden = t
        },
        SET_IS_MOBILE_MENU_OPEN: function (e, t) {
          e.isMobileMenuOpen = t
        },
        SET_HEADLINE: function (e, t) {
          e.headline.content = t, e.headline.firstPaint = !0
        },
        REMOVE_HEADLINE_FIRST_PAINT: function (e) {
          e.headline.firstPaint = !1
        },
        SET_HAS_LOADER: function (e, t) {
          e.hasLoader = t
        },
        SET_IS_IN_TRANSITION: function (e, t) {
          e.isInTransition = t
        },
        SET_IS_SCROLLING: function (e, t) {
          e.isScrolling = t
        },
        SET_CURRENT_PAGE_ID: function (e, t) {
          e.currentPageId = t
        },
        SET_CURRENT_PAGE_CHILD_ID: function (e, t) {
          e.currentPageChildId = t
        },
        STORE_PAGES_MAP: function (e, t) {
          e.pagesMap = t
        },
        STORE_SETTINGS: function (e, t) {
          e.settings = t
        },
        STORE_PAGE: function (e, t) {
          e.pages[t.id] = t.page
        },
        SET_IS_IN_HEADER: function (e, t) {
          e.isInHeader = t
        },
        SET_LOADER_MOUNTED: function (e) {
          e.isLoaderMounted = !0
        }
      },
      _ = v,
      g = a("8468"),
      b = (a("456d"), a("96cf"), a("1da1")),
      y = (a("6d93"), a("59ca")),
      S = a.n(y),
      C = (a("66ce"), {
        apiKey: "AIzaSyCzL2uVGCDR3DkyltLDtNJQ024Kqgxwgas",
        authDomain: "pelizzari-studio.firebaseapp.com",
        databaseURL: "https://pelizzari-studio.firebaseio.com",
        projectId: "pelizzari-studio",
        storageBucket: "pelizzari-studio.appspot.com",
        messagingSenderId: "438070367598"
      }),
      E = C,
      O = {
        database: null,
        init: function () {
          S.a.initializeApp(E), O.database = S.a.database()
        },
        getPagesMap: function () {
          var e = Object(b["a"])(regeneratorRuntime.mark(function e() {
            var t;
            return regeneratorRuntime.wrap(function (e) {
              while (1) switch (e.prev = e.next) {
                case 0:
                  return e.next = 2, O.database.ref("/db_root/content/pagesMap").once("value");
                case 2:
                  return t = e.sent, e.abrupt("return", t.val());
                case 4:
                case "end":
                  return e.stop()
              }
            }, e, this)
          }));

          function t() {
            return e.apply(this, arguments)
          }
          return t
        }(),
        getSettings: function () {
          var e = Object(b["a"])(regeneratorRuntime.mark(function e() {
            var t;
            return regeneratorRuntime.wrap(function (e) {
              while (1) switch (e.prev = e.next) {
                case 0:
                  return e.next = 2, O.database.ref("/db_root/settings").once("value");
                case 2:
                  return t = e.sent, e.abrupt("return", t.val());
                case 4:
                case "end":
                  return e.stop()
              }
            }, e, this)
          }));

          function t() {
            return e.apply(this, arguments)
          }
          return t
        }(),
        getPage: function () {
          var e = Object(b["a"])(regeneratorRuntime.mark(function e(t) {
            var a;
            return regeneratorRuntime.wrap(function (e) {
              while (1) switch (e.prev = e.next) {
                case 0:
                  return e.next = 2, O.database.ref("/db_root/content/pages/".concat(t)).once("value");
                case 2:
                  return a = e.sent, e.abrupt("return", a.val());
                case 4:
                case "end":
                  return e.stop()
              }
            }, e, this)
          }));

          function t(t) {
            return e.apply(this, arguments)
          }
          return t
        }()
      },
      A = O,
      I = {
        LOAD_INITIAL_DATA: function () {
          var e = Object(b["a"])(regeneratorRuntime.mark(function e(t) {
            var a, n;
            return regeneratorRuntime.wrap(function (e) {
              while (1) switch (e.prev = e.next) {
                case 0:
                  if (!(Object.keys(t.state.pagesMap).length > 0)) {
                    e.next = 2;
                    break
                  }
                  return e.abrupt("return");
                case 2:
                  if (!(Object.keys(t.state.settings).length > 0)) {
                    e.next = 4;
                    break
                  }
                  return e.abrupt("return");
                case 4:
                  return e.next = 6, A.getPagesMap();
                case 6:
                  return a = e.sent, t.commit("STORE_PAGES_MAP", a), e.next = 10, A.getSettings();
                case 10:
                  n = e.sent, t.commit("STORE_SETTINGS", n);
                case 12:
                case "end":
                  return e.stop()
              }
            }, e, this)
          }));

          function t(t) {
            return e.apply(this, arguments)
          }
          return t
        }(),
        LOAD_PAGE: function () {
          var e = Object(b["a"])(regeneratorRuntime.mark(function e(t) {
            var a, n;
            return regeneratorRuntime.wrap(function (e) {
              while (1) switch (e.prev = e.next) {
                case 0:
                  if (a = t.state.currentPageId, n = t.getters.getPage(a), "undefined" !== typeof n) {
                    e.next = 7;
                    break
                  }
                  return e.next = 5, A.getPage(a);
                case 5:
                  n = e.sent, t.commit("STORE_PAGE", {
                    page: n,
                    id: a
                  });
                case 7:
                  return e.abrupt("return", n);
                case 8:
                case "end":
                  return e.stop()
              }
            }, e, this)
          }));

          function t(t) {
            return e.apply(this, arguments)
          }
          return t
        }(),
        SEND_FORM: function () {
          var e = Object(b["a"])(regeneratorRuntime.mark(function e(t, a) {
            return regeneratorRuntime.wrap(function (e) {
              while (1) switch (e.prev = e.next) {
                case 0:
                  return Object(g["a"])(t), e.next = 3, fetch("https://pelizzari.com/form/send.php", {
                    method: "POST",
                    body: a.form
                  }).then(function (e) {
                    return a.vue.$el.classList.remove("avoid-interactions"), e.json()
                  }).then(function (e) {
                    1 === e.response && a.vue.$eventHub.$emit("form-sended")
                  });
                case 3:
                case "end":
                  return e.stop()
              }
            }, e, this)
          }));

          function t(t, a) {
            return e.apply(this, arguments)
          }
          return t
        }(),
        SEND_NEWSLETTER: function () {
          var e = Object(b["a"])(regeneratorRuntime.mark(function e(t, a) {
            return regeneratorRuntime.wrap(function (e) {
              while (1) switch (e.prev = e.next) {
                case 0:
                  return Object(g["a"])(t), e.next = 3, fetch("https://pelizzari.com/form/join.php", {
                    method: "POST",
                    body: a.form
                  }).then(function (e) {
                    return a.vue.$el.querySelector("form").classList.remove("avoid-interactions"), e.json()
                  }).then(function (e) {
                    1 === e.response && a.vue.$eventHub.$emit("newsletter-sended")
                  });
                case 3:
                case "end":
                  return e.stop()
              }
            }, e, this)
          }));

          function t(t, a) {
            return e.apply(this, arguments)
          }
          return t
        }()
      },
      w = I,
      x = (a("7514"), {
        getPage: function (e) {
          return function (t) {
            return e.pages[t]
          }
        },
        getPageDetails: function (e) {
          return function (t) {
            var a = Object.keys(e.pagesMap).find(function (a) {
              var n = e.pagesMap[a];
              return n.paths[e.lang] === "/".concat(t.page)
            });
            if ("undefined" === typeof t.page && (a = "home"), "undefined" === typeof t.project) return {
              parentId: a,
              childId: null
            };
            var n = Object.keys(e.pagesMap[a].childrens).find(function (n) {
              var s = e.pagesMap[a].childrens[n];
              return s.paths[e.lang] === "/".concat(t.project)
            });
            return {
              parentId: a,
              childId: n
            }
          }
        },
        getComponents: function (e) {
          return function (t) {
            var a = e.pages[e.currentPageId].structure[e.lang],
              n = [];
            return Object.keys(a).forEach(function (e) {
              a[e].component === t && n.push(a[e].data)
            }), n
          }
        },
        getFirstLevelPageUrl: function (e) {
          return function (t) {
            var a = e.pagesMap[t];
            return "undefined" === typeof a ? "#" : "it" === e.lang ? a.paths.it : "en" === e.lang ? a.paths.en : "ru" === e.lang ? a.paths.ru : "#"
          }
        }
      }),
      T = x;
    n["a"].use(h["a"]);
    var $ = new h["a"].Store({
        state: f,
        mutations: _,
        actions: w,
        getters: T
      }),
      L = $,
      k = a("ebcf"),
      P = a.n(k),
      D = new P.a,
      R = D,
      N = (a("f400"), function (e, t, a) {
        var n = a.trappedElement.querySelector(".dot").getBoundingClientRect(),
          s = {
            x: n.left + n.width / 2,
            y: n.top + n.height / 2
          };
        l["e"].killTweensOf(e), l["e"].to(a, .8, {
          lerpedX: s.x,
          lerpedY: s.y,
          lerpedRadius: 15,
          ease: l["b"].easeOut
        }), l["e"].to(e, .8, {
          fill: "rgba(0, 0, 0, 0)",
          stroke: "white",
          ease: l["b"].easeOut
        }), l["e"].set(e, {
          x: a.lerpedX,
          y: a.lerpedY
        })
      }),
      q = N,
      M = function (e, t, a) {
        var n = a.trappedElement.querySelector("a").getBoundingClientRect(),
          s = {
            x: n.left - 10,
            y: n.bottom - 5
          };
        l["e"].killTweensOf(e), l["e"].to(a, .8, {
          lerpedX: s.x,
          lerpedY: s.y,
          lerpedRadius: 1,
          ease: l["b"].easeOut
        }), l["e"].set(e, {
          x: a.lerpedX,
          y: a.lerpedY
        })
      },
      j = M,
      H = function (e, t, a) {
        var n = a.trappedElement,
          s = n.getBoundingClientRect(),
          r = {
            x: n.classList.contains("prev") ? s.left : s.right,
            y: s.top + s.height / 2
          };
        l["e"].to(a, .8, {
          lerpedX: r.x,
          lerpedY: r.y,
          lerpedRadius: 30,
          ease: l["b"].easeOut
        }), l["e"].to(e, .8, {
          fill: "rgba(0, 0, 0, 0)",
          stroke: L.state.pageNegative ? "white" : "#1E1E1E",
          ease: l["b"].easeOut
        }), l["e"].set(e, {
          x: a.lerpedX,
          y: a.lerpedY
        })
      },
      F = H,
      B = function (e, t, a) {
        var n = a.trappedElement,
          s = n.querySelector(".info__circle").getBoundingClientRect(),
          r = {
            x: s.left + s.width / 2,
            y: s.top + s.height / 2
          };
        l["e"].to(a, .8, {
          lerpedX: r.x,
          lerpedY: r.y,
          lerpedRadius: 22,
          ease: l["b"].easeOut
        }), l["e"].to(e, .8, {
          fill: "rgba(0, 0, 0, 0)",
          stroke: L.state.pageNegative ? "white" : "#1E1E1E",
          ease: l["b"].easeOut
        }), l["e"].set(e, {
          x: a.lerpedX,
          y: a.lerpedY
        })
      },
      z = B,
      G = function (e, t, a) {
        var n = a.trappedElement.getBoundingClientRect(),
          s = {
            x: n.left - 10,
            y: n.height + n.top - 5
          };
        l["e"].to(a, .8, {
          lerpedX: s.x,
          lerpedY: s.y,
          lerpedRadius: 1,
          ease: l["b"].easeOut
        }), l["e"].set(e, {
          x: a.lerpedX,
          y: a.lerpedY
        })
      },
      Y = G,
      V = function (e, t, a) {
        var n = a.trappedElement.querySelector(".press__separator").getBoundingClientRect(),
          s = {
            x: n.left + n.width / 2,
            y: n.top + n.height - 5
          };
        l["e"].to(a, .8, {
          lerpedX: s.x,
          lerpedY: s.y,
          lerpedRadius: 2.5,
          ease: l["b"].easeOut
        }), l["e"].set(e, {
          x: a.lerpedX,
          y: a.lerpedY
        })
      },
      U = V,
      X = function (e, t, a) {
        var n = a.trappedElement.querySelector(".next-project__separator").getBoundingClientRect(),
          s = {
            x: n.left + n.width / 2,
            y: n.top + n.height - 3
          };
        l["e"].killTweensOf(e), l["e"].to(a, .8, {
          lerpedX: s.x,
          lerpedY: s.y,
          lerpedRadius: 1,
          ease: l["b"].easeOut
        }), l["e"].set(e, {
          x: a.lerpedX,
          y: a.lerpedY
        })
      },
      W = X,
      Z = function (e, t, a) {
        var n = a.trappedElement.getBoundingClientRect(),
          s = {
            x: n.right + 2.5,
            y: n.height + n.top
          };
        l["e"].to(a, .8, {
          lerpedX: s.x,
          lerpedY: s.y,
          lerpedRadius: 2.5,
          ease: l["b"].easeOut
        }), l["e"].set(e, {
          x: a.lerpedX,
          y: a.lerpedY
        })
      },
      Q = Z,
      K = new Map;
    K.set("featured-dot", q), K.set("menu-link", j), K.set("arrow", F), K.set("info", z), K.set("filter-label", Y), K.set("press-list", U), K.set("next-project", W), K.set("logo", Q);
    var J = K,
      ee = (a("6b54"), a("28a5"), a("a481"), a("8c4f")),
      te = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "view home",
          attrs: {
            "data-transition": "home"
          }
        }, [a("div", {
          staticClass: "home__filler"
        }), e.sliderData ? a("FeaturedSlider", {
          attrs: {
            data: e.sliderData
          }
        }) : e._e()], 1)
      },
      ae = [],
      ne = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component featured-slider"
        }, [a("div", {
          staticClass: "container"
        }, [a("router-link", {
          attrs: {
            to: e.slides[e.currentSlide].link || "#"
          }
        }, [a("div", {
          staticClass: "image",
          attrs: {
            "data-src": e.slides[e.currentSlide].pictureUrl
          },
          on: {
            mouseenter: e.createRipple
          }
        }, [a("transition", {
          attrs: {
            mode: "in-out",
            css: !1
          },
          on: {
            enter: e.imageEnter,
            leave: e.imageLeave
          }
        }, [a("div", {
          key: e.currentSlide,
          staticClass: "image__actual",
          style: {
            backgroundImage: "url('" + e.slides[e.currentSlide].pictureUrl + "')"
          }
        })]), a("div", {
          staticClass: "image__black"
        }), a("div", {
          staticClass: "image__masks"
        }, [a("div", {
          staticClass: "image__mask"
        }), a("div", {
          staticClass: "image__mask"
        })])], 1)]), a("div", {
          staticClass: "sign"
        }, [a("svg", {
          staticStyle: {
            "enable-background": "new 0 0 276.9 150.4"
          },
          attrs: {
            viewBox: "0 0 276.9 150.4",
            "xml:space": "preserve"
          }
        }, [a("path", {
          staticClass: "sign__name",
          attrs: {
            fill: "none",
            stroke: "#FFFFFF",
            "stroke-width": "5px",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            d: "M243.5,17.5c0,0-23-20.8-79.8-13.5c-57.1,7.3-108.1,39.6-138,66.1s-42,75.3,33.9,67.2s130.1-38.6,130.1-38.6 l-8.9,49.1c0,0,16.9-33.5,27.6-43.3c0,0,5.4-5.8,10.4-7.4c0,0,6.2-2.2,7.6,2.9c1.5,5.8-10.1,6.8-10.1,6.8"
          }
        }), a("path", {
          staticClass: "sign__end",
          attrs: {
            fill: "none",
            stroke: "#FFFFFF",
            "stroke-width": "5px",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            d: "M268.5,110.3l6-1L268.5,110.3z"
          }
        })])]), a("div", {
          staticClass: "counter"
        }, [a("div", {
          staticClass: "counter__current"
        }, [a("span", [e._v("0")]), a("transition", {
          attrs: {
            mode: "in-out",
            css: !1
          },
          on: {
            enter: e.counterEnter,
            leave: e.counterLeave
          }
        }, [a("span", {
          key: e.currentSlide
        }, [e._v(e._s(e.currentSlide + 1))])])], 1), a("div", {
          staticClass: "counter__total"
        }, [a("span", [e._v("0")]), a("span", [e._v(e._s(Object.keys(e.slides).length))])])]), a("div", {
          staticClass: "info-wrapper"
        }, [a("div", {
          staticClass: "info",
          attrs: {
            "data-trapped-mode": "info"
          },
          on: {
            click: e.toggleInfo,
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t), e.hoverInfo()
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t), e.outInfo()
            }
          }
        }, [e._m(0), a("div", {
          staticClass: "info__label"
        }, [a("span", [a("span", {
          staticClass: "hide-mobile"
        }, [a("a", {
          staticClass: "info__lang",
          class: {
            active: "it" === e.$store.state.lang
          },
          attrs: {
            href: "/it"
          }
        }, [e._v("It")]), e._v(" . "), a("a", {
          staticClass: "info__lang",
          class: {
            active: "en" === e.$store.state.lang
          },
          attrs: {
            href: "/en"
          }
        }, [e._v("En")]), e._v(" ."), a("span", {
          staticClass: "hide-mobile"
        }, [e._v("Â â¤Â ")])]), a("a", {
          staticClass: "info__social hide-mobile",
          attrs: {
            href: "https://www.facebook.com/pelizzaristudio/",
            target: "_blank",
            rel: "noopener"
          }
        }, [e._v("Facebook")]), a("span", {
          staticClass: "hide-mobile"
        }, [e._v("Â â¤Â ")]), a("a", {
          staticClass: "info__social hide-mobile",
          attrs: {
            href: "https://www.instagram.com/pelizzaristudio/",
            target: "_blank",
            rel: "noopener"
          }
        }, [e._v("Instagram")]), a("span", {
          staticClass: "break-mobile"
        }, [e._v("Â â¤Â ")]), e._v("\n            Pelizzari s.r.l."), e._v("\n            P.I. 03664730987\n          ")])]), e._m(1)])]), a("div", {
          staticClass: "controls"
        }, [a("div", {
          staticClass: "control control--prev prev",
          attrs: {
            "data-trapped-mode": "arrow"
          },
          on: {
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            },
            click: e.goPrev
          }
        }, [a("svg", {
          attrs: {
            viewBox: "0 0 130 16.1",
            "xml:space": "preserve"
          }
        }, [a("line", {
          staticClass: "line",
          attrs: {
            fill: "none",
            stroke: "#616161",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "130",
            y1: "8.1",
            x2: "0",
            y2: "8.1"
          }
        }), a("line", {
          staticClass: "arrowLine",
          attrs: {
            fill: "none",
            stroke: "#616161",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "121.9",
            y1: "16.1",
            x2: "130",
            y2: "8.1"
          }
        }), a("line", {
          staticClass: "arrowLine",
          attrs: {
            fill: "none",
            stroke: "#616161",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "130",
            y1: "8.1",
            x2: "121.9",
            y2: "0"
          }
        })])]), a("div", {
          staticClass: "control control--next next",
          attrs: {
            "data-trapped-mode": "arrow"
          },
          on: {
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            },
            click: e.goNext
          }
        }, [a("svg", {
          attrs: {
            viewBox: "0 0 130 16.1",
            "xml:space": "preserve"
          }
        }, [a("line", {
          staticClass: "line",
          attrs: {
            fill: "none",
            stroke: "#616161",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "130",
            y1: "8.1",
            x2: "0",
            y2: "8.1"
          }
        }), a("line", {
          staticClass: "arrowLine",
          attrs: {
            fill: "none",
            stroke: "#616161",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "121.9",
            y1: "16.1",
            x2: "130",
            y2: "8.1"
          }
        }), a("line", {
          staticClass: "arrowLine",
          attrs: {
            fill: "none",
            stroke: "#616161",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "130",
            y1: "8.1",
            x2: "121.9",
            y2: "0"
          }
        })])])]), a("div", {
          staticClass: "content"
        }, [a("router-link", {
          staticClass: "content__title",
          attrs: {
            to: e.slides[e.currentSlide].link || "#"
          }
        }, [a("transition", {
          attrs: {
            mode: "in-out",
            css: !1
          },
          on: {
            enter: e.titleEnter,
            leave: e.titleLeave
          }
        }, [a("h2", {
          key: e.currentSlide,
          domProps: {
            innerHTML: e._s(e.slides[e.currentSlide].title)
          }
        })])], 1), a("div", {
          staticClass: "content__text"
        }, [a("transition", {
          attrs: {
            mode: "in-out",
            css: !1
          },
          on: {
            enter: e.textEnter,
            leave: e.textLeave
          }
        }, [a("div", {
          key: e.currentSlide,
          staticClass: "content__text-container"
        }, [a("p", {
          domProps: {
            innerHTML: e._s(e.slides[e.currentSlide].subtitle)
          }
        })])])], 1), a("div", {
          staticClass: "dots"
        }, e._l(e.slides, function (t, n) {
          return a("div", {
            key: n,
            staticClass: "dot-container",
            class: {
              active: e.currentSlide === n
            },
            attrs: {
              "data-trapped-mode": "featured-dot"
            },
            on: {
              mouseover: function (t) {
                e.$eventHub.$emit("pointerin", t)
              },
              mouseout: function (t) {
                e.$eventHub.$emit("pointerout", t)
              },
              click: function (t) {
                e.slideTo(n)
              }
            }
          }, [a("div", {
            staticClass: "dot"
          })])
        }))], 1)], 1), a("div", {
          staticStyle: {
            display: "none"
          }
        }, e._l(e.slides, function (e, t) {
          return a("img", {
            key: t,
            attrs: {
              src: e.pictureUrl
            }
          })
        }))])
      },
      se = [function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "info__circle"
        }, [a("div", {
          staticClass: "info__icon"
        })])
      }, function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "info__label-over"
        }, [a("span", [e._v("Info")])])
      }],
      re = (a("1c4c"), function (e) {
        var t = Array.from(e.querySelectorAll(".scroll-stagger-line")),
          a = new l["d"]({
            paused: !0
          });
        return a.addLabel("start").staggerTo(t, .3, {
          y: 0,
          scaleY: 0,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeOut
        }, .02), a
      }),
      ie = re,
      oe = function (e) {
        var t = Array.from(e.querySelectorAll("[data-scroll-child]")),
          a = new l["d"]({
            paused: !0
          });
        return a.addLabel("start").staggerTo(t, 1, {
          y: 0,
          scaleY: 1,
          autoAlpha: 1,
          transformOrigin: "50% 0%",
          ease: l["a"].easeOut
        }, .1), a
      },
      le = oe,
      ce = function (e) {
        var t = Array.from(e.querySelectorAll("[data-scroll-child]")),
          a = new l["d"]({
            paused: !0
          });
        return a.addLabel("start").staggerTo(t, 1, {
          y: 0,
          scaleY: 1,
          autoAlpha: 1,
          transformOrigin: "50% 0%",
          ease: l["a"].easeOut
        }, .3), a
      },
      ue = ce,
      de = function (e) {
        var t = e.querySelector(".slider-container"),
          a = e.querySelector(".slider-arrows"),
          n = e.querySelector(".slider-counter"),
          s = new l["d"]({
            paused: !0
          });
        return s.addLabel("start").to(t, 1, {
          y: 0,
          scaleY: 1,
          autoAlpha: 1,
          transformOrigin: "50% 0%",
          ease: l["a"].easeOut
        }, "start").to(a, 1, {
          x: 0,
          autoAlpha: 1,
          ease: l["a"].easeOut
        }, "start+=0.3").to(n, 1, {
          y: 0,
          autoAlpha: 1,
          ease: l["a"].easeOut
        }, "start+=0.6"), s
      },
      pe = de,
      he = function (e) {
        var t = Array.from(e.querySelectorAll("[data-scroll-child]")),
          a = e.querySelector(".sign__name"),
          n = e.querySelector(".sign__end"),
          s = new l["d"]({
            paused: !1
          });
        return s.addLabel("start").set(a, {
          strokeDashoffset: a.getTotalLength(),
          strokeDasharray: a.getTotalLength()
        }).set(n, {
          strokeDashoffset: 1 + Math.ceil(n.getTotalLength()),
          strokeDasharray: 1 + Math.ceil(n.getTotalLength())
        }).staggerTo(t, 1, {
          y: 0,
          scaleY: 1,
          autoAlpha: 1,
          transformOrigin: "50% 0%",
          ease: l["a"].easeOut
        }, .3).to(a, .8, {
          strokeDashoffset: 0,
          ease: l["a"].easeInOut
        }, "start+=0.6").to(n, .8, {
          strokeDashoffset: 0,
          ease: l["a"].easeOut
        }, "-=0.4"), s
      },
      me = he,
      fe = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = Array.from(e.querySelectorAll(".scroll-stagger-line")),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel("start").staggerTo(a, .3, {
          y: 20,
          scaleY: 1.4,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeIn
        }, .02), n
      },
      ve = fe,
      _e = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = Array.from(e.querySelectorAll("[data-scroll-child].in-viewport")).reverse(),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel("start").staggerTo(a, 1, {
          y: 80,
          scaleY: 1.4,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeIn
        }, .1), n
      },
      ge = _e,
      be = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = Array.from(e.querySelectorAll("[data-scroll-child]")),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel("start").staggerTo(a, 1, {
          y: 80,
          scaleY: 1.4,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeIn
        }, .3), n
      },
      ye = be,
      Se = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = e.querySelector(".slider-container"),
          n = e.querySelector(".slider-arrows"),
          s = e.querySelector(".slider-counter"),
          r = new l["d"]({
            paused: t
          });
        return r.addLabel("start").to(a, 1, {
          y: 80,
          scaleY: 1.4,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeIn
        }, "start").to(n, 1, {
          x: 20,
          autoAlpha: 0,
          ease: l["a"].easeIn
        }, "start+=0.3").to(s, 1, {
          y: 20,
          autoAlpha: 0,
          ease: l["a"].easeIn
        }, "start+=0.6"), r
      },
      Ce = Se,
      Ee = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = Array.from(e.querySelectorAll("[data-scroll-child]:not(.sign)")).reverse(),
          n = e.querySelector(".sign"),
          s = new l["d"]({
            paused: t
          });
        return s.addLabel("start").to(n, 1, {
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start").staggerTo(a, 1, {
          y: 80,
          scaleY: 1.4,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeIn
        }, .3, "start"), s
      },
      Oe = Ee,
      Ae = function (e) {
        var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
          a = new l["d"]({
            paused: !1
          });
        return a.addLabel("start").set(e, {
          yPercent: t ? 0 : -100,
          width: t ? 0 : "auto"
        }).to(e, 1, {
          yPercent: t ? 100 : 0,
          ease: l["a"].easeInOut
        }), a
      },
      Ie = Ae,
      we = a("2e20");
    /*!
     * VERSION: 0.6.1
     * DATE: 2018-08-27
     * UPDATES AND DOCS AT: http://greensock.com
     *
     * @license Copyright (c) 2008-2018, GreenSock. All rights reserved.
     * SplitText is a Club GreenSock membership benefit; You must have a valid membership to use
     * this code without violating the terms of use. Visit http://greensock.com/club/ to sign up or get more details.
     * This work is subject to the software agreement that was issued with your membership.
     * 
     * @author: Jack Doyle, jack@greensock.com
     */
    (function (e) {
      var t = e.GreenSockGlobals || e,
        a = function (e) {
          var a, n = e.split("."),
            s = t;
          for (a = 0; a < n.length; a++) s[n[a]] = s = s[n[a]] || {};
          return s
        },
        n = a("com.greensock.utils"),
        s = function e(t) {
          var a = t.nodeType,
            n = "";
          if (1 === a || 9 === a || 11 === a) {
            if ("string" === typeof t.textContent) return t.textContent;
            for (t = t.firstChild; t; t = t.nextSibling) n += e(t)
          } else if (3 === a || 4 === a) return t.nodeValue;
          return n
        },
        r = document,
        i = r.defaultView ? r.defaultView.getComputedStyle : function () {},
        o = /([A-Z])/g,
        l = function (e, t, a, n) {
          var s;
          return (a = a || i(e, null)) ? (e = a.getPropertyValue(t.replace(o, "-$1").toLowerCase()), s = e || a.length ? e : a[t]) : e.currentStyle && (a = e.currentStyle, s = a[t]), n ? s : parseInt(s, 10) || 0
        },
        c = function (e) {
          return !!(e.length && e[0] && (e[0].nodeType && e[0].style && !e.nodeType || e[0].length && e[0][0]))
        },
        u = function (e) {
          var t, a, n, s = [],
            r = e.length;
          for (t = 0; t < r; t++)
            if (a = e[t], c(a))
              for (n = a.length, n = 0; n < a.length; n++) s.push(a[n]);
            else s.push(a);
          return s
        },
        d = function (e, t) {
          var a, n = t.length;
          while (--n > -1)
            if (a = t[n], e.substr(0, a.length) === a) return a.length
        },
        p = /(?:\r|\n|\t\t)/g,
        h = /(?:\s\s+)/g,
        m = 55296,
        f = 56319,
        v = 56320,
        _ = 127462,
        g = 127487,
        b = 127995,
        y = 127999,
        S = function (e) {
          return (e.charCodeAt(0) - m << 10) + (e.charCodeAt(1) - v) + 65536
        },
        C = r.all && !r.addEventListener,
        E = " style='position:relative;display:inline-block;" + (C ? "*display:inline;*zoom:1;'" : "'"),
        O = function (e, t) {
          e = e || "";
          var a = -1 !== e.indexOf("++"),
            n = 1;
          return a && (e = e.split("++").join("")),
            function () {
              return "<" + t + E + (e ? " class='" + e + (a ? n++ : "") + "'>" : ">")
            }
        },
        A = n.SplitText = t.SplitText = function (e, t) {
          if ("string" === typeof e && (e = A.selector(e)), !e) throw "cannot split a null element.";
          this.elements = c(e) ? u(e) : [e], this.chars = [], this.words = [], this.lines = [], this._originals = [], this.vars = t || {}, this.split(t)
        },
        I = function e(t, a, n) {
          var s = t.nodeType;
          if (1 === s || 9 === s || 11 === s)
            for (t = t.firstChild; t; t = t.nextSibling) e(t, a, n);
          else 3 !== s && 4 !== s || (t.nodeValue = t.nodeValue.split(a).join(n))
        },
        w = function (e, t) {
          var a = t.length;
          while (--a > -1) e.push(t[a])
        },
        x = function (e) {
          var t, a = [],
            n = e.length;
          for (t = 0; t !== n; a.push(e[t++]));
          return a
        },
        T = function (e, t, a) {
          var n;
          while (e && e !== t) {
            if (n = e._next || e.nextSibling, n) return n.textContent.charAt(0) === a;
            e = e.parentNode || e._parent
          }
          return !1
        },
        $ = function e(t) {
          var a, n, s = x(t.childNodes),
            r = s.length;
          for (a = 0; a < r; a++) n = s[a], n._isSplit ? e(n) : (a && 3 === n.previousSibling.nodeType ? n.previousSibling.nodeValue += 3 === n.nodeType ? n.nodeValue : n.firstChild.nodeValue : 3 !== n.nodeType && t.insertBefore(n.firstChild, n), t.removeChild(n))
        },
        L = function (e, t, a, n, s, o, c) {
          var u, d, p, h, m, f, v, _, g, b, y, S, C = i(e),
            E = l(e, "paddingLeft", C),
            O = -999,
            A = l(e, "borderBottomWidth", C) + l(e, "borderTopWidth", C),
            x = l(e, "borderLeftWidth", C) + l(e, "borderRightWidth", C),
            L = l(e, "paddingTop", C) + l(e, "paddingBottom", C),
            k = l(e, "paddingLeft", C) + l(e, "paddingRight", C),
            P = .2 * l(e, "fontSize"),
            D = l(e, "textAlign", C, !0),
            R = [],
            N = [],
            q = [],
            M = t.wordDelimiter || " ",
            j = t.span ? "span" : "div",
            H = t.type || t.split || "chars,words,lines",
            F = s && -1 !== H.indexOf("lines") ? [] : null,
            B = -1 !== H.indexOf("words"),
            z = -1 !== H.indexOf("chars"),
            G = "absolute" === t.position || !0 === t.absolute,
            Y = t.linesClass,
            V = -1 !== (Y || "").indexOf("++"),
            U = [];
          for (V && (Y = Y.split("++").join("")), d = e.getElementsByTagName("*"), p = d.length, m = [], u = 0; u < p; u++) m[u] = d[u];
          if (F || G)
            for (u = 0; u < p; u++) h = m[u], f = h.parentNode === e, (f || G || z && !B) && (S = h.offsetTop, F && f && Math.abs(S - O) > P && ("BR" !== h.nodeName || 0 === u) && (v = [], F.push(v), O = S), G && (h._x = h.offsetLeft, h._y = S, h._w = h.offsetWidth, h._h = h.offsetHeight), F && ((h._isSplit && f || !z && f || B && f || !B && h.parentNode.parentNode === e && !h.parentNode._isSplit) && (v.push(h), h._x -= E, T(h, e, M) && (h._wordEnd = !0)), "BR" === h.nodeName && (h.nextSibling && "BR" === h.nextSibling.nodeName || 0 === u) && F.push([])));
          for (u = 0; u < p; u++) h = m[u], f = h.parentNode === e, "BR" !== h.nodeName ? (G && (g = h.style, B || f || (h._x += h.parentNode._x, h._y += h.parentNode._y), g.left = h._x + "px", g.top = h._y + "px", g.position = "absolute", g.display = "block", g.width = h._w + 1 + "px", g.height = h._h + "px"), !B && z ? h._isSplit ? (h._next = h.nextSibling, h.parentNode.appendChild(h)) : h.parentNode._isSplit ? (h._parent = h.parentNode, !h.previousSibling && h.firstChild && (h.firstChild._isFirst = !0), h.nextSibling && " " === h.nextSibling.textContent && !h.nextSibling.nextSibling && U.push(h.nextSibling), h._next = h.nextSibling && h.nextSibling._isFirst ? null : h.nextSibling, h.parentNode.removeChild(h), m.splice(u--, 1), p--) : f || (S = !h.nextSibling && T(h.parentNode, e, M), h.parentNode._parent && h.parentNode._parent.appendChild(h), S && h.parentNode.appendChild(r.createTextNode(" ")), t.span && (h.style.display = "inline"), R.push(h)) : h.parentNode._isSplit && !h._isSplit && "" !== h.innerHTML ? N.push(h) : z && !h._isSplit && (t.span && (h.style.display = "inline"), R.push(h))) : F || G ? (h.parentNode && h.parentNode.removeChild(h), m.splice(u--, 1), p--) : B || e.appendChild(h);
          u = U.length;
          while (--u > -1) U[u].parentNode.removeChild(U[u]);
          if (F) {
            G && (b = r.createElement(j), e.appendChild(b), y = b.offsetWidth + "px", S = b.offsetParent === e ? 0 : e.offsetLeft, e.removeChild(b)), g = e.style.cssText, e.style.cssText = "display:none;";
            while (e.firstChild) e.removeChild(e.firstChild);
            for (_ = " " === M && (!G || !B && !z), u = 0; u < F.length; u++) {
              for (v = F[u], b = r.createElement(j), b.style.cssText = "display:block;text-align:" + D + ";position:" + (G ? "absolute;" : "relative;"), Y && (b.className = Y + (V ? u + 1 : "")), q.push(b), p = v.length, d = 0; d < p; d++) "BR" !== v[d].nodeName && (h = v[d], b.appendChild(h), _ && h._wordEnd && b.appendChild(r.createTextNode(" ")), G && (0 === d && (b.style.top = h._y + "px", b.style.left = E + S + "px"), h.style.top = "0px", S && (h.style.left = h._x - S + "px")));
              0 === p ? b.innerHTML = "&nbsp;" : B || z || ($(b), I(b, String.fromCharCode(160), " ")), G && (b.style.width = y, b.style.height = h._h + "px"), e.appendChild(b)
            }
            e.style.cssText = g
          }
          G && (c > e.clientHeight && (e.style.height = c - L + "px", e.clientHeight < c && (e.style.height = c + A + "px")), o > e.clientWidth && (e.style.width = o - k + "px", e.clientWidth < o && (e.style.width = o + x + "px"))), w(a, R), w(n, N), w(s, q)
        },
        k = function (e, t, a, n) {
          var i, o, l, c, u, v, C, E, O, A, w = t.span ? "span" : "div",
            x = t.type || t.split || "chars,words,lines",
            T = -1 !== x.indexOf("chars"),
            $ = "absolute" === t.position || !0 === t.absolute,
            L = t.wordDelimiter || " ",
            k = " " !== L ? "" : $ ? "&#173; " : " ",
            P = t.span ? "</span>" : "</div>",
            D = !0,
            R = t.specialChars ? "function" === typeof t.specialChars ? t.specialChars : d : null,
            N = r.createElement("div"),
            q = e.parentNode;
          for (q.insertBefore(N, e), N.textContent = e.nodeValue, q.removeChild(e), e = N, i = s(e), C = -1 !== i.indexOf("<"), !1 !== t.reduceWhiteSpace && (i = i.replace(h, " ").replace(p, "")), C && (i = i.split("<").join("{{LT}}")), u = i.length, o = (" " === i.charAt(0) ? k : "") + a(), l = 0; l < u; l++)
            if (v = i.charAt(l), R && (A = R(i.substr(l), t.specialChars))) v = i.substr(l, A || 1), o += T && " " !== v ? n() + v + "</" + w + ">" : v, l += A - 1;
            else if (v === L && i.charAt(l - 1) !== L && l) {
            o += D ? P : "", D = !1;
            while (i.charAt(l + 1) === L) o += k, l++;
            l === u - 1 ? o += k : ")" !== i.charAt(l + 1) && (o += k + a(), D = !0)
          } else "{" === v && "{{LT}}" === i.substr(l, 6) ? (o += T ? n() + "{{LT}}</" + w + ">" : "{{LT}}", l += 5) : v.charCodeAt(0) >= m && v.charCodeAt(0) <= f || i.charCodeAt(l + 1) >= 65024 && i.charCodeAt(l + 1) <= 65039 ? (E = S(i.substr(l, 2)), O = S(i.substr(l + 2, 2)), c = E >= _ && E <= g && O >= _ && O <= g || O >= b && O <= y ? 4 : 2, o += T && " " !== v ? n() + i.substr(l, c) + "</" + w + ">" : i.substr(l, c), l += c - 1) : o += T && " " !== v ? n() + v + "</" + w + ">" : v;
          e.outerHTML = o + (D ? P : ""), C && I(q, "{{LT}}", "<")
        },
        P = function e(t, a, n, s) {
          var r, i, o = x(t.childNodes),
            c = o.length,
            u = "absolute" === a.position || !0 === a.absolute;
          if (3 !== t.nodeType || c > 1) {
            for (a.absolute = !1, r = 0; r < c; r++) i = o[r], (3 !== i.nodeType || /\S+/.test(i.nodeValue)) && (u && 3 !== i.nodeType && "inline" === l(i, "display", null, !0) && (i.style.display = "inline-block", i.style.position = "relative"), i._isSplit = !0, e(i, a, n, s));
            return a.absolute = u, void(t._isSplit = !0)
          }
          k(t, a, n, s)
        },
        D = A.prototype;
      D.split = function (e) {
        this.isSplit && this.revert(), this.vars = e = e || this.vars, this._originals.length = this.chars.length = this.words.length = this.lines.length = 0;
        var t, a, n, s = this.elements.length,
          r = e.span ? "span" : "div",
          i = O(e.wordsClass, r),
          o = O(e.charsClass, r);
        while (--s > -1) n = this.elements[s], this._originals[s] = n.innerHTML, t = n.clientHeight, a = n.clientWidth, P(n, e, i, o), L(n, e, this.chars, this.words, this.lines, a, t);
        return this.chars.reverse(), this.words.reverse(), this.lines.reverse(), this.isSplit = !0, this
      }, D.revert = function () {
        if (!this._originals) throw "revert() call wasn't scoped properly.";
        var e = this._originals.length;
        while (--e > -1) this.elements[e].innerHTML = this._originals[e];
        return this.chars = [], this.words = [], this.lines = [], this.isSplit = !1, this
      }, A.selector = e.$ || e.jQuery || function (t) {
        var a = e.$ || e.jQuery;
        return a ? (A.selector = a, a(t)) : "undefined" === typeof document ? t : document.querySelectorAll ? document.querySelectorAll(t) : document.getElementById("#" === t.charAt(0) ? t.substr(1) : t)
      }, A.version = "0.6.1"
    })(we["g"]);
    var xe = we["i"].SplitText,
      Te = function (e) {
        var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
        if (!t) new xe(e, {
          linesClass: "content__title-line",
          charsClass: "content__title-letter"
        });
        var a = Array.from(e.querySelectorAll(".content__title-letter")),
          n = new l["d"]({
            paused: !0
          });
        return n.addLabel("start").set(e, {
          autoAlpha: 1
        }).set(a, {
          yPercent: t ? 0 : 100
        }).set(e, {
          height: t ? 0 : "auto",
          minHeight: !!t && 0
        }).staggerTo(a, .7, {
          delay: t ? 0 : 1,
          yPercent: t ? 100 : 0,
          ease: t ? l["a"].easeIn : l["a"].easeOut
        }, .04), n
      },
      $e = Te,
      Le = function (e, t, a) {
        var n = new l["d"]({
          paused: !0
        });
        return n.addLabel("start").set(e, {
          scale: t ? 1 : 1.2,
          clipPath: t ? "none" : "inset(".concat("next" === a ? 100 : .005, "% 0.001% ").concat("next" === a ? .002 : 100, "% 0.003%)"),
          webkitClipPath: t ? "none" : "inset(".concat("next" === a ? 100 : .005, "% 0.001% ").concat("next" === a ? .002 : 100, "% 0.003%)")
        }).to(e, 1, {
          delay: .6,
          scale: t ? 1.2 : 1,
          clipPath: t ? "none" : "inset(0% 0.001% 0.002% 0.003%)",
          webkitClipPath: t ? "none" : "inset(0% 0.001% 0.002% 0.003%)",
          transformOrigin: "next" === a ? t ? "50% 100%" : "50% 0%" : t ? "50% 0%" : "50% 100%",
          ease: l["a"].easeInOut
        }), n
      },
      ke = Le,
      Pe = a("2909"),
      De = function (e) {
        var t = Array.from(e.querySelectorAll(".menu__link")),
          a = Array.from(document.querySelectorAll(".mobile-menu__icon-line")),
          n = Array.from(e.querySelectorAll(".mobile-menu__footer-social")),
          s = Array.from(e.querySelectorAll(".mobile-menu__footer-lang")),
          r = new l["d"]({
            paused: !0
          });
        return r.addLabel("start").set(e, {
          yPercent: -100
        }).set(t, {
          y: -50,
          autoAlpha: 0
        }).set(a, {
          rotation: 0,
          clearProps: "all"
        }).set(Object(Pe["a"])(n).concat(Object(Pe["a"])(s)), {
          autoAlpha: 0
        }).to(e, 1, {
          yPercent: 0,
          ease: l["a"].easeInOut
        }, "start").to(a[0], 1, {
          scaleX: 0,
          transformOrigin: "100% 50%",
          ease: l["a"].easeInOut
        }, "start").to(a[1], 1, {
          scaleX: 0,
          transformOrigin: "0% 50%",
          ease: l["a"].easeInOut
        }, "start").set(a[0], {
          transformOrigin: "0% 50%",
          rotation: 45
        }).set(a[1], {
          transformOrigin: "0% 50%",
          rotation: -45
        }).to(a, 1, {
          scaleX: 1,
          ease: l["a"].easeInOut
        }).staggerTo(t, 1, {
          y: 0,
          autoAlpha: 1,
          transformOrigin: "100% 0%",
          ease: l["a"].easeOut
        }, .02, "start+=1").staggerTo(Object(Pe["a"])(n).concat(Object(Pe["a"])(s)), .6, {
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, .06, "start+=1"), r
      },
      Re = De,
      Ne = function (e, t) {
        var a = new l["d"]({
          paused: !0
        });
        return a.addLabel("start").set(e, {
          scale: 1.4,
          clipPath: "next" === t ? "inset(0% 100% 0.002% 0.003%)" : "inset(0% 0.001% 0.002% 100%)",
          webkitClipPath: "next" === t ? "inset(0% 100% 0.002% 0.003%)" : "inset(0% 0.001% 0.002% 100%)"
        }).to(e, .8, {
          scale: 1,
          clipPath: "0% 0.001% 0.002% 0.004%",
          webkitClipPath: "0% 0.001% 0.002% 0.004%",
          transformOrigin: "next" === t ? "0% 50%" : "100% 50%",
          ease: l["a"].easeInOut
        }), a
      },
      qe = Ne,
      Me = function (e, t) {
        var a = new l["d"]({
          paudes: !0
        });
        return a.addLabel("start").set(e, {
          clipPath: "0% 0.001% 0.003% 0.004%",
          webkitClipPath: "0% 0.001% 0.003% 0.004%"
        }).to(e, .8, {
          clipPath: "next" === t ? "0% 0.002% 0.003% 100%" : "0% 100% 0.001% 0.002%",
          webkitClipPath: "next" === t ? "0% 0.002% 0.003% 100%" : "0% 100% 0.001% 0.002%",
          transformOrigin: "0% 50%",
          ease: l["a"].easeInOut
        }), a
      },
      je = Me,
      He = function (e) {
        var t = e.querySelector(".info__label"),
          a = e.querySelector(".info__label-over"),
          n = e.querySelector(".next-project") || [],
          s = document.querySelector(".dots") || [],
          r = e.querySelector(".info__circle"),
          i = new l["d"]({
            paused: !0
          });
        return i.addLabel("start").set(t, {
          yPercent: -100,
          autoAlpha: 0
        }).to(n, .4, {
          opacity: 0,
          ease: l["a"].easeInOut
        }, "start").to(s, .4, {
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start").set(n, {
          width: 1,
          minWidth: "1px",
          height: 0
        }).to(t, .6, {
          yPercent: 0,
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, "start+=0.4").to(a, .8, {
          yPercent: 100,
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start+=0.4").to(r, .6, {
          rotation: 225,
          ease: l["a"].easeInOut
        }, "start"), i
      },
      Fe = He,
      Be = function (e) {
        var t = e.querySelector(".label__line"),
          a = new l["d"]({
            paused: !0
          });
        return a.addLabel("start").to(t, .4, {
          scaleX: 0,
          transformOrigin: "100% 0%",
          ease: l["a"].easeInOut
        }).to(t, .4, {
          scaleX: 1,
          transformOrigin: "0% 50%",
          ease: l["a"].easeInOut
        }), a
      },
      ze = Be,
      Ge = function (e) {
        var t = e.querySelector(".info__label-over"),
          a = new l["d"]({
            paused: !0
          });
        return a.addLabel("start").to(t, .8, {
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }), a
      },
      Ye = Ge,
      Ve = function (e) {
        var t = e.querySelector(".next-project__name"),
          a = Array.from(e.querySelectorAll(".next-project__name span")),
          n = e.querySelector(".next-project__label"),
          s = e.querySelector(".next-project__separator"),
          r = e.querySelector(".black-mask__filler"),
          i = e.querySelector(".black-mask__inner");
        l["e"].set(a, {
          yPercent: 100,
          autoAlpha: 0
        });
        var o = new l["d"]({
          paused: !0
        });
        return o.addLabel("start").to([n, s], .8, {
          x: function () {
            return -t.getBoundingClientRect().width
          },
          ease: l["a"].easeInOut
        }, "start").to([r, i], .8, {
          height: 60,
          ease: l["a"].easeInOut
        }, "start").staggerTo(a, .6, {
          yPercent: 0,
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, .02, "-=0.4"), o
      },
      Ue = Ve,
      Xe = function (e) {
        var t = e.querySelector(".scroll__line-inner"),
          a = new l["d"]({
            paused: !0,
            repeat: -1
          });
        return a.addLabel("start").to(t, 1.2, {
          scaleX: 1,
          ease: l["a"].easeInOut
        }).to(t, 1.2, {
          xPercent: -100,
          ease: l["a"].easeInOut
        }), a
      },
      We = Xe,
      Ze = function (e) {
        var t = e.querySelector(".scroll__label"),
          a = e.querySelector(".scroll__line"),
          n = new l["d"]({
            paused: !0
          });
        return n.addLabel("start").to(t, 1.2, {
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start").to(a, 1.2, {
          xPercent: -100,
          ease: l["a"].easeInOut
        }, "start"), n
      },
      Qe = Ze,
      Ke = function (e) {
        var t = e.querySelectorAll(".object__label"),
          a = e.querySelector(".object__line"),
          n = new l["d"]({
            paused: !0
          });
        return n.addLabel("start").staggerTo(t, .8, {
          cycle: {
            left: function (e, t) {
              return t.dataset.originalLeft
            }
          },
          yPercent: 0,
          xPercent: 0,
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, 0, "start").to(a, .8, {
          width: "100%",
          ease: l["a"].easeInOut
        }, "start"), n
      },
      Je = Ke,
      et = function (e) {
        var t = Array.from(e.querySelectorAll(".form__column-line")),
          a = Array.from(e.querySelectorAll("label")),
          n = e.querySelector(".object__label"),
          s = e.querySelector(".object__line"),
          r = e.querySelector(".object-mobile__label"),
          i = e.querySelector(".object-mobile__line"),
          o = new l["d"]({
            paused: !0
          });
        return o.addLabel("start").staggerTo([n, r, a], .8, {
          yPercent: 0,
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, .2, "start+=0.2").staggerTo([s, i, t], .8, {
          scaleX: 1,
          ease: l["a"].easeInOut
        }, .2, "start"), o
      },
      tt = et,
      at = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = Array.from(e.querySelectorAll(".form__column-line")),
          n = Array.from(e.querySelectorAll("label")),
          s = e.querySelector(".object__line"),
          r = e.querySelector(".object__label"),
          i = e.querySelector(".object-mobile__line"),
          o = e.querySelector(".object-mobile__label"),
          c = new l["d"]({
            paused: t
          });
        return c.addLabel("start").to([r, o, n], .8, {
          yPercent: 100,
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start").staggerTo([s, i, a], .8, {
          scaleX: 0,
          ease: l["a"].easeInOut
        }, .02, "start"), c
      },
      nt = at,
      st = function (e) {
        var t = e.querySelector(".object__label.firstone"),
          a = e.querySelector(".object__label.selected"),
          n = Array.from(e.querySelectorAll(".object__label:not(.selected):not(.firstone)")),
          s = e.querySelector(".object__line"),
          r = new l["d"]({
            paused: !0
          });
        return r.addLabel("start").to(t, .8, {
          xPercent: -100,
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start").to(a, .8, {
          left: 0,
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, "start").to(s, .8, {
          width: 120,
          ease: l["a"].easeInOut
        }, "start").to(n, .8, {
          yPercent: 100,
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start"), r
      },
      rt = st,
      it = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = e.querySelector(".press__bottom-line"),
          n = e.querySelector(".press__content"),
          s = new l["d"]({
            puased: t
          });
        return s.addLabel("start").to(a, 1.2, {
          scaleX: 1,
          ease: l["a"].easeInOut
        }, "start+=0.4").to(n, 1.2, {
          yPercent: 0,
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, "start"), s
      },
      ot = it,
      lt = function (e, t) {
        var a = e.querySelector(".press__content"),
          n = e.querySelector(".press__bottom-line"),
          s = new l["d"]({
            paused: t
          });
        return s.addLabel("start").to(n, .4, {
          scaleX: 0,
          ease: l["a"].easeInOut
        }, "start").to(a, .4, {
          yPercent: 100,
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start+=0.2"), s
      },
      ct = lt,
      ut = function (e) {
        var t = e.querySelector(".object-mobile__modal"),
          a = Array.from(e.querySelectorAll(".object-mobile__modal-label"));
        l["e"].set(t, {
          scaleY: 0
        }), l["e"].set(a, {
          y: 10,
          autoAlpha: 0
        });
        var n = new l["d"]({
          paused: !0
        });
        return n.addLabel("start").to(t, .4, {
          scaleY: 1,
          ease: l["a"].easeInOut
        }, "start").staggerTo(a, .4, {
          y: 0,
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, .05, "start+=0.2"), n
      },
      dt = ut,
      pt = function (e) {
        var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1],
          a = e.querySelector("p");
        if (!t) new xe(a, {
          linesClass: "content__text-line",
          wordsClass: "content__text-word",
          charsClass: "content__text-char"
        });
        var n = new l["d"]({
            paused: !0
          }),
          s = Array.from(a.querySelectorAll(".content__text-line")),
          r = s.reverse();
        return n.addLabel("start").set(a, {
          height: t ? 0 : "auto"
        }).set(s, {
          yPercent: t ? 0 : -100,
          autoAlpha: t ? 1 : 0
        }).staggerTo(r, .6, {
          delay: t ? 0 : 1,
          yPercent: t ? 100 : 0,
          autoAlpha: t ? 0 : 1,
          ease: t ? l["a"].easeIn : l["a"].easeOut
        }, .2), n
      },
      ht = pt,
      mt = function (e) {
        var t = e.querySelector(".hero__image"),
          a = e.querySelector(".hero__image-mask"),
          n = new l["d"]({
            paused: !0
          });
        return n.addLabel("start").set(t, {
          scale: 1.6,
          y: 60
        }).set(a, {
          yPercent: 0
        }).to(t, 1, {
          scale: 1.2,
          y: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeInOut
        }, "start").to(a, 1, {
          yPercent: -100,
          ease: l["a"].easeInOut
        }, "start"), n
      },
      ft = mt,
      vt = function (e) {
        var t = new l["d"]({
          paused: !0
        });
        return t.addLabel("start").to(e, .8, {
          yPercent: 100,
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }), t
      },
      _t = vt,
      gt = new Map;
    gt.set("text-blocks-in", le), gt.set("text-stagger-lines-in", ie), gt.set("image-stretch-in", ue), gt.set("slideshow-in", pe), gt.set("team-in", me), gt.set("contact-form-in", tt), gt.set("press-element-in", ot), gt.set("text-blocks-out", ge), gt.set("text-stagger-lines-out", ve), gt.set("image-stretch-out", ye), gt.set("slideshow-out", Ce), gt.set("team-out", Oe), gt.set("contact-form-out", nt), gt.set("press-element-out", ct), gt.set("counter-slide", Ie), gt.set("title-slide", $e), gt.set("text-slide", ht), gt.set("image-slide", ke), gt.set("mobile-menu", Re), gt.set("slideshow-slide-in", qe), gt.set("slideshow-slide-out", je), gt.set("reveal-info", Fe), gt.set("hover-info", Ye), gt.set("hover-next-project", Ue), gt.set("request-click", rt), gt.set("underline-in", ze), gt.set("scroll-down-loop", We), gt.set("scroll-down-hide", Qe), gt.set("request-hover", Je), gt.set("mobile-request-open", dt), gt.set("project-hero-loaded", ft), gt.set("filter-enter", _t);
    var bt = gt,
      yt = a("d4ec"),
      St = a("bee2"),
      Ct = a("912c"),
      Et = function () {
        function e(t, a, n) {
          var s = this,
            r = !(arguments.length > 3 && void 0 !== arguments[3]) || arguments[3];
          Object(yt["a"])(this, e), this.app = a, this.absolute = r, this.vueApp = n.$root.$children[0], this.DOMElement = t, this.DOMRect = t.getBoundingClientRect(), this.isPointerIn = !1, this.prevIsPointerIn = !1, this.destroyed = !1, this.renderHandler = function () {
            s.render()
          };
          var i = this.DOMElement.dataset.src,
            o = i.replace(/[\s]/gi, "");
          if (this.textureId = o, "undefined" === typeof Ct["loader"].resources.displace && Ct["loader"].add("displace", "/static/images/displace-test.jpg"), this.absolute) {
            var l = n.$el.querySelector('img[data-index="'.concat(t.dataset.index, '"].loaded'));
            null !== l && (this.bsTexture = new Ct["BaseTexture"](l), this.texture = new Ct["Texture"](this.bsTexture))
          } else "undefined" === typeof Ct["loader"].resources[this.textureId] && Ct["loader"].add(this.textureId, i);
          Ct["loader"].load(function (e, t) {
            s.loader = e, s.resources = t, s.initStage()
          })
        }
        return Object(St["a"])(e, [{
          key: "initStage",
          value: function () {
            this.rippleContainer = new Ct["Sprite"], this.rippleContainer.anchor = new Ct["Point"](0, 0), this.app.stage.addChild(this.rippleContainer), this.backgroundImage = new Ct["Sprite"](this.absolute ? this.texture : this.resources[this.textureId].texture), this.backgroundImage.x = this.DOMRect.left, this.backgroundImage.y = this.DOMRect.top, this.backgroundImage.anchor = new Ct["Point"](0, 0), this.rippleContainer.addChild(this.backgroundImage), this.mask = (new Ct["Graphics"]).beginFill(9160191).drawRect(0, 0, this.DOMRect.width, this.DOMRect.height).endFill(), this.mask.anchor = new Ct["Point"](.5, .5), this.rippleContainer.mask = this.mask, this.rippleContainer.addChild(this.mask), this.fitImage(), this.displacementImage = new Ct["Sprite"](this.resources.displace.texture), this.displacementImage.x = this.DOMRect.left, this.displacementImage.y = this.DOMRect.top, this.displacementImage.width = 300, this.displacementImage.height = 300, this.displacementImage.anchor = new Ct["Point"](.5, .5), this.rippleContainer.addChild(this.displacementImage), this.displacementFilter = new Ct["filters"].DisplacementFilter(this.displacementImage, 0), this.backgroundImage.filters = [this.displacementFilter], this.app.ticker.add(this.renderHandler)
          }
        }, {
          key: "render",
          value: function () {
            this.DOMRect = this.DOMElement.getBoundingClientRect(), this.absolute ? (this.backgroundImage.position.x = this.backgroundImagePosition.x + this.DOMRect.left, this.backgroundImage.position.y = this.backgroundImagePosition.y + this.DOMRect.top, this.mask.position = new Ct["Point"](this.DOMRect.left, this.DOMRect.top)) : (this.backgroundImage.x = this.backgroundImagePosition.x, this.backgroundImage.y = this.backgroundImagePosition.y, this.mask.x = 0, this.mask.y = 0), this.rippleContainer.alpha = 1, this.absolute ? (this.displacementImage.x = this.vueApp.mouse.lerpedX, this.displacementImage.y = this.vueApp.mouse.lerpedY) : (this.displacementImage.x = this.vueApp.mouse.lerpedX - this.DOMRect.left, this.displacementImage.y = this.vueApp.mouse.lerpedY - this.DOMRect.top), this.displacementFilter.scale.x = 2 * this.vueApp.mouse.lerpedRadius, this.displacementFilter.scale.y = 2 * this.vueApp.mouse.lerpedRadius, this.vueApp.mouse.lerpedX < this.DOMRect.left || this.vueApp.mouse.lerpedX > this.DOMRect.left + this.DOMRect.width || this.vueApp.mouse.lerpedY < this.DOMRect.top || this.vueApp.mouse.lerpedY > this.DOMRect.top + this.DOMRect.height ? this.isPointerIn = !1 : this.isPointerIn = !0, this.isPointerIn === this.prevIsPointerIn || this.isPointerIn || this.destroy(), this.prevIsPointerIn = this.isPointerIn
          }
        }, {
          key: "fitImage",
          value: function () {
            var e = {
                x: this.backgroundImage.width,
                y: this.backgroundImage.height
              },
              t = this.DOMRect.width / this.DOMRect.height,
              a = e.x / e.y,
              n = 1,
              s = new Ct["Point"](0, 0);
            t > a ? (n = this.DOMRect.width / e.x, s.y += -(e.y * n - this.DOMRect.height) / 2) : (n = this.DOMRect.height / e.y, s.x += -(e.x * n - this.DOMRect.width) / 2), this.backgroundImage.scale = new Ct["Point"](n, n), this.backgroundImagePosition = s
          }
        }, {
          key: "destroy",
          value: function () {
            this.destroyed || (this.app.ticker.remove(this.renderHandler), this.app.stage.removeChild(this.rippleContainer), this.rippleContainer.destroy(), this.destroyed = !0)
          }
        }]), e
      }(),
      Ot = Et,
      At = function () {
        function e(t) {
          Object(yt["a"])(this, e), this.DOMElement = t, this.DOMRect = this.DOMElement.getBoundingClientRect(), Ct["utils"].skipHello(), this.app = new Ct["Application"]({
            width: this.DOMRect.width,
            height: this.DOMRect.height,
            transparent: !0
          }), this.ripple = {
            destroyed: !0
          }, this.destroyed = !1, this.DOMElement.appendChild(this.app.view)
        }
        return Object(St["a"])(e, [{
          key: "createRipple",
          value: function (e, t) {
            !this.destroyed && this.ripple.destroyed && (this.ripple = new Ot(e, this.app, t, !1))
          }
        }, {
          key: "destroyRipple",
          value: function () {
            this.ripple.destroyed || this.ripple.destroy()
          }
        }, {
          key: "destroy",
          value: function () {
            this.app.view.style.opacity = 0, this.app.destroy(), this.destroyed = !0
          }
        }]), e
      }(),
      It = At,
      wt = a("c8b5"),
      xt = a.n(wt),
      Tt = {
        name: "FeaturedSlider",
        props: ["data"],
        data: function () {
          return {
            isSliding: !1,
            rippleContainer: !1,
            currentSlide: 0,
            direction: "left",
            revealInfoTimeline: null,
            isInfoOpen: !1,
            hoverInfoTimeline: null
          }
        },
        computed: {
          slides: function () {
            var e = this.data.structure[L.state.lang],
              t = Object.keys(e)[0];
            return e[t].data.slides
          }
        },
        mounted: function () {
          var e = this;
          if (window.addEventListener("keydown", function (t) {
              39 === t.keyCode && e.goNext(), 37 === t.keyCode && e.goPrev()
            }), zs.isTouchDevice()) {
            var t = new xt.a(this.$el);
            t.get("swipe").set({
              direction: xt.a.DIRECTION_ALL
            }), t.on("swipeleft", this.goNext), t.on("swipeup", this.goNext), t.on("swipedown", this.goPrev), t.on("swiperight", this.goPrev)
          }
          this.$eventHub.$on("slide-next", this.goNext), this.$eventHub.$on("slide-prev", this.goPrev), this.$eventHub.$on("route-enter-done", function () {
            zs.isTouchDevice() || (e.rippleContainer = new It(e.$el.querySelector(".image")));
            var t = e.$root.$children[0].mouse,
              a = t.x,
              n = t.y,
              s = e.$el.querySelector(".image");
            zs.isInRect({
              x: a,
              y: n
            }, s.getBoundingClientRect()) && e.createRipple()
          }), this.$eventHub.$on("navigation", function () {
            e.rippleContainer && (e.rippleContainer.destroyRipple(), e.rippleContainer.destroy())
          }), this.revealInfoTimeline = bt.get("reveal-info")(this.$el.querySelector(".info")), this.hoverInfoTimeline = bt.get("hover-info")(this.$el.querySelector(".info"))
        },
        methods: {
          createRipple: function () {
            this.rippleContainer && !this.isSliding && this.rippleContainer.createRipple(this.$el.querySelector(".image"), this)
          },
          slideTo: function (e) {
            this.isSliding || L.state.isInTransition || (this.isSliding = !0, L.commit("SET_NAVIGATION_ALLOWED", !1), this.currentSlide = e)
          },
          goPrev: function () {
            this.rippleContainer && this.rippleContainer.destroyRipple(), this.isSliding || L.state.isInTransition || (this.isSliding = !0, L.commit("SET_NAVIGATION_ALLOWED", !1), this.direction = "prev", this.currentSlide > 0 ? this.currentSlide -= 1 : this.currentSlide = Object.keys(this.slides).length - 1)
          },
          goNext: function () {
            this.rippleContainer && this.rippleContainer.destroyRipple(), this.isSliding || L.state.isInTransition || (this.isSliding = !0, L.commit("SET_NAVIGATION_ALLOWED", !1), this.direction = "next", this.currentSlide < Object.keys(this.slides).length - 1 ? this.currentSlide += 1 : this.currentSlide = 0)
          },
          counterEnter: function (e, t) {
            bt.get("counter-slide")(e).play(), t()
          },
          counterLeave: function (e, t) {
            bt.get("counter-slide")(e, !0).eventCallback("onComplete", t).play()
          },
          titleEnter: function (e, t) {
            var a = this;
            bt.get("title-slide")(e).eventCallback("onComplete", function () {
              a.isSliding = !1, L.commit("SET_NAVIGATION_ALLOWED", !0);
              var e = a.$el.querySelector(".image"),
                t = a.$root.$children[0].mouse,
                n = t.x,
                s = t.y;
              zs.isInRect({
                x: n,
                y: s
              }, e.getBoundingClientRect()) && a.createRipple()
            }).play(), t()
          },
          titleLeave: function (e, t) {
            bt.get("title-slide")(e, !0).eventCallback("onComplete", t).play()
          },
          imageEnter: function (e, t) {
            bt.get("image-slide")(e, !1, this.direction).eventCallback("onStart", t).play()
          },
          imageLeave: function (e, t) {
            bt.get("image-slide")(e, !0, this.direction).eventCallback("onComplete", t).play()
          },
          textEnter: function (e, t) {
            bt.get("text-slide")(e, !1, this.direction).eventCallback("onStart", t).play()
          },
          textLeave: function (e, t) {
            bt.get("text-slide")(e, !0, this.direction).eventCallback("onComplete", t).play()
          },
          toggleInfo: function () {
            this.isInfoOpen ? this.hideInfo() : this.revealInfo(), this.isInfoOpen = !this.isInfoOpen
          },
          hoverInfo: function () {
            this.hoverInfoTimeline.play()
          },
          outInfo: function () {
            this.isInfoOpen || this.hoverInfoTimeline.reverse()
          },
          revealInfo: function () {
            this.revealInfoTimeline.play()
          },
          hideInfo: function () {
            this.revealInfoTimeline.reverse()
          }
        }
      },
      $t = Tt,
      Lt = (a("da04"), a("228e"), a("2877")),
      kt = Object(Lt["a"])($t, ne, se, !1, null, "06587b3d", null);
    kt.options.__file = "FeaturedSlider.vue";
    var Pt = kt.exports,
      Dt = {
        name: "Home",
        components: {
          FeaturedSlider: Pt
        },
        data: function () {
          return {
            sliderData: !1
          }
        },
        created: function () {
          L.commit("SET_PAGE_NEGATIVE", !0), L.commit("SET_HEADER_NEGATIVE", !0), L.commit("SET_HEADLINE", ""), this.$eventHub.$once("dataloaded", this.dataLoaded)
        },
        methods: {
          dataLoaded: function () {
            this.sliderData = L.state.pages[L.state.currentPageId]
          }
        }
      },
      Rt = Dt,
      Nt = (a("5ba6"), Object(Lt["a"])(Rt, te, ae, !1, null, "2332aa36", null));
    Nt.options.__file = "Home.vue";
    var qt = Nt.exports,
      Mt = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "view portfolio",
          attrs: {
            "data-transition": "portfolio"
          }
        }, [a("div", {
          staticClass: "mobile-filters"
        }, [a("div", {
          staticClass: "container"
        }, [a("router-link", {
          staticClass: "mobile-filters__filter",
          class: {
            active: "#all" === e.hash || "" === e.hash
          },
          attrs: {
            to: "#all"
          }
        }, [a("span", [e._v(e._s(e.$translate("FILTERS_ALL")))])]), a("router-link", {
          staticClass: "mobile-filters__filter",
          class: {
            active: "#residential" === e.hash
          },
          attrs: {
            to: "#residential"
          }
        }, [a("span", [e._v(e._s(e.$translate("FILTERS_RESIDENZE_PRIVATE")))])]), a("router-link", {
          staticClass: "mobile-filters__filter",
          class: {
            active: "#retail" === e.hash
          },
          attrs: {
            to: "#retail"
          }
        }, [a("span", [e._v(e._s(e.$translate("FILTERS_OSPITALITY_AND_RETAIL")))])])], 1)]), a("div", {
          staticClass: "projects"
        }, [a("div", {
          staticClass: "container"
        }, e._l(e.pictures, function (t, n) {
          return a("div", {
            key: n,
            staticClass: "project",
            attrs: {
              "data-index": n
            },
            on: {
              mouseenter: e.createRipple
            }
          }, [a("router-link", {
            attrs: {
              to: "/" + e.$store.state.lang + e.$store.getters.getFirstLevelPageUrl("portfolio") + ("en" === e.$store.state.lang ? t.pathEn : "ru" === e.$store.state.lang ? t.pathRu : t.path)
            }
          }, [a("ProjectPicture", {
            attrs: {
              data: t.structure[e.$store.state.lang],
              index: n
            }
          })], 1)], 1)
        }))])])
      },
      jt = [],
      Ht = (a("55dd"), function () {
        function e(t, a) {
          var n = this;
          Object(yt["a"])(this, e), this.DOMElement = t, this.vue = a, this.DOMRect = t.getBoundingClientRect(), Ct["utils"].skipHello(), this.app = new Ct["Application"]({
            width: window.innerWidth,
            height: window.innerHeight,
            resolution: window.devicePixelRatio,
            transparent: !0,
            antialias: !0,
            forceFXAA: !0,
            powerPreference: "high-performance"
          }), this.ripples = [], this.destroyed = !1, this.isScrolling = !1, this.idt = !1, this.vue.$eventHub.$on("scroll-started", function () {
            n.idt && window.clearTimeout(n.idt), n.destroyAllRipple(), n.isScrolling = !0
          }), this.vue.$eventHub.$on("scroll-stopped", function () {
            n.idt = setTimeout(function () {
              n.isScrolling = !1
            }, 2e3)
          }), this.DOMElement.appendChild(this.app.view)
        }
        return Object(St["a"])(e, [{
          key: "createRipple",
          value: function (e, t) {
            if (!this.destroyed && !this.isScrolling) {
              this.destroyAllRipple();
              var a = new Ot(e, this.app, t);
              this.ripples.push(a)
            }
          }
        }, {
          key: "destroyAllRipple",
          value: function () {
            for (var e = this.ripples.length - 1; e >= 0; e -= 1) {
              var t = this.ripples[e];
              t.destroyed || (t && t.destroy(), this.ripples.splice(e, 1))
            }
          }
        }, {
          key: "destroy",
          value: function () {
            this.app.destroy({
              children: !0,
              texture: !0,
              baseTexture: !0
            }), this.destroyed = !0
          }
        }]), e
      }()),
      Ft = Ht,
      Bt = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component project-picture"
        }, [a("div", {
          staticClass: "picture",
          style: {
            backgroundColor: e.randomColor
          },
          attrs: {
            "data-src": e.image,
            "data-index": e.index
          }
        }, [a("div", {
          ref: "picture",
          staticClass: "picture__actual",
          style: {
            backgroundImage: "url('" + e.image + "')",
            opacity: this.loaded ? 1 : 0
          }
        })]), a("div", {
          staticClass: "content"
        }, [a("div", {
          staticClass: "content__title"
        }, [e._v(e._s(e.title))])]), a("img", {
          class: {
            loaded: e.loaded
          },
          staticStyle: {
            display: "none"
          },
          attrs: {
            src: e.image + "&preventcache=" + Date.now(),
            "data-index": e.index,
            crossorigin: "anonymous"
          },
          on: {
            load: function (t) {
              e.loaded = !0
            }
          }
        })])
      },
      zt = [],
      Gt = a("2ef0"),
      Yt = a("310c"),
      Vt = {
        name: "ProjectPicture",
        props: ["data", "index"],
        data: function () {
          return {
            loaded: !1
          }
        },
        computed: {
          randomColor: function () {
            return Yt[Object.keys(Yt)[Object(Gt["random"])(0, 9)]]
          },
          image: function () {
            var e = this,
              t = Object.keys(this.data).find(function (t) {
                return "ProjectHero" === e.data[t].component
              });
            return this.data[t].data.image
          },
          title: function () {
            var e = this,
              t = Object.keys(this.data).find(function (t) {
                return "ProjectDetails" === e.data[t].component
              });
            return this.data[t].data.name
          }
        }
      },
      Ut = Vt,
      Xt = (a("38f4"), a("2473"), Object(Lt["a"])(Ut, Bt, zt, !1, null, "44b74f30", null));
    Xt.options.__file = "ProjectPicture.vue";
    var Wt = Xt.exports,
      Zt = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("footer", [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "footer-container"
        }, [a("div", {
          staticClass: "info",
          attrs: {
            "data-trapped-mode": "info"
          },
          on: {
            click: e.toggleInfo,
            mouseenter: function (t) {
              e.$eventHub.$emit("pointerin", t), e.hoverInfo()
            },
            mouseleave: function (t) {
              e.$eventHub.$emit("pointerout", t), e.outInfo()
            }
          }
        }, [e._m(0), a("div", {
          staticClass: "info__label"
        }, [a("span", [a("span", {
          staticClass: "hide-mobile"
        }, [a("a", {
          staticClass: "info__lang",
          class: {
            active: "it" === e.$store.state.lang
          },
          attrs: {
            href: "/it"
          }
        }, [e._v("It")]), e._v(" . "), a("a", {
          staticClass: "info__lang",
          class: {
            active: "en" === e.$store.state.lang
          },
          attrs: {
            href: "/en"
          }
        }, [e._v("En")]), e._v(" ."), a("span", {
          staticClass: "hide-mobile"
        }, [e._v("Â â¤Â ")])]), a("a", {
          staticClass: "info__social hide-mobile",
          attrs: {
            href: "https://www.facebook.com/pelizzaristudio/",
            target: "_blank",
            rel: "noopener"
          }
        }, [e._v("Facebook")]), a("span", {
          staticClass: "hide-mobile"
        }, [e._v("Â â¤Â ")]), a("a", {
          staticClass: "info__social hide-mobile",
          attrs: {
            href: "https://www.instagram.com/pelizzaristudio/",
            target: "_blank",
            rel: "noopener"
          }
        }, [e._v("Instagram")]), a("span", {
          staticClass: "break-mobile"
        }, [e._v("Â â¤Â ")]), e._v("\n            Pelizzari s.r.l.\n            P.I. 03664730987\n          ")])]), e._m(1)]), e.isProject ? a("div", {
          staticClass: "next-project",
          attrs: {
            "data-trapped-mode": "next-project"
          },
          on: {
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t), e.hoverNextProject()
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t), e.outNextProject()
            }
          }
        }, [a("div", {
          staticClass: "next-project__container",
          attrs: {
            "data-next-path": e.nextProject.path
          },
          on: {
            click: e.goNextProject
          }
        }, [a("div", {
          staticClass: "next-project__label"
        }, [e._v("Next Project")]), a("div", {
          staticClass: "next-project__separator"
        }, [e._v("Â Â Â ")]), a("div", {
          staticClass: "next-project__name",
          domProps: {
            innerHTML: e._s(e.nextProject.labelHTML)
          }
        })])]) : e._e()])]), e.isProject ? a("div", {
          staticClass: "black-mask"
        }, [a("div", {
          staticClass: "black-mask__filler"
        }), e._m(2)]) : e._e()])
      },
      Qt = [function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "info__circle"
        }, [a("div", {
          staticClass: "info__icon"
        })])
      }, function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "info__label-over"
        }, [a("span", [e._v("Info")])])
      }, function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "black-mask__inner"
        })])
      }],
      Kt = (a("20d6"), a("be94")),
      Jt = {
        name: "Footer",
        data: function () {
          return {
            isInfoOpen: !1,
            infoTimeline: null,
            hoverInfoTimeline: null,
            haveData: !1
          }
        },
        created: function () {
          this.$eventHub.$once("dataloaded", this.dataLoaded)
        },
        computed: {
          isProject: function () {
            return "Project" === this.$router.history.current.name
          },
          nextProject: function () {
            if (!this.isProject || !this.haveData) return {
              label: "",
              url: ""
            };
            var e = [],
              t = L.state.pages.portfolio.childrens;
            Object.keys(t).forEach(function (a) {
              e.push(Object(Kt["a"])({}, t[a], {
                key: a
              }))
            }), e.sort(function (e, t) {
              return e.order - t.order
            });
            var a = e.findIndex(function (e) {
                return e.key === L.state.currentPageChildId
              }),
              n = a + 1;
            "undefined" === typeof e[n] && (n = 0);
            var s = e[n].key,
              r = L.state.pages.portfolio.childrens[s],
              i = "it" === L.state.lang ? r.path : "en" === L.state.lang ? r.pathEn : r.pathRu,
              o = r.structure[L.state.lang],
              l = Object.keys(o),
              c = l.find(function (e) {
                return "ProjectDetails" === o[e].component
              }),
              u = o[c].data.name.split(" "),
              d = "";
            return u.forEach(function (e, t) {
              d = "".concat(d).concat(t > 0 ? '<span style="display: inline-block">&nbsp;</span>' : "", '<span style="display: inline-block">').concat(e, "</span>")
            }), {
              label: o[c].data.name,
              labelHTML: d,
              path: i
            }
          }
        },
        mounted: function () {
          this.infoTimeline = bt.get("reveal-info")(this.$el), this.hoverInfoTimeline = bt.get("hover-info")(this.$el)
        },
        watch: {
          nextProject: function () {
            var e = this;
            this.$nextTick(function () {
              e.hoverNextProjectTimeline = bt.get("hover-next-project")(e.$el)
            })
          }
        },
        methods: {
          dataLoaded: function () {
            this.haveData = !0
          },
          toggleInfo: function () {
            this.isInfoOpen ? this.hideInfo() : this.revealInfo(), this.isInfoOpen = !this.isInfoOpen
          },
          hoverNextProject: function () {
            this.hoverNextProjectTimeline.play()
          },
          outNextProject: function () {
            this.hoverNextProjectTimeline.reverse()
          },
          hoverInfo: function () {
            this.hoverInfoTimeline.play()
          },
          outInfo: function () {
            this.isInfoOpen && this.toggleInfo(), this.hoverInfoTimeline.reverse()
          },
          revealInfo: function () {
            this.infoTimeline.play()
          },
          hideInfo: function () {
            this.infoTimeline.reverse()
          },
          goNextProject: function (e) {
            L.commit("SET_HEADER_NEGATIVE", !0), this.$parent.$el.dataset.transition = "project-next", this.$router.push("/".concat(L.state.lang).concat(L.getters.getFirstLevelPageUrl("portfolio")).concat(e.currentTarget.dataset.nextPath))
          }
        }
      },
      ea = Jt,
      ta = (a("2a04"), Object(Lt["a"])(ea, Zt, Qt, !1, null, "8503fc02", null));
    ta.options.__file = "Footer.vue";
    var aa = ta.exports,
      na = {
        name: "Portfolio",
        components: {
          ProjectPicture: Wt,
          Footer: aa
        },
        data: function () {
          return {
            hash: document.location.hash,
            rippleContainer: null,
            rafId: !1,
            haveData: !1
          }
        },
        created: function () {
          L.commit("SET_PAGE_NEGATIVE", !1), L.commit("SET_HEADER_NEGATIVE", !1), L.commit("SET_HEADLINE", "Portfolio"), this.$eventHub.$on("route-enter-done", this.attachScroll), this.$eventHub.$on("dataloaded", this.dataLoaded)
        },
        mounted: function () {
          var e = this;
          this.$eventHub.$on("route-leave-done", function () {
            e.hash = document.location.hash
          }), this.$eventHub.$on("route-enter-done", function () {
            e.rippleContainer = new Ft(e.$el.querySelector(".projects"), e)
          }), this.$eventHub.$on("navigation", function () {
            e.rippleContainer && (e.rippleContainer.destroyAllRipple(), e.rippleContainer.destroy())
          })
        },
        beforeDestroy: function () {
          R.remove(this.rafId)
        },
        computed: {
          pictures: function () {
            if (!this.haveData) return {};
            var e = L.state.pages.portfolio.childrens,
              t = Object.keys(e);
            t.sort(function (t, a) {
              return e[t].order - e[a].order
            });
            var a = [];
            t.forEach(function (t) {
              return a.push(e[t])
            });
            var n = document.location.hash;
            if ("#all" === n || "" === n) return a;
            var s = "";
            "#residential" === n && (s = "residenze-private"), "#retail" === n && (s = "hospitality-retail");
            var r = [];
            return t.forEach(function (e) {
              var t = L.state.pages.portfolio.childrens[e].structure[L.state.lang],
                a = Object.keys(t);
              a.forEach(function (a) {
                "ProjectDetails" === t[a].component && t[a].data.category === s && r.push(L.state.pages.portfolio.childrens[e])
              })
            }), r.sort(function (e, t) {
              return e.order - t.order
            }), r
          }
        },
        methods: {
          dataLoaded: function () {
            this.haveData = !0
          },
          attachScroll: function () {
            this.rafId || (this.rafId = R.add(this.renderScroll))
          },
          renderScroll: function () {
            var e = this.$el.querySelector("canvas"),
              t = this.$root.$children[0].scroll.lerpedAmount;
            l["e"].set(e, {
              y: -t
            })
          },
          createRipple: function (e) {
            this.rippleContainer && this.rippleContainer.createRipple(e.currentTarget.querySelector(".picture"), this)
          }
        }
      },
      sa = na,
      ra = (a("e819"), a("6204"), Object(Lt["a"])(sa, Mt, jt, !1, null, "6e3b9a3d", null));
    ra.options.__file = "Portfolio.vue";
    var ia = ra.exports,
      oa = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "view project",
          attrs: {
            "data-transition": "project"
          }
        }, [e._l(e.structure, function (t, n) {
          return a(e.$store.state.settings.components[t.component].component, {
            key: n,
            tag: "div",
            staticClass: "component",
            attrs: {
              data: t.data
            }
          })
        }), a("Footer")], 2)
      },
      la = [],
      ca = {
        elements: [],
        viewportElements: [],
        initialStates: {
          "text-blocks": {
            state: {
              y: 20,
              scaleY: 1,
              autoAlpha: 0
            },
            handler: function () {}
          },
          "text-blocks-stretch": {
            state: {
              y: 80,
              scaleY: 1.4,
              autoAlpha: 0
            },
            handler: function () {}
          },
          "text-stagger-lines": {
            state: {},
            handler: function () {}
          },
          "image-stretch": {
            state: {
              y: 80,
              scaleY: 1.4,
              autoAlpha: 0
            },
            handler: function () {}
          },
          slideshow: {
            state: {},
            handler: function (e) {
              var t = e.querySelector(".slider-container"),
                a = e.querySelector(".slider-arrows"),
                n = e.querySelector(".slider-counter");
              l["e"].set(t, {
                y: 80,
                scaleY: 1.4,
                autoAlpha: 0
              }), l["e"].set(a, {
                x: 20,
                autoAlpha: 0
              }), l["e"].set(n, {
                y: 20,
                autoAlpha: 0
              })
            }
          },
          team: {
            state: {
              y: 80,
              scaleY: 1.4,
              autoAlpha: 0
            },
            handler: function () {}
          },
          "contact-form": {
            state: {},
            handler: function (e) {
              var t = Array.from(e.querySelectorAll(".form__column-line")),
                a = Array.from(e.querySelectorAll("label")),
                n = Array.from(e.querySelectorAll(".object__label")),
                s = e.querySelector(".object__line"),
                r = Array.from(e.querySelectorAll(".object-mobile__label")),
                i = e.querySelector(".object-mobile__line");
              l["e"].set([s, i, t], {
                scaleX: 0
              }), l["e"].set([n, r, a], {
                autoAlpha: 0,
                yPercent: 100
              })
            }
          },
          "press-element": {
            state: {},
            handler: function (e) {
              var t = e.querySelector(".press__content"),
                a = e.querySelector(".press__bottom-line");
              l["e"].set(t, {
                yPercent: 100,
                autoAlpha: 0
              }), l["e"].set(a, {
                scaleX: 0
              })
            }
          }
        },
        start: function () {
          R.add(ca.loop, "scroll-elements")
        },
        pause: function () {
          R.remove("scroll-elements")
        },
        reflow: function () {
          ca.elements = Array.from(document.querySelectorAll("[data-scroll-element]"));
          var e = Array.from(document.querySelectorAll("[data-scroll-element]")),
            t = Array.from(document.querySelectorAll("[data-scroll-child]"));
          ca.viewportElements = Object(Pe["a"])(e).concat(Object(Pe["a"])(t)), ca.elements.forEach(function (e) {
            var t = e.dataset.scrollInitial;
            Array.from(e.querySelectorAll("[data-scroll-child]")).forEach(function (e) {
              l["e"].set(e, ca.initialStates[t].state)
            }), ca.initialStates[t].handler(e)
          })
        },
        loop: function () {
          for (var e = window.innerHeight / 100 * 80, t = ca.elements.length - 1; t >= 0; t -= 1) {
            var a = ca.elements[t];
            if (a.getBoundingClientRect().top < e) {
              var n = "".concat(a.dataset.scrollAnimation, "-in");
              bt.get(n)(a).play(), ca.elements.splice(t, 1)
            }
          }
          ca.viewportElements.forEach(function (e) {
            var t = e.getBoundingClientRect();
            t.top + t.height > 0 && t.top < window.innerHeight ? e.classList.add("in-viewport") : e.classList.remove("in-viewport")
          })
        },
        animateOut: function () {
          var e = [],
            t = Array.from(document.querySelectorAll("[data-scroll-element].in-viewport"));
          return t.forEach(function (t) {
            var a = "".concat(t.dataset.scrollAnimation, "-out");
            e.push(bt.get(a)(t, !1))
          }), e
        }
      },
      ua = ca,
      da = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component project-hero"
        }, [a("div", {
          staticClass: "hero"
        }, [a("div", {
          ref: "image",
          staticClass: "hero__image",
          style: {
            backgroundImage: "url('" + e.data.image + "')",
            opacity: e.loaded ? 1 : 0
          }
        }, [a("div", {
          ref: "mask",
          staticClass: "hero__image-inner-mask"
        })]), a("div", {
          staticClass: "hero__image-mask"
        }), a("ScrollDown")], 1), a("img", {
          staticStyle: {
            display: "none"
          },
          attrs: {
            src: e.data.image
          },
          on: {
            load: function (t) {
              e.loaded = !0
            }
          }
        })])
      },
      pa = [],
      ha = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "scroll-down",
          on: {
            click: e.scroll
          }
        }, [e._m(0)])
      },
      ma = [function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "scroll"
        }, [a("div", {
          staticClass: "scroll__line"
        }, [a("div", {
          staticClass: "scroll__line-inner"
        })]), a("div", {
          staticClass: "scroll__label"
        }, [a("span", [e._v("Scroll")])])])])
      }],
      fa = {
        name: "ScrollDown",
        data: function () {
          return {
            id: null,
            timeline: null,
            hideTimeline: null
          }
        },
        mounted: function () {
          this.timeline = bt.get("scroll-down-loop")(this.$el).play(), this.hideTimeline = bt.get("scroll-down-hide")(this.$el), this.id = R.add(this.renderScrollDown)
        },
        beforeDestroy: function () {
          R.remove(this.id)
        },
        methods: {
          scroll: function () {
            this.$root.$children[0].forceScroll(400, !1)
          },
          renderScrollDown: function () {
            var e = this.$root.$children[0].scroll.amount;
            e < 10 ? this.hideTimeline.reverse() : this.hideTimeline.play()
          }
        }
      },
      va = fa,
      _a = (a("430d"), Object(Lt["a"])(va, ha, ma, !1, null, "2441b72b", null));
    _a.options.__file = "ScrollDown.vue";
    var ga = _a.exports,
      ba = {
        name: "ProjectHero",
        components: {
          ScrollDown: ga
        },
        props: ["data"],
        data: function () {
          return {
            loaded: !1,
            rafId: !1
          }
        },
        mounted: function () {
          var e = this;
          this.$eventHub.$on("route-enter-done", function () {
            e.rafId || (e.rafId = R.add(e.renderScroll))
          })
        },
        beforeDestroy: function () {
          R.remove(this.rafId)
        },
        methods: {
          renderScroll: function () {
            if ("undefined" !== typeof this.$refs.image && "undefined" !== typeof this.$refs.mask) {
              var e = this.$root.$children[0].scroll.lerpedAmount,
                t = window.innerHeight,
                a = 1.2 + .2 / t * e,
                n = Math.min(Math.max(a, 1), 1.2);
              l["e"].set(this.$refs.image, {
                scale: n
              });
              var s = .8 / t * e * -1,
                r = Math.min(Math.max(s, .2), 1);
              l["e"].set(this.$refs.mask, {
                opacity: r
              })
            }
          }
        }
      },
      ya = ba,
      Sa = (a("d038"), Object(Lt["a"])(ya, da, pa, !1, null, null, null));
    Sa.options.__file = "ProjectHero.vue";
    var Ca = Sa.exports,
      Ea = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component project-intro",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "text-blocks",
            "data-scroll-initial": "text-blocks-stretch"
          }
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "headline",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "headline__category",
          attrs: {
            "data-scroll-child": ""
          }
        }, [e._v(e._s(e.category))]), a("div", {
          staticClass: "headline__title",
          attrs: {
            "data-scroll-child": ""
          }
        }, [e._v(e._s(e.data.name))])]), e.data.text.length > 0 && "<br />" !== e.data.text ? a("div", {
          staticClass: "intro",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "intro__text",
          domProps: {
            innerHTML: e._s(e.data.text)
          }
        })]) : e._e()])])
      },
      Oa = [],
      Aa = {
        name: "ProjectIntro",
        props: ["data"],
        computed: {
          category: function () {
            return "residenze-private" === this.data.category ? this.$translate("FILTERS_RESIDENZE_PRIVATE") : this.$translate("FILTERS_OSPITALITY_AND_RETAIL")
          }
        }
      },
      Ia = Aa,
      wa = (a("bc84"), a("cf7c"), Object(Lt["a"])(Ia, Ea, Oa, !1, null, "4858e5d4", null));
    wa.options.__file = "ProjectIntro.vue";
    var xa = wa.exports,
      Ta = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component double-photo",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "image-stretch",
            "data-scroll-initial": "image-stretch"
          }
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "photo-wrapper"
        }, [a("Photo", {
          attrs: {
            url: e.data.imageLeft,
            alt: e.data.imageLeftAlt
          }
        }), a("Photo", {
          attrs: {
            url: e.data.imageRight,
            alt: e.data.imageRightAlt
          }
        })], 1)])])
      },
      $a = [],
      La = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component photo"
        }, [a("img", {
          attrs: {
            src: e.url,
            alt: e.alt
          }
        }), a("div", {
          staticClass: "photo__image",
          style: {
            backgroundImage: "url(" + e.url + ")"
          },
          attrs: {
            "data-scroll-child": ""
          }
        })])
      },
      ka = [],
      Pa = {
        name: "Photo",
        props: ["url", "alt"]
      },
      Da = Pa,
      Ra = (a("4d31"), Object(Lt["a"])(Da, La, ka, !1, null, "41fb3a05", null));
    Ra.options.__file = "Photo.vue";
    var Na = Ra.exports,
      qa = {
        name: "DoublePhoto",
        props: ["data"],
        components: {
          Photo: Na
        }
      },
      Ma = qa,
      ja = (a("6ac3"), Object(Lt["a"])(Ma, Ta, $a, !1, null, "c4713e88", null));
    ja.options.__file = "DoublePhoto.vue";
    var Ha = ja.exports,
      Fa = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component single-photo",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "image-stretch",
            "data-scroll-initial": "image-stretch"
          }
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "photo-wrapper"
        }, [a("Photo", {
          attrs: {
            url: e.data.url,
            alt: e.data.alt
          }
        })], 1)])])
      },
      Ba = [],
      za = {
        name: "SinglePhoto",
        props: ["data"],
        components: {
          Photo: Na
        }
      },
      Ga = za,
      Ya = (a("82d6"), Object(Lt["a"])(Ga, Fa, Ba, !1, null, "62a1c8d2", null));
    Ya.options.__file = "SinglePhoto.vue";
    var Va = Ya.exports,
      Ua = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component one-column-text",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "text-blocks",
            "data-scroll-initial": "text-blocks-stretch"
          }
        }, [a("div", {
          staticClass: "container"
        }, [e.data.title.length > 0 ? a("div", {
          staticClass: "intro__headline",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("p", {
          domProps: {
            innerHTML: e._s(e.data.title)
          }
        })]) : e._e(), e.data.text.length > 0 ? a("div", {
          staticClass: "intro__subline",
          attrs: {
            "data-scroll-child": ""
          },
          domProps: {
            innerHTML: e._s(e.data.text)
          }
        }) : e._e()])])
      },
      Xa = [],
      Wa = {
        name: "OneColumnText",
        props: ["data"],
        data: function () {
          return {}
        }
      },
      Za = Wa,
      Qa = (a("d778"), Object(Lt["a"])(Za, Ua, Xa, !1, null, "4a2b77cc", null));
    Qa.options.__file = "OneColumnText.vue";
    var Ka = Qa.exports,
      Ja = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component double-column-text",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "text-blocks",
            "data-scroll-initial": "text-blocks-stretch"
          }
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "intro__two-columns",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "column column--left",
          domProps: {
            innerHTML: e._s(e.data.leftColumn)
          }
        }), a("div", {
          staticClass: "column column--right",
          domProps: {
            innerHTML: e._s(e.data.rightColumn)
          }
        })])])])
      },
      en = [],
      tn = {
        name: "DoubleColumnText",
        props: ["data"],
        data: function () {
          return {}
        }
      },
      an = tn,
      nn = (a("5b8b"), Object(Lt["a"])(an, Ja, en, !1, null, "a3bb9354", null));
    nn.options.__file = "DoubleColumnText.vue";
    var sn = nn.exports,
      rn = {
        name: "Project",
        components: {
          ProjectHero: Ca,
          ProjectIntro: xa,
          DoublePhoto: Ha,
          SinglePhoto: Va,
          OneColumnText: Ka,
          DoubleColumnText: sn,
          Footer: aa
        },
        data: function () {
          return {
            structure: {},
            id: null,
            isHeaderNegative: !1
          }
        },
        created: function () {
          L.commit("SET_PAGE_NEGATIVE", !1), L.commit("SET_HEADLINE", ""), this.$eventHub.$once("dataloaded", this.dataLoaded), this.$eventHub.$once("route-enter-done", this.enterDone)
        },
        beforeDestroy: function () {
          R.remove(this.id)
        },
        methods: {
          dataLoaded: function () {
            var e = [],
              t = L.state.pages[L.state.currentPageId].childrens,
              a = t[L.state.currentPageChildId].structure[L.state.lang],
              n = Object.keys(a);
            n.forEach(function (t) {
              return e.push(a[t])
            }), this.structure = e.sort(function (e, t) {
              return e.order - t.order
            }), L.commit("SET_HEADLINE", this.structure[1].data.name), this.$nextTick(ua.reflow)
          },
          enterDone: function () {
            this.id = R.add(this.checkScroll)
          },
          checkScroll: function () {
            var e = this.$root.$children[0].scroll.lerpedAmount,
              t = window.innerHeight - 60;
            Math.abs(e) > t && this.isHeaderNegative ? (this.isHeaderNegative = !1, L.commit("SET_HEADER_NEGATIVE", !1)) : Math.abs(e) <= t && !this.isHeaderNegative && (this.isHeaderNegative = !0, L.commit("SET_HEADER_NEGATIVE", !0))
          }
        }
      },
      on = rn,
      ln = (a("071a"), Object(Lt["a"])(on, oa, la, !1, null, "210d3b82", null));
    ln.options.__file = "Project.vue";
    var cn = ln.exports,
      un = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "view studio",
          attrs: {
            "data-transition": "studio"
          }
        }, [e.haveData ? a("OneColumnText", {
          attrs: {
            data: e.introData
          }
        }) : e._e(), e.haveData ? a("DoubleColumnText", {
          attrs: {
            data: e.columnsData
          }
        }) : e._e(), e.haveData ? a("StudioTeam", {
          attrs: {
            data: e.teamData,
            company: e.companyData
          }
        }) : e._e(), e.haveData ? a("Slider", {
          attrs: {
            data: e.sliderData
          }
        }) : e._e(), e.haveData ? a("StudioCredits", {
          attrs: {
            data: e.creditsData
          }
        }) : e._e(), a("Footer")], 1)
      },
      dn = [],
      pn = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component studio-team",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "team",
            "data-scroll-initial": "team"
          }
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "team-content"
        }, [a("div", {
          staticClass: "member"
        }, [a("div", {
          staticClass: "member__photo",
          style: {
            backgroundImage: "url('" + e.first.image + "')"
          },
          attrs: {
            "data-scroll-child": ""
          }
        }), a("div", {
          staticClass: "member__bio",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("p", [a("strong", [e._v(e._s(e.first.name))])]), a("div", {
          domProps: {
            innerHTML: e._s(e.first.description)
          }
        })]), a("a", {
          staticClass: "company-profile",
          attrs: {
            href: e.company.file,
            target: "_blank"
          }
        }, [a("UnderlineLabel", {
          attrs: {
            label: e.$translate("COMPANY_PRESENTATION")
          }
        })], 1), a("div", {
          staticClass: "sign",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("svg", {
          staticStyle: {
            "enable-background": "new 0 0 276.9 150.4"
          },
          attrs: {
            viewBox: "0 0 276.9 150.4",
            "xml:space": "preserve"
          }
        }, [a("path", {
          staticClass: "sign__name",
          attrs: {
            fill: "none",
            stroke: "#000000",
            "stroke-width": "5px",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            d: "M243.5,17.5c0,0-23-20.8-79.8-13.5c-57.1,7.3-108.1,39.6-138,66.1s-42,75.3,33.9,67.2s130.1-38.6,130.1-38.6 l-8.9,49.1c0,0,16.9-33.5,27.6-43.3c0,0,5.4-5.8,10.4-7.4c0,0,6.2-2.2,7.6,2.9c1.5,5.8-10.1,6.8-10.1,6.8"
          }
        }), a("path", {
          staticClass: "sign__end",
          attrs: {
            fill: "none",
            stroke: "#000000",
            "stroke-width": "5px",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            d: "M268.5,110.3l6-1L268.5,110.3z"
          }
        })])])]), a("div", {
          staticClass: "member"
        }, [a("div", {
          staticClass: "member__photo",
          style: {
            backgroundImage: "url('" + e.second.image + "')"
          },
          attrs: {
            "data-scroll-child": ""
          }
        }), a("div", {
          staticClass: "member__bio",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("p", [a("strong", [e._v(e._s(e.second.name))])]), a("div", {
          domProps: {
            innerHTML: e._s(e.second.description)
          }
        })])])]), a("a", {
          staticClass: "company-profile-mobile",
          attrs: {
            href: e.company.file,
            target: "_blank"
          }
        }, [a("UnderlineLabel", {
          attrs: {
            label: e.$translate("COMPANY_PRESENTATION")
          }
        })], 1)])])
      },
      hn = [],
      mn = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "underline-label",
          class: {
            formButton: e.isFormButton || e.isNewsletterButton, isSmall: e.small
          },
          on: {
            mouseenter: e.enter,
            mouseleave: e.leave
          }
        }, [a("div", {
          staticClass: "label"
        }, [a("span", {
          staticClass: "label__inner label__inner--base"
        }, [e._v(e._s(e.label))]), e.isFormButton || e.isNewsletterButton ? a("span", {
          staticClass: "label__inner label__inner--sending"
        }, [e._v(e._s(e.$translate("FORM_SENDING")))]) : e._e(), e.isFormButton || e.isNewsletterButton ? a("span", {
          staticClass: "label__inner label__inner--sended"
        }, [e._v(e._s(e.$translate("FORM_SENDED")))]) : e._e(), a("span", {
          staticClass: "label__line"
        })])])
      },
      fn = [],
      vn = {
        name: "UnderlineLabel",
        props: ["label", "is-form-button", "is-newsletter-button", "small"],
        data: function () {
          return {
            timeline: null
          }
        },
        mounted: function () {
          this.timeline = bt.get("underline-in")(this.$el);
          var e = this.$el.querySelector(".label__inner--sending"),
            t = this.$el.querySelector(".label__inner--sended");
          this.isFormButton ? (l["e"].set([e, t], {
            autoAlpha: 0,
            xPercent: 100
          }), this.$eventHub.$on("form-sending", this.changeSending), this.$eventHub.$on("form-sended", this.changeSended)) : this.isNewsletterButton && (l["e"].set([e, t], {
            autoAlpha: 0,
            xPercent: 100
          }), this.$eventHub.$on("newsletter-sending", this.changeSending), this.$eventHub.$on("newsletter-sended", this.changeSended))
        },
        methods: {
          enter: function () {
            this.timeline.play()
          },
          leave: function () {
            this.timeline.reverse()
          },
          changeSending: function () {
            if (this.isFormButton || this.isNewsletterButton) {
              var e = this.$el.querySelector(".label__inner--base"),
                t = this.$el.querySelector(".label__inner--sending");
              l["e"].set(this.$el, {
                pointerEvent: "none"
              }), l["e"].to(t, .4, {
                autoAlpha: 1,
                xPercent: 0,
                ease: l["a"].easeInOut
              }), l["e"].to(e, .4, {
                autoAlpha: 0,
                xPercent: -100,
                ease: l["a"].easeInOut
              })
            }
          },
          changeSended: function () {
            if (this.isFormButton || this.isNewsletterButton) {
              var e = this.$el.querySelector(".label__inner--sending"),
                t = this.$el.querySelector(".label__inner--sended");
              l["e"].to(t, .4, {
                autoAlpha: 1,
                xPercent: 0,
                ease: l["a"].easeInOut
              }), l["e"].to(e, .4, {
                autoAlpha: 0,
                xPercent: -100,
                ease: l["a"].easeInOut
              })
            }
          }
        }
      },
      _n = vn,
      gn = (a("662f"), Object(Lt["a"])(_n, mn, fn, !1, null, "cec78e02", null));
    gn.options.__file = "UnderlineLabel.vue";
    var bn = gn.exports,
      yn = {
        name: "StudioTeam",
        props: ["data", "company"],
        components: {
          UnderlineLabel: bn
        },
        computed: {
          first: function () {
            return {
              image: this.data.leftImage,
              name: this.data.leftName,
              description: this.data.leftDescription
            }
          },
          second: function () {
            return {
              image: this.data.rightImage,
              name: this.data.rightName,
              description: this.data.rightDescription
            }
          }
        }
      },
      Sn = yn,
      Cn = (a("9262"), Object(Lt["a"])(Sn, pn, hn, !1, null, "55b07b1c", null));
    Cn.options.__file = "StudioTeam.vue";
    var En = Cn.exports,
      On = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component slider",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "slideshow",
            "data-scroll-initial": "slideshow"
          }
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "slider-container"
        }, [a("transition", {
          attrs: {
            mode: "in-out",
            css: !1
          },
          on: {
            enter: e.slideEnter,
            leave: e.slideLeave
          }
        }, [a("div", {
          key: e.currentSlide,
          staticClass: "slide",
          style: {
            backgroundImage: "url('" + e.slides[e.currentSlide].image + "')"
          }
        })])], 1), a("div", {
          staticClass: "slider-controls"
        }, [a("div", {
          staticClass: "slider-counter"
        }, [a("span", [e._v(e._s(e.$utils.zeroFill(e.currentSlide + 1)) + " - " + e._s(e.slides.length))])]), a("div", {
          staticClass: "slider-arrows"
        }, [a("div", {
          staticClass: "arrow arrow--prev prev",
          attrs: {
            "data-trapped-mode": "arrow"
          },
          on: {
            click: e.slidePrev,
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            }
          }
        }, [a("svg", {
          attrs: {
            viewBox: "0 0 130 16.1",
            "xml:space": "preserve"
          }
        }, [a("line", {
          staticClass: "line",
          attrs: {
            fill: "none",
            stroke: "#CECECE",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "130",
            y1: "8.1",
            x2: "0",
            y2: "8.1"
          }
        }), a("line", {
          staticClass: "arrowLine",
          attrs: {
            fill: "none",
            stroke: "#CECECE",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "121.9",
            y1: "16.1",
            x2: "130",
            y2: "8.1"
          }
        }), a("line", {
          staticClass: "arrowLine",
          attrs: {
            fill: "none",
            stroke: "#CECECE",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "130",
            y1: "8.1",
            x2: "121.9",
            y2: "0"
          }
        })])]), a("div", {
          staticClass: "arrow arrow--next next",
          attrs: {
            "data-trapped-mode": "arrow"
          },
          on: {
            click: e.slideNext,
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            }
          }
        }, [a("svg", {
          attrs: {
            viewBox: "0 0 130 16.1",
            "xml:space": "preserve"
          }
        }, [a("line", {
          staticClass: "line",
          attrs: {
            fill: "none",
            stroke: "#CECECE",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "130",
            y1: "8.1",
            x2: "0",
            y2: "8.1"
          }
        }), a("line", {
          staticClass: "arrowLine",
          attrs: {
            fill: "none",
            stroke: "#CECECE",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "121.9",
            y1: "16.1",
            x2: "130",
            y2: "8.1"
          }
        }), a("line", {
          staticClass: "arrowLine",
          attrs: {
            fill: "none",
            stroke: "#CECECE",
            "stroke-width": "2",
            "stroke-linecap": "round",
            "stroke-linejoin": "round",
            x1: "130",
            y1: "8.1",
            x2: "121.9",
            y2: "0"
          }
        })])])])])]), a("div", {
          staticStyle: {
            display: "none"
          }
        }, e._l(e.slides, function (e, t) {
          return a("img", {
            key: t,
            attrs: {
              src: e.image
            }
          })
        }))])
      },
      An = [],
      In = {
        name: "Slider",
        props: ["data"],
        data: function () {
          return {
            currentSlide: 0,
            isSliding: !1,
            slideDirection: "next"
          }
        },
        computed: {
          slides: function () {
            return this.data.images
          }
        },
        mounted: function () {
          if (zs.isTouchDevice()) {
            var e = new xt.a(this.$el);
            e.on("swipeleft", this.slideNext), e.on("swiperight", this.slidePrev)
          }
        },
        methods: {
          slidePrev: function () {
            this.isSliding || (this.slideDirection = "prev", 0 === this.currentSlide ? this.currentSlide = this.slides.length - 1 : this.currentSlide -= 1)
          },
          slideNext: function () {
            this.isSliding || (this.slideDirection = "next", this.currentSlide === this.slides.length - 1 ? this.currentSlide = 0 : this.currentSlide += 1)
          },
          slideEnter: function (e, t) {
            this.isSliding = !0, bt.get("slideshow-slide-in")(e, this.slideDirection).play(), t()
          },
          slideLeave: function (e, t) {
            var a = this;
            bt.get("slideshow-slide-out")(e, this.slideDirection).eventCallback("onComplete", function () {
              a.isSliding = !1, t()
            }).play()
          }
        }
      },
      wn = In,
      xn = (a("6dcd"), Object(Lt["a"])(wn, On, An, !1, null, "1fd2859d", null));
    xn.options.__file = "Slider.vue";
    var Tn = xn.exports,
      $n = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component studio-credits",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "text-blocks",
            "data-scroll-initial": "text-blocks-stretch"
          }
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "credits-container"
        }, [a("div", {
          staticClass: "credits",
          attrs: {
            "data-scroll-child": ""
          }
        }, [e._v("Credits")]), a("div", {
          staticClass: "credits-content"
        }, e._l(e.data.credits, function (t, n) {
          return a("div", {
            key: n,
            attrs: {
              "data-scroll-child": ""
            }
          }, [a("b", [e._v(e._s(t.role))]), e._v(" " + e._s(t.name) + "\n        ")])
        }))])])])
      },
      Ln = [],
      kn = {
        name: "StudioCredits",
        props: ["data"]
      },
      Pn = kn,
      Dn = (a("880e"), Object(Lt["a"])(Pn, $n, Ln, !1, null, "ff295678", null));
    Dn.options.__file = "StudioCredits.vue";
    var Rn = Dn.exports,
      Nn = {
        name: "Studio",
        components: {
          OneColumnText: Ka,
          DoubleColumnText: sn,
          StudioTeam: En,
          Slider: Tn,
          StudioCredits: Rn,
          Footer: aa
        },
        data: function () {
          return {
            haveData: !1
          }
        },
        computed: Object(Kt["a"])({}, Object(h["b"])({
          componentData: "getComponents"
        }), {
          introData: function () {
            return !!this.haveData && this.componentData("OneColumnText")[0]
          },
          columnsData: function () {
            return !!this.haveData && this.componentData("DoubleColumnText")[0]
          },
          teamData: function () {
            return !!this.haveData && this.componentData("Team")[0]
          },
          sliderData: function () {
            return !!this.haveData && this.componentData("Slideshow")[0]
          },
          creditsData: function () {
            return !!this.haveData && this.componentData("Credits")[0]
          },
          companyData: function () {
            return !!this.haveData && this.componentData("CompanyPresentation")[0]
          }
        }),
        created: function () {
          L.commit("SET_PAGE_NEGATIVE", !1), L.commit("SET_HEADER_NEGATIVE", !1), L.commit("SET_HEADLINE", "Studio"), this.$eventHub.$once("dataloaded", this.dataLoaded)
        },
        methods: {
          dataLoaded: function () {
            this.haveData = !0, this.$nextTick(ua.reflow)
          }
        }
      },
      qn = Nn,
      Mn = (a("ae89"), Object(Lt["a"])(qn, un, dn, !1, null, "58ae15b4", null));
    Mn.options.__file = "Studio.vue";
    var jn = Mn.exports,
      Hn = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "view press",
          attrs: {
            "data-transition": "press"
          }
        }, [e.haveData ? a("OneColumnText", {
          attrs: {
            data: e.introData
          }
        }) : e._e(), a("PressList"), a("Footer")], 1)
      },
      Fn = [],
      Bn = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component press-list"
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "press-list-container"
        }, e._l(e.articles, function (t, n) {
          return a("div", {
            key: n,
            staticClass: "press",
            attrs: {
              "data-scroll-element": "",
              "data-scroll-animation": "press-element",
              "data-scroll-initial": "press-element",
              "data-trapped-mode": "press-list"
            },
            on: {
              mouseover: function (t) {
                e.$eventHub.$emit("pointerin", t), e.showImage(t)
              },
              mouseout: function (t) {
                e.$eventHub.$emit("pointerout", t), e.hideImage(t)
              }
            }
          }, [a("a", {
            attrs: {
              href: t.data.file,
              target: "_blank",
              title: t.data.label
            }
          }, [a("div", {
            staticClass: "press__content",
            attrs: {
              "data-scroll-child": ""
            }
          }, [a("span", {
            staticClass: "press__title"
          }, [e._v(e._s(t.data.label))]), a("span", {
            staticClass: "press__separator"
          }, [e._v("Â Â Â ")]), a("span", {
            staticClass: "press__date"
          }, [e._v(e._s(t.data.date))])])]), a("div", {
            staticClass: "press__bottom-line"
          }), a("div", {
            staticClass: "press__line",
            style: {
              backgroundColor: t.data.color || "#686867"
            }
          }), a("div", {
            staticClass: "press__image"
          }, [a("div", {
            staticClass: "press__image-overlay",
            style: {
              backgroundColor: t.data.color || "#686867"
            }
          }), a("div", {
            staticClass: "press__image-inner",
            style: {
              backgroundImage: "url('" + t.data.image + "')"
            }
          })])])
        }))])])
      },
      zn = [],
      Gn = {
        name: "PressList",
        data: function () {
          return {
            id: null,
            haveData: !1
          }
        },
        created: function () {
          this.$eventHub.$once("dataloaded", this.dataLoaded)
        },
        mounted: function () {
          this.id = R.add(this.renderSelected)
        },
        computed: {
          articles: function () {
            if (!this.haveData) return {};
            var e = Object(Kt["a"])({}, L.state.pages.press.structure[L.state.lang]);
            delete e[Object.keys(e)[0]];
            var t = [];
            return Object.keys(e).forEach(function (a) {
              return t.push(e[a])
            }), t.sort(function (e, t) {
              return e.order - t.order
            })
          }
        },
        beforeDestroy: function () {
          R.remove(this.id)
        },
        methods: {
          dataLoaded: function () {
            this.haveData = !0, this.$nextTick(this.initImages), this.$nextTick(ua.reflow)
          },
          renderSelected: function () {
            var e = this;
            if (zs.isTouchDevice()) {
              var t = Array.from(this.$el.querySelectorAll(".press"));
              t.forEach(function (t) {
                var a = t.getBoundingClientRect(),
                  n = window.innerHeight / 2 - a.height / 2,
                  s = window.innerHeight / 2 + a.height / 2;
                a.top + a.height / 2 > n && a.top + a.height / 2 < s && (t.classList.contains("active") || (e.$el.querySelector(".press.active") && (e.hideImage(!1, e.$el.querySelector(".press.active").querySelector(".press__image")), e.$el.querySelector(".press.active").classList.remove("active"), e.showImage(!1, t.querySelector(".press__image"))), t.classList.add("active")))
              })
            }
          },
          initImages: function () {
            var e = Array.from(this.$el.querySelectorAll(".press__image"));
            l["e"].set(e, {
              yPercent: -30,
              scaleY: 1.4,
              autoAlpha: 0
            });
            var t = Array.from(this.$el.querySelectorAll(".press"));
            t.forEach(function (e) {
              var t = e.querySelector(".press__line"),
                a = e.querySelector(".press__title");
              l["e"].set(t, {
                width: a.getBoundingClientRect().width + 5
              })
            })
          },
          showImage: function (e, t) {
            var a = e ? e.currentTarget.querySelector(".press__image") : t;
            l["e"].to(a, .8, {
              yPercent: -50,
              scaleY: 1,
              autoAlpha: 1,
              transformOrigin: "50% 0%",
              ease: l["a"].easeOut
            })
          },
          hideImage: function (e, t) {
            var a = e ? e.currentTarget.querySelector(".press__image") : t;
            l["e"].to(a, .6, {
              yPercent: -80,
              scaleY: 1.4,
              autoAlpha: 0,
              transformOrigin: "50% 0%",
              ease: l["a"].easeOut,
              onComplete: function () {
                l["e"].set(a, {
                  yPercent: -30
                })
              }
            })
          }
        }
      },
      Yn = Gn,
      Vn = (a("d163"), Object(Lt["a"])(Yn, Bn, zn, !1, null, "c4e44266", null));
    Vn.options.__file = "PressList.vue";
    var Un = Vn.exports,
      Xn = {
        name: "Press",
        components: {
          PressList: Un,
          Footer: aa,
          OneColumnText: Ka
        },
        data: function () {
          return {
            haveData: !1
          }
        },
        computed: Object(Kt["a"])({}, Object(h["b"])({
          componentData: "getComponents"
        }), {
          introData: function () {
            return !!this.haveData && this.componentData("OneColumnText")[0]
          }
        }),
        created: function () {
          L.commit("SET_PAGE_NEGATIVE", !1), L.commit("SET_HEADER_NEGATIVE", !1), L.commit("SET_HEADLINE", "Press"), this.$eventHub.$once("dataloaded", this.dataLoaded)
        },
        methods: {
          dataLoaded: function () {
            this.haveData = !0, this.$nextTick(ua.reflow)
          }
        }
      },
      Wn = Xn,
      Zn = (a("2733"), Object(Lt["a"])(Wn, Hn, Fn, !1, null, "2d1ddcd1", null));
    Zn.options.__file = "Press.vue";
    var Qn = Zn.exports,
      Kn = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "view contact",
          attrs: {
            "data-transition": "contact"
          }
        }, [e.hasData ? a("ContactBlock", {
          attrs: {
            data: e.firstBlock
          }
        }) : e._e(), e.hasData ? a("ContactForm", {
          attrs: {
            data: e.form
          }
        }) : e._e(), e.hasData ? a("NewsletterBlock", {
          attrs: {
            data: e.newsletterBlock
          }
        }) : e._e(), a("Footer")], 1)
      },
      Jn = [],
      es = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component contact-intro"
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "contact__intro",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "text-blocks",
            "data-scroll-initial": "text-blocks-stretch"
          }
        }, [a("div", {
          staticClass: "contact__title",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("p", [e._v(e._s(e.$translate("GENERAL_INFORMATION")))])]), e._m(0)])])])
      },
      ts = [function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "contact__mail",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "mail"
        }, [a("a", {
          attrs: {
            href: "mailto:info@pellizzari.com"
          }
        }, [e._v("\n            info@pelizzari.com\n          ")])])])
      }],
      as = {
        name: "ContactIntro"
      },
      ns = as,
      ss = (a("d1ed"), Object(Lt["a"])(ns, es, ts, !1, null, "854394f6", null));
    ss.options.__file = "ContactIntro.vue";
    var rs = ss.exports,
      is = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component contact-block"
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "contact__two-columns",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "text-blocks",
            "data-scroll-initial": "text-blocks-stretch"
          }
        }, [a("div", {
          staticClass: "column column--left",
          attrs: {
            "data-scroll-child": ""
          },
          domProps: {
            innerHTML: e._s(e.data.leftColumn)
          }
        }), a("div", {
          staticClass: "column column--right",
          attrs: {
            "data-scroll-child": ""
          },
          domProps: {
            innerHTML: e._s(e.data.rightColumn)
          }
        })])])])
      },
      os = [],
      ls = {
        name: "ContactBlock",
        props: ["data"],
        mounted: function () {
          var e = this;
          this.$nextTick(function () {
            var t = e.$el.querySelector('.column a[href^="tel:"]');
            t.addEventListener("click", function () {
              gtag_report_conversion(t.getAttribute("href"))
            })
          })
        }
      },
      cs = ls,
      us = (a("d0bf"), Object(Lt["a"])(cs, is, os, !1, null, null, null));
    us.options.__file = "ContactBlock.vue";
    var ds = us.exports,
      ps = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component contact-form",
          attrs: {
            "data-scroll-element": "",
            "data-scroll-animation": "contact-form",
            "data-scroll-initial": "contact-form"
          }
        }, [a("div", {
          staticClass: "container"
        }, [a("form", {
          attrs: {
            method: "POST"
          },
          nativeOn: {
            submit: function (t) {
              e.event.preventDefault()
            }
          }
        }, [a("div", {
          staticClass: "form__row",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "object",
          on: {
            mouseenter: e.requestTypeHover,
            mouseleave: e.requestTypeOut
          }
        }, [a("span", {
          staticClass: "object__label firstone selected"
        }, [e._v(e._s(e.$translate("FORM_REQUEST_TYPE")) + " *")]), e._l(e.data.recipients, function (t, n) {
          return a("span", {
            key: n,
            staticClass: "object__label",
            attrs: {
              "data-trapped-mode": "filter-label",
              "data-mode": t.label,
              "data-email": t.email
            },
            on: {
              mouseover: function (t) {
                e.$eventHub.$emit("pointerin", t)
              },
              mouseout: function (t) {
                e.$eventHub.$emit("pointerout", t)
              }
            }
          }, [e._v(e._s(t.label))])
        }), a("div", {
          staticClass: "object__line"
        })], 2), a("div", {
          staticClass: "object-mobile"
        }, [a("div", {
          staticClass: "object-mobile__label",
          on: {
            click: e.openMobileRequestType
          }
        }, [e._v("\n            " + e._s(e.selectedMode || e.$translate("FORM_REQUEST_TYPE") + " *") + "\n          ")]), a("div", {
          staticClass: "object-mobile__line"
        }), a("div", {
          staticClass: "object-mobile__modal"
        }, e._l(e.data.recipients, function (t, n) {
          return a("div", {
            key: n,
            staticClass: "object-mobile__modal-label",
            attrs: {
              "data-mode": t.label,
              "data-email": t.email
            },
            on: {
              click: e.selectMode
            }
          }, [a("span", [e._v(e._s(t.label))])])
        }))]), a("input", {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: e.values.object,
            expression: "values.object"
          }],
          attrs: {
            name: "object",
            type: "hidden"
          },
          domProps: {
            value: e.values.object
          },
          on: {
            input: function (t) {
              t.target.composing || e.$set(e.values, "object", t.target.value)
            }
          }
        }), a("input", {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: e.values.recipient,
            expression: "values.recipient"
          }],
          attrs: {
            name: "recipient",
            type: "hidden"
          },
          domProps: {
            value: e.values.recipient
          },
          on: {
            input: function (t) {
              t.target.composing || e.$set(e.values, "recipient", t.target.value)
            }
          }
        })]), a("div", {
          staticClass: "form__row",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "form__column form__column--name"
        }, [a("label", {
          attrs: {
            for: "name"
          }
        }, [e._v(e._s(e.$translate("FORM_NAME")) + " *")]), a("input", {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: e.values.name,
            expression: "values.name"
          }],
          attrs: {
            id: "name",
            name: "name",
            type: "text",
            required: "",
            autocomplete: "name"
          },
          domProps: {
            value: e.values.name
          },
          on: {
            input: function (t) {
              t.target.composing || e.$set(e.values, "name", t.target.value)
            }
          }
        }), a("div", {
          staticClass: "form__column-line"
        })]), a("div", {
          staticClass: "form__column form__column--company"
        }, [a("label", {
          attrs: {
            for: "name"
          }
        }, [e._v(e._s(e.$translate("FORM_AZIENDA")))]), a("input", {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: e.values.company,
            expression: "values.company"
          }],
          attrs: {
            id: "company",
            name: "company",
            type: "text",
            autocomplete: "organization"
          },
          domProps: {
            value: e.values.company
          },
          on: {
            input: function (t) {
              t.target.composing || e.$set(e.values, "company", t.target.value)
            }
          }
        }), a("div", {
          staticClass: "form__column-line"
        })])]), a("div", {
          staticClass: "form__row",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "form__column form__column--email"
        }, [a("label", {
          attrs: {
            for: "email"
          }
        }, [e._v(e._s(e.$translate("FORM_EMAIL")) + " *")]), a("input", {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: e.values.email,
            expression: "values.email"
          }],
          attrs: {
            id: "email",
            name: "email",
            type: "email",
            required: "",
            autocomplete: "email"
          },
          domProps: {
            value: e.values.email
          },
          on: {
            input: function (t) {
              t.target.composing || e.$set(e.values, "email", t.target.value)
            }
          }
        }), a("div", {
          staticClass: "form__column-line"
        })]), a("div", {
          staticClass: "form__column form__column--tel"
        }, [a("label", {
          attrs: {
            for: "tel"
          }
        }, [e._v(e._s(e.$translate("FORM_TEL")) + " *")]), a("input", {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: e.values.tel,
            expression: "values.tel"
          }],
          attrs: {
            id: "tel",
            name: "tel",
            type: "tel",
            required: "",
            autocomplete: "tel-national"
          },
          domProps: {
            value: e.values.tel
          },
          on: {
            input: function (t) {
              t.target.composing || e.$set(e.values, "tel", t.target.value)
            }
          }
        }), a("div", {
          staticClass: "form__column-line"
        })])]), a("div", {
          staticClass: "form__row form__row--message",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "form__column form__column--message"
        }, [a("label", {
          attrs: {
            for: "message"
          }
        }, [e._v(e._s(e.$translate("FORM_MESSAGE")) + " *")]), a("textarea", {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: e.values.message,
            expression: "values.message"
          }],
          attrs: {
            name: "message",
            id: "message",
            cols: "30",
            rows: "10"
          },
          domProps: {
            value: e.values.message
          },
          on: {
            input: function (t) {
              t.target.composing || e.$set(e.values, "message", t.target.value)
            }
          }
        }), a("div", {
          staticClass: "form__column-line"
        })])]), a("div", {
          staticClass: "form__row form__row--file",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "form__column form__column--file"
        }, [a("label", {
          attrs: {
            for: "file"
          }
        }, [e._v(e._s(e.$translate("FORM_FILE")) + " "), a("small", [e._v("(max. 8mb)")]), "" !== e.file.name ? a("span", [e._v("\n            Â Â .Â Â " + e._s(e.file.name) + "\n            ")]) : e._e()]), a("input", {
          attrs: {
            id: "file",
            name: "file",
            type: "file",
            accept: "image/*,.pdf,.doc,.docx,.pages"
          }
        }), a("div", {
          staticClass: "form__column-line"
        })])]), a("div", {
          staticClass: "form__row form__row--privacy",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "form__column form__column--privacy"
        }, e._l(e.data.agreements, function (t, n) {
          return a("div", {
            key: n,
            staticClass: "privacy__agreement"
          }, [a("input", {
            attrs: {
              id: "p-" + n,
              name: "p-" + n,
              type: "checkbox",
              value: "1",
              required: 0 === n
            }
          }), a("label", {
            staticClass: "privacy-label",
            attrs: {
              for: "p-" + n
            },
            domProps: {
              innerHTML: e._s(t)
            }
          })])
        })), a("div", {
          staticClass: "form__column form__column--send",
          on: {
            click: e.sendForm
          }
        }, [a("UnderlineLabel", {
          attrs: {
            label: e.$translate("FORM_SEND"),
            "is-form-button": !0
          }
        })], 1)])]), a("div", {
          staticClass: "form__thx"
        }, [a("span", [e._v(e._s(e.$translate("FORM_THX")))])])])])
      },
      hs = [],
      ms = {
        name: "ContactForm",
        props: ["data"],
        components: {
          UnderlineLabel: bn
        },
        data: function () {
          return {
            requestTypeTimeline: null,
            selectedItem: !1,
            mobileRequestOpen: !1,
            mobileRequestOpenTimeline: null,
            selectedMode: !1,
            values: {
              recipient: "",
              object: "",
              name: "",
              company: "",
              email: "",
              tel: "",
              message: ""
            },
            file: {
              name: "",
              size: 0
            }
          }
        },
        mounted: function () {
          var e = this;
          this.requestTypeTimeline = bt.get("request-hover")(this.$el), this.mobileRequestOpenTimeline = bt.get("mobile-request-open")(this.$el), this.$nextTick(function () {
            var t = Array.from(e.$el.querySelectorAll('input:not([type="hidden"]):not([type="checkbox"]):not([type="file"]), textarea, select'));
            t.forEach(function (t) {
              t.addEventListener("mouseover", e.moveLabel), t.addEventListener("mouseleave", e.moveLabel), t.addEventListener("focus", e.moveLabel), t.addEventListener("blur", e.moveLabel)
            });
            var a = Array.from(e.$el.querySelectorAll(".object__label"));
            a.forEach(function (e, t) {
              l["e"].set(e, {
                left: 120 * t + (t > 0 ? 50 : 0)
              }), e.dataset.originalLeft = 120 * t + (t > 0 ? 50 : 0)
            }), a.forEach(function (t) {
              return t.addEventListener("click", e.selectType)
            });
            var n = a.find(function (e) {
              return e.classList.contains("selected")
            });
            l["e"].set(n, {
              left: 0
            });
            var s = e.$el.querySelectorAll(".object__label:not(.selected)"),
              r = e.$el.querySelector(".object__line");
            l["e"].set(s, {
              yPercent: 100,
              autoAlpha: 0
            }), l["e"].set(r, {
              width: 120
            });
            var i = e.$el.querySelector('input[type="file"]');
            i.addEventListener("change", function () {
              e.file = "" !== i ? {
                name: i.files[0].name,
                size: i.files[0].size
              } : {
                name: "",
                size: 0
              }
            })
          });
          var t = this.$el.querySelector(".form__thx");
          l["e"].set(t, {
            autoAlpha: 0,
            y: 30
          }), this.$eventHub.$on("form-sended", this.changeSended)
        },
        methods: {
          moveLabel: function (e) {
            var t = e.currentTarget,
              a = t.parentNode.childNodes[0];
            "mouseover" === e.type || "focus" === e.type ? t.focus() : t.blur(), l["e"].to(a, .4, {
              y: ("mouseleave" === e.type || "blur" === e.type) && t.value.length <= 0 ? 0 : -30,
              delay: .1,
              ease: l["b"].easeOut
            })
          },
          requestTypeHover: function () {
            this.selectedItem || this.requestTypeTimeline.play()
          },
          requestTypeOut: function () {
            this.selectedItem || this.requestTypeTimeline.reverse()
          },
          selectType: function (e) {
            var t = this,
              a = e.currentTarget;
            this.selectedItem = !0, this.values.recipient = a.dataset.email, this.values.object = a.dataset.mode, this.$el.querySelector(".object__label.selected").classList.remove("selected"), a.classList.add("selected"), bt.get("request-click")(this.$el).eventCallback("onComplete", function () {
              t.requestTypeTimeline = bt.get("request-hover")(t.$el), t.selectedItem = !1
            }).play()
          },
          openMobileRequestType: function () {
            this.mobileRequestOpen ? (this.mobileRequestOpenTimeline.reverse(), this.mobileRequestOpen = !1) : (this.mobileRequestOpenTimeline.play(), this.mobileRequestOpen = !0)
          },
          selectMode: function (e) {
            this.selectedMode = e.currentTarget.dataset.mode, this.values.recipient = e.currentTarget.data.email, this.values.object = this.selectedMode, this.openMobileRequestType()
          },
          sendForm: function () {
            Array.from(this.$el.querySelectorAll(".form__column.error")).forEach(function (e) {
              return e.classList.remove("error")
            });
            var e = this.$el.querySelector(".object"),
              t = this.$el.querySelector(".object-mobile");
            e.classList.remove("error"), t.classList.remove("error");
            var a = this.$el.querySelector(".form__column--name"),
              n = this.$el.querySelector(".form__column--email"),
              s = this.$el.querySelector(".form__column--tel"),
              r = this.$el.querySelector(".form__column--message"),
              i = this.$el.querySelector(".form__column--file"),
              o = this.$el.querySelector('[name="p-0"]');
            o.classList.remove("error");
            var l = !0;
            if (this.values.recipient.length < 1 && (e.classList.add("error"), t.classList.add("error"), l = !1), this.values.name.length < 1 && (a.classList.add("error"), l = !1), this.values.email.length < 1 && (n.classList.add("error"), l = !1), this.values.tel.length < 1 && (s.classList.add("error"), l = !1), this.values.message.length < 1 && (r.classList.add("error"), l = !1), this.file.size > 8388608 && (i.classList.add("error"), l = !1), o.checked || (o.classList.add("error"), l = !1), l) {
              this.$el.classList.add("avoid-interactions"), this.$eventHub.$emit("form-sending");
              var c = new FormData;
              c.append("name", this.values.name), c.append("company", this.values.company), c.append("email", this.values.email), c.append("tel", this.values.tel), c.append("recipient", this.values.recipient), c.append("object", this.values.object), c.append("message", this.values.message), c.append("privacy", 1), c.append("newsletter", 1), c.append("file", this.$el.querySelector('[name="file"]').files[0]), this.$store.dispatch("SEND_FORM", {
                form: c,
                vue: this
              })
            }
          },
          changeSended: function () {
            var e = this.$el.querySelector(".form__thx");
            l["e"].to(e, 1, {
              autoAlpha: 1,
              y: 0,
              ease: l["b"].easeInOut
            })
          }
        }
      },
      fs = ms,
      vs = (a("6aa5"), a("50d9"), Object(Lt["a"])(fs, ps, hs, !1, null, "ef2f4e26", null));
    vs.options.__file = "ContactForm.vue";
    var _s = vs.exports,
      gs = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component newsletter"
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "newsletter-container"
        }, [a("div", {
          staticClass: "left-text",
          domProps: {
            innerHTML: e._s(e.data.text)
          }
        }), a("div", {
          staticClass: "right-text"
        }, [a("div", {
          staticClass: "newsletter-wrapper"
        }, [a("form", {
          attrs: {
            method: "POST"
          },
          nativeOn: {
            submit: function (t) {
              e.event.preventDefault()
            }
          }
        }, [a("div", {
          staticClass: "form__row",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "form__column form__column--email"
        }, [a("label", {
          attrs: {
            for: "email"
          }
        }, [e._v(e._s(e.$translate("FORM_EMAIL")) + " *")]), a("input", {
          directives: [{
            name: "model",
            rawName: "v-model",
            value: e.values.email,
            expression: "values.email"
          }],
          attrs: {
            id: "email",
            name: "email",
            type: "email",
            required: "",
            autocomplete: "email"
          },
          domProps: {
            value: e.values.email
          },
          on: {
            input: function (t) {
              t.target.composing || e.$set(e.values, "email", t.target.value)
            }
          }
        }), a("div", {
          staticClass: "form__column-line"
        })])]), a("div", {
          staticClass: "form__row",
          attrs: {
            "data-scroll-child": ""
          }
        }, [a("div", {
          staticClass: "form__column form__column--send",
          on: {
            click: e.sendNewsletter
          }
        }, [a("UnderlineLabel", {
          attrs: {
            label: e.$translate("SIGNUP_FOR_NEWSLETTER"),
            "is-newsletter-button": !0
          }
        })], 1)])]), a("div", {
          staticClass: "form__thx"
        }, [a("span", [e._v(e._s(e.$translate("NEWSLETTER_THX")))])])])])]), a("div", {
          staticClass: "social-container"
        }, [a("a", {
          attrs: {
            href: "https://www.facebook.com/pelizzaristudio/",
            target: "_blank",
            rel: "noopener"
          }
        }, [a("div", {
          staticClass: "social"
        }, [a("UnderlineLabel", {
          attrs: {
            label: "Facebook",
            small: !0
          }
        })], 1)]), a("div", {
          staticClass: "social-separator"
        }, [e._v("Â Â Â â¤Â Â Â ")]), a("a", {
          attrs: {
            href: "https://www.instagram.com/pelizzaristudio/",
            target: "_blank",
            rel: "noopener"
          }
        }, [a("div", {
          staticClass: "social"
        }, [a("UnderlineLabel", {
          attrs: {
            label: "Instagram",
            small: !0
          }
        })], 1)]), a("br"), a("a", {
          attrs: {
            href: "https://www.pinterest.com/claudiapelizzar/",
            target: "_blank",
            rel: "noopener"
          }
        }, [a("div", {
          staticClass: "social"
        }, [a("UnderlineLabel", {
          attrs: {
            label: "Pinterest",
            small: !0
          }
        })], 1)]), a("div", {
          staticClass: "social-separator"
        }, [e._v("Â Â Â â¤Â Â Â ")]), a("a", {
          attrs: {
            href: "https://vimeo.com/pelizzari",
            target: "_blank",
            rel: "noopener"
          }
        }, [a("div", {
          staticClass: "social"
        }, [a("UnderlineLabel", {
          attrs: {
            label: "Vimeo",
            small: !0
          }
        })], 1)]), a("br"), a("a", {
          attrs: {
            href: "https://www.archilovers.com/teams/123066/pelizzari-studio.html",
            target: "_blank",
            rel: "noopener"
          }
        }, [a("div", {
          staticClass: "social"
        }, [a("UnderlineLabel", {
          attrs: {
            label: "Archilovers",
            small: !0
          }
        })], 1)])])])])
      },
      bs = [],
      ys = {
        name: "NewsletterBlock",
        components: {
          UnderlineLabel: bn
        },
        props: ["data"],
        data: function () {
          return {
            values: {
              email: ""
            }
          }
        },
        mounted: function () {
          var e = this;
          this.$nextTick(function () {
            var t = Array.from(e.$el.querySelectorAll('input[type="email"]'));
            t.forEach(function (t) {
              t.addEventListener("mouseover", e.moveLabel), t.addEventListener("mouseleave", e.moveLabel), t.addEventListener("focus", e.moveLabel), t.addEventListener("blur", e.moveLabel)
            })
          });
          var t = this.$el.querySelector(".form__thx");
          l["e"].set(t, {
            autoAlpha: 0,
            y: 30
          }), this.$eventHub.$on("newsletter-sended", this.changeSended)
        },
        methods: {
          moveLabel: function (e) {
            var t = e.currentTarget,
              a = t.parentNode.childNodes[0];
            "mouseover" === e.type || "focus" === e.type ? t.focus() : t.blur(), l["e"].to(a, .4, {
              y: ("mouseleave" === e.type || "blur" === e.type) && t.value.length <= 0 ? 0 : -30,
              delay: .1,
              ease: l["b"].easeOut
            })
          },
          sendNewsletter: function () {
            Array.from(this.$el.querySelectorAll(".form__column.error")).forEach(function (e) {
              return e.classList.remove("error")
            });
            var e = this.$el.querySelector(".form__column--email"),
              t = !0;
            if (this.values.email.length < 1 && (e.classList.add("error"), t = !1), t) {
              this.$el.querySelector("form").classList.add("avoid-interactions"), this.$eventHub.$emit("newsletter-sending");
              var a = new FormData;
              a.append("email", this.values.email), this.$store.dispatch("SEND_NEWSLETTER", {
                form: a,
                vue: this
              })
            }
          },
          changeSended: function () {
            var e = this.$el.querySelector(".form__thx");
            l["e"].to(e, 1, {
              autoAlpha: 1,
              y: 0,
              ease: l["b"].easeInOut
            })
          }
        }
      },
      Ss = ys,
      Cs = (a("18c5"), Object(Lt["a"])(Ss, gs, bs, !1, null, null, null));
    Cs.options.__file = "NewsletterBlock.vue";
    var Es = Cs.exports,
      Os = {
        name: "Contact",
        components: {
          ContactBlock: ds,
          ContactForm: _s,
          ContactIntro: rs,
          NewsletterBlock: Es,
          Footer: aa
        },
        data: function () {
          return {
            hasData: !1
          }
        },
        computed: Object(Kt["a"])({}, Object(h["b"])({
          componentData: "getComponents"
        }), {
          firstBlock: function () {
            return !!this.hasData && this.componentData("ContactBlock")[0]
          },
          form: function () {
            return !!this.hasData && this.componentData("ContactForm")[0]
          },
          newsletterBlock: function () {
            return !!this.hasData && this.componentData("NewsletterBlock")[0]
          }
        }),
        created: function () {
          L.commit("SET_PAGE_NEGATIVE", !1), L.commit("SET_HEADER_NEGATIVE", !1), L.commit("SET_HEADLINE", "Contact"), this.$eventHub.$once("dataloaded", this.dataLoaded)
        },
        methods: {
          dataLoaded: function () {
            this.hasData = !0, this.$nextTick(ua.reflow)
          }
        }
      },
      As = Os,
      Is = (a("e55b"), Object(Lt["a"])(As, Kn, Jn, !1, null, "ce76f972", null));
    Is.options.__file = "Contact.vue";
    var ws = Is.exports,
      xs = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "view"
        })
      },
      Ts = [],
      $s = {
        name: "BaseView",
        data: function () {
          return {}
        }
      },
      Ls = $s,
      ks = (a("9e44"), Object(Lt["a"])(Ls, xs, Ts, !1, null, "0f21e6b2", null));
    ks.options.__file = "BaseView.vue";
    var Ps = ks.exports,
      Ds = function () {
        var e = this,
          t = e.$createElement;
        e._self._c;
        return e._m(0)
      },
      Rs = [function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "view error"
        }, [a("span", [e._v("This page does not exists")])])
      }],
      Ns = {
        name: "Error404",
        data: function () {
          return {}
        }
      },
      qs = Ns,
      Ms = (a("d2ef"), Object(Lt["a"])(qs, Ds, Rs, !1, null, null, null));
    Ms.options.__file = "Error404.vue";
    var js = Ms.exports;
    n["a"].use(ee["a"]);
    var Hs = new ee["a"]({
      mode: "history",
      base: "/",
      routes: [{
        path: "/:lang",
        name: "Home",
        component: qt
      }, {
        path: "/:lang/:page(portfolio)",
        name: "Portfolio",
        component: ia
      }, {
        path: "/:lang/:page(portfolio)/:project",
        name: "Project",
        component: cn
      }, {
        path: "/:lang/:page(studio)",
        name: "Studio",
        component: jn
      }, {
        path: "/:lang/:page(press)",
        name: "Press",
        component: Qn
      }, {
        path: "/:lang/:page(contact|contatti)",
        name: "Contact",
        component: ws
      }, {
        path: "/*",
        name: "BaseView",
        component: Ps
      }, {
        path: "/error",
        name: "Error404",
        component: js
      }]
    });
    Hs.beforeEach(function (e, t, a) {
      var n = e.params.lang;
      ("undefined" === typeof n || "it" !== n && "en" !== n && "ru" !== n) && (n = "en", window.navigator.language.indexOf("it") >= 0 && (n = "it"), window.navigator.language.indexOf("ru") >= 0 && (n = "ru"), Hs.push("/".concat(n))), L.commit("SET_LANG", n), L.dispatch("LOAD_INITIAL_DATA").then(function () {
        if (!L.state.isInTransition && L.state.navigationAllowed) {
          var n = L.getters.getPageDetails(e.params);
          L.commit("SET_CURRENT_PAGE_ID", n.parentId), L.commit("SET_CURRENT_PAGE_CHILD_ID", n.childId), Hs.app.$eventHub.$emit("navigation", {
            to: e,
            from: t
          }), L.commit("SET_IS_IN_TRANSITION", !0), a()
        }
      })
    });
    var Fs = Hs,
      Bs = {
        getBreakpointsMap: function () {
          var e = getComputedStyle(document.head).getPropertyValue("font-family");
          e = e.replace("/map-to-JSON((", ""), e = e.replace("))/", "");
          var t = e.split(","),
            a = new Map;
          return t.forEach(function (e) {
            var t = e.split(":");
            a.set(t[0].replace(/["]/gi, "").trim(), parseInt(t[1].replace("px", ""), 10))
          }), a
        },
        screenIsUntil: function (e) {
          var t = Bs.getBreakpointsMap();
          return window.innerWidth < t.get(e)
        },
        isTouchDevice: function () {
          return window.matchMedia("(pointer: coarse) and (hover: none)").matches
        },
        setPageMeta: function () {
          var e = "";
          if (L.state.currentPageChildId) {
            var t = L.state.pages[L.state.currentPageId].childrens[L.state.currentPageChildId].structure[L.state.lang],
              a = Object.keys(t),
              n = a.find(function (e) {
                return "ProjectDetails" === t[e].component
              });
            e = t[n].data.name
          } else e = Fs.history.current.name;
          document.title = "Pelizzari Studio â¤ ".concat(e);
          var s = L.state.pages[L.state.currentPageId],
            r = s.description,
            i = s.descriptionEn,
            o = s.descriptionRu,
            l = r;
          "en" === L.state.lang && (l = i), "ru" === L.state.lang && (l = o), document.querySelector('meta[name="description"]').setAttribute("content", l)
        },
        isSafari: function () {
          var e = navigator.userAgent.toLowerCase(),
            t = !1;
          return t = /constructor/i.test(window.HTMLElement) || function (e) {
            return "[object SafariRemoteNotification]" === e.toString()
          }(!window.safari || window.safari.pushNotification), t = t || -1 !== e.indexOf("safari") && !(-1 !== e.indexOf("chrome")) && -1 !== e.indexOf("version/"), t
        },
        isInRect: function (e, t) {
          return e.x > t.left && e.y > t.top && e.x < t.right && e.y < t.bottom
        },
        zeroFill: function (e) {
          return e < 10 ? "0".concat(e) : e
        }
      },
      zs = Bs,
      Gs = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = arguments.length > 2 && void 0 !== arguments[2] && arguments[2],
          n = e.querySelector(".home__filler"),
          s = e.querySelector(".image"),
          r = e.querySelector(".image__actual"),
          i = e.querySelector(".sign"),
          o = e.querySelector(".sign__name"),
          c = e.querySelector(".sign__end"),
          u = e.querySelector(".image__black"),
          d = Array.from(e.querySelectorAll(".image__mask")),
          p = Array.from(e.querySelectorAll(".controls .control")),
          h = Array.from(e.querySelectorAll(".dot-container")),
          m = Array.from(e.querySelectorAll(".counter__current span")),
          f = Array.from(e.querySelectorAll(".counter__total span")),
          v = e.querySelector(".info__circle"),
          _ = zs.isTouchDevice() ? e.querySelector(".info__label-over") : [],
          g = document.querySelector(".filters"),
          b = e.querySelector(".content__title h2");
        new xe(e.querySelector(".content__title h2"), {
          linesClass: "content__title-line",
          wordsClass: "content__title-word",
          charsClass: "content__title-letter"
        }), new xe(e.querySelector(".content__text p"), {
          linesClass: "content__text-line",
          wordsClass: "content__text-word",
          charsClass: "content__text-letter"
        });
        e.querySelector(".image__mask:nth-child(1)").style.backgroundColor = "#131313", e.querySelector(".image__mask:nth-child(2)").style.backgroundColor = "#0d0d0d";
        var y = Array.from(e.querySelectorAll(".content__title-letter")),
          S = Array.from(e.querySelectorAll(".content__text-line")),
          C = new l["d"]({
            paused: t
          });
        return C
        .addLabel("delayed")
        .set(e, {autoAlpha: 1})
        .set(g, {yPercent: 0})
        .set(n, {yPercent: 100})
        .set(s, {scaleX: 1,scaleY: .08,x: -1 * (window.innerWidth / 2 + s.getBoundingClientRect().width)})
        .set([r, i, u].concat(Object(Pe["a"])(d)), {autoAlpha: 0})
        .set(o, {strokeDashoffset: 1 + Math.ceil(o.getTotalLength()),strokeDasharray: 1 + Math.ceil(o.getTotalLength())})
        .set(c, {strokeDashoffset: 1 + Math.ceil(c.getTotalLength()),strokeDasharray: 1 + Math.ceil(c.getTotalLength())})
        .set(y, {yPercent: 100})
        .set(S, {yPercent: 100,autoAlpha: 0})
        .set(p[0], {xPercent: -20,autoAlpha: 0})
        .set(p[1], {xPercent: 20,autoAlpha: 0})
        .set(h, {autoAlpha: 0,scale: 2})
        .set([m[0], f[1]], {yPercent: -100})
        .set([m[1], f[0]], {yPercent: 100})
        .set(v, {autoAlpha: 0,scale: 1.5})
        .set(_, {autoAlpha: 0})
        .set(b, {autoAlpha: 1})
        .to(n, a ? 0 : .8, {yPercent: 0,ease: l["a"].easeInOut}, "delayed")
        .addLabel("start", a ? "delayed" : "delayed+=0.4")
        .to(s, 1.2, {x: 0,ease: l["a"].easeInOut}, "start"), zs.screenIsUntil("ipadP") || 
       C.to(s, .6, {scaleX: 2,ease: l["a"].easeIn}, "start")
        .to(s, .6, {scaleX: 1,ease: l["a"].easeOut}, "start+=0.6"), 
       C.to(s, .6, {scaleY: 1,ease: l["a"].easeInOut}, "start+=0.9")
        .addLabel("expand", "start+=1")
        .staggerFromTo(d, .8, {yPercent: 100,autoAlpha: 1}, {yPercent: 0,ease: l["a"].easeIn}, .2, "expand")
        .to(d, .8, {yPercent: -100,ease: l["a"].easeOut})
        .set(r, {scale: 2,autoAlpha: 1,yPercent: 10}, "-=1")
        .to(r, 1.5, {yPercent: 0,scale: 1,ease: l["a"].easeOut}, "-=1")
        .addLabel("enter-title", "-=2.2")
        .staggerTo(y, .7, {yPercent: 0,ease: l["a"].easeOut}, .04, "enter-title")
        .to(m, .6, {yPercent: 0,ease: l["a"].easeInOut}, "expand-=0.2")
        .to(f, .6, {yPercent: 0,ease: l["a"].easeInOut}, "expand")
        .addLabel("enter-text-and-dots", "start+=1")
        .staggerTo(S, .8, {yPercent: 0,autoAlpha: 1,ease: l["a"].easeInOut}, 0, "enter-text-and-dots")
        .staggerTo(h, .6, {scale: 1,autoAlpha: 1,ease: l["a"].easeInOut,clearProps: "all"}, .2, "enter-text-and-dots")
        .addLabel("enter-arrows", "enter-text-and-dots+=0.8")
        .to(p, .6, {xPercent: 0,autoAlpha: 1,ease: l["a"].easeOut}, "enter-arrows")
        .to(v, .6, {scale: 1,autoAlpha: 1,ease: l["a"].easeInOut}, "enter-arrows")
        .to(_, .6, {autoAlpha: 1,ease: l["a"].easeInOut}, "enter-arrows")
        .set(i, {autoAlpha: 1}, "enter-arrows+=0.2")
        .to(o, .8, {strokeDashoffset: 0,ease: l["a"].easeInOut}, "enter-arrows+=0.2")
        .to(c, .8, {strokeDashoffset: 0,ease: l["a"].easeInOut}, "-=0.4"), C
      },
      Ys = Gs,
      Vs = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = null,
          n = null,
          s = null;
        zs.screenIsUntil("ipadL") ? (a = Array.from(e.querySelectorAll(".project:nth-child(2n + 1)")), n = Array.from(e.querySelectorAll(".project:nth-child(2n)")), s = []) : (a = Array.from(e.querySelectorAll(".project:nth-child(3n + 1)")), n = Array.from(e.querySelectorAll(".project:nth-child(3n + 2)")), s = Array.from(e.querySelectorAll(".project:nth-child(3n)")));
        var r = [a, n, s],
          i = document.querySelector(".filters"),
          o = document.querySelector(".side-headline"),
          c = new l["d"]({
            paused: t
          });
        return c.addLabel("start").set(e, {
          autoAlpha: 1
        }).set(o, {
          xPercent: 100,
          autoAlpha: 1
        }).set(Object(Pe["a"])(r[0]).concat(Object(Pe["a"])(r[1]), Object(Pe["a"])(r[2])), {
          yPercent: 60,
          scaleY: 1.2,
          autoAlpha: 0
        }).set(i, {
          yPercent: 100
        }).to(o, 1, {
          xPercent: 0,
          ease: l["a"].easeInOut
        }, "start").to(r[0], 1, {
          yPercent: 0,
          scaleY: 1,
          autoAlpha: 1,
          transformOrigin: "50% 0%",
          ease: l["a"].easeOut
        }, "start").to(r[1], 1, {
          yPercent: 0,
          scaleY: 1,
          autoAlpha: 1,
          transformOrigin: "50% 0%",
          ease: l["a"].easeOut
        }, "start+=0.3").to(r[2], 1, {
          yPercent: 0,
          scaleY: 1,
          autoAlpha: 1,
          transformOrigin: "50% 0%",
          ease: l["a"].easeOut
        }, "start+=0.6").to(i, 1, {
          autoAlpha: 1,
          yPercent: -100,
          ease: l["a"].easeInOut
        }, "start"), c
      },
      Us = Vs,
      Xs = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = document.querySelector(".side-headline"),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel("start").set(e, {
          autoAlpha: 1
        }).set(a, {
          xPercent: 100,
          autoAlpha: 1
        }).to(a, 1, {
          xPercent: 0,
          ease: l["a"].easeInOut
        }, "start"), n
      },
      Ws = Xs,
      Zs = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = document.querySelector(".side-headline"),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel().set(e, {
          autoAlpha: 1
        }).set(a, {
          xPercent: 100,
          autoAlpha: 1
        }).to(a, 1, {
          xPercent: 0,
          ease: l["a"].easeInOut
        }, "start"), n
      },
      Qs = Zs,
      Ks = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = document.querySelector(".side-headline"),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel("start").set(e, {
          autoAlpha: 1
        }).set(a, {
          xPercent: 100,
          autoAlpha: 1
        }).to(a, 1, {
          xPercent: 0,
          ease: l["a"].easeInOut
        }, "start"), n
      },
      Js = Ks,
      er = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = e.querySelector(".hero__image"),
          n = e.querySelector(".hero__image-mask"),
          s = document.querySelector(".side-headline"),
          r = e.querySelector(".scroll-down"),
          i = new l["d"]({
            paused: t
          });
        return i.addLabel("start").set(e, {
          autoAlpha: 1
        }).set(a, {
          scale: 1.6,
          y: 60
        }).set(n, {
          yPercent: 0
        }).set(s, {
          xPercent: 100,
          autoAlpha: 1
        }).set(r, {
          autoAlpha: 0
        }).to(a, 1, {
          scale: 1.2,
          y: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeInOut
        }, "start").to(n, 1, {
          yPercent: -100,
          ease: l["a"].easeInOut
        }, "start").to(s, 1, {
          xPercent: 0,
          ease: l["a"].easeInOut
        }, "start").to(r, .6, {
          autoAlpha: 1,
          ease: l["a"].easeInOut
        }, "start+=0.4"), i
      },
      tr = er,
      ar = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = e.querySelector(".home__filler"),
          n = e.querySelector(".image"),
          s = e.querySelector(".image__actual"),
          r = e.querySelector(".sign"),
          i = Array.from(e.querySelectorAll(".image__mask")),
          o = Array.from(e.querySelectorAll(".controls .control")),
          c = Array.from(e.querySelectorAll(".dot-container")),
          u = Array.from(e.querySelectorAll(".counter__current span")),
          d = Array.from(e.querySelectorAll(".counter__total span")),
          p = e.querySelector(".info__circle"),
          h = e.querySelector(".info__label-over");
        e.querySelector(".image__mask:nth-child(1)").style.backgroundColor = "#131313", e.querySelector(".image__mask:nth-child(2)").style.backgroundColor = "#0d0d0d";
        var m = Array.from(e.querySelectorAll(".content__title-letter")),
          f = Array.from(e.querySelectorAll(".content__text-line")).reverse(),
          v = new l["d"]({
            paused: t
          });
        return v.addLabel("start")
        .to(r, .8, {autoAlpha: 0,ease: l["a"].easeInOut}, "start")
        .staggerTo(m, .8, {yPercent: 100,ease: l["a"].easeInOut}, .04, "start")
        .staggerTo(f, .8, {autoAlpha: 0,yPercent: 100,ease: l["a"].easeInOut}, .05, "start")
        .staggerTo(c, .6, {scale: 2,autoAlpha: 0,ease: l["a"].easeInOut}, .2, "start")
        .to(o[0], .6, {xPercent: -20,autoAlpha: 0,ease: l["a"].easeOut}, "start")
        .to(o[1], .6, {xPercent: 20,autoAlpha: 0,ease: l["a"].easeInOut}, "start")
        .to(p, .6, {scale: 1.5,autoAlpha: 0,ease: l["a"].easeInOut}, "start")
        .to(h, .6, {autoAlpha: 0,ease: l["a"].easeInOut}, "start")
        .to(u[0], .6, {yPercent: -100,ease: l["a"].easeInOut}, "start+=0.2")
        .to(d[1], .6, {yPercent: -100,ease: l["a"].easeInOut}, "start")
        .to(u[1], .6, {yPercent: 100,ease: l["a"].easeInOut}, "start+=0.2")
        .to(d[0], .6, {yPercent: 100,ease: l["a"].easeInOut}, "start")
        .staggerTo(i, .8, {yPercent: 0,ease: l["a"].easeIn}, .2, "start")
        .to(i, .8, {yPercent: 100,ease: l["a"].easeOut}, "start+=0.8")
        .set(s, {autoAlpha: 0}, "-=0.8")
        .to(n, .6, {scaleY: 0,ease: l["a"].easeInOut}, "-=0.4")
        .to(s, 1.5, {yPercent: 10,scale: 2,ease: l["a"].easeIn}, "start")
        .to(a, .8, {yPercent: -100,ease: l["a"].easeInOut}, "-=0.2"), v
      },
      nr = ar,
      sr = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = null,
          n = null,
          s = null;
        zs.screenIsUntil("ipadL") ? (a = Array.from(e.querySelectorAll(".project:nth-child(2n + 1)")), n = Array.from(e.querySelectorAll(".project:nth-child(2n)")), s = []) : (a = Array.from(e.querySelectorAll(".project:nth-child(3n + 1)")), n = Array.from(e.querySelectorAll(".project:nth-child(3n + 2)")), s = Array.from(e.querySelectorAll(".project:nth-child(3n)")));
        var r = [a, n, s],
          i = document.querySelector(".filters"),
          o = document.querySelector(".side-headline"),
          c = new l["d"]({
            paused: t
          });
        return c.addLabel("start").to(o, 1, {
          xPercent: 100,
          ease: l["a"].easeInOut
        }, "start").to(r[2], 1, {
          yPercent: 60,
          scaleY: 1.2,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeIn
        }, "start").to(r[1], 1, {
          yPercent: 60,
          scaleY: 1.2,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeIn
        }, "start+=0.3").to(r[0], 1, {
          yPercent: 60,
          scaleY: 1.2,
          autoAlpha: 0,
          transformOrigin: "50% 0%",
          ease: l["a"].easeIn
        }, "start+=0.6").to(i, 1, {
          autoAlpha: 0,
          yPercent: -100,
          ease: l["a"].easeInOut
        }, "start"), c
      },
      rr = sr,
      ir = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = document.querySelector(".side-headline"),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel("start").to(a, 1, {
          xPercent: 100,
          ease: l["a"].easeInOut
        }, "start"), n
      },
      or = ir,
      lr = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = document.querySelector(".side-headline"),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel("start").to(a, 1, {
          xPercent: 100,
          ease: l["a"].easeInOut
        }, "start"), n
      },
      cr = lr,
      ur = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = document.querySelector(".side-headline"),
          n = new l["d"]({
            paused: t
          });
        return n.addLabel("start").to(a, 1, {
          xPercent: 100,
          ease: l["a"].easeInOut
        }, "start"), n
      },
      dr = ur,
      pr = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = e.querySelector(".hero__image"),
          n = e.querySelector(".hero__image-mask"),
          s = document.querySelector(".side-headline"),
          r = e.querySelector(".scroll-down"),
          i = new l["d"]({
            paused: t
          });
        return i.addLabel("start").set(n, {
          yPercent: 100
        }).to(a, 1, {
          scale: 1.4,
          y: -60,
          ease: l["a"].easeInOut
        }, "start").to(n, 1, {
          yPercent: 0,
          ease: l["a"].easeInOut
        }, "start").to(s, 1, {
          xPercent: 100,
          ease: l["a"].easeInOut
        }, "start").to(r, .6, {
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start"), i
      },
      hr = pr,
      mr = function (e) {
        var t = Array.from(document.querySelectorAll(".loader__headline span")),
          a = document.querySelector(".loader__line"),
          n = document.querySelector(".loader"),
          s = new l["d"]({
            paused: !0
          });
        return s.addLabel("start").set(e, {autoAlpha: 0})
        .staggerTo(t, 1, {y: 0,autoAlpha: 1,ease: l["a"].easeInOut}, .1, "start")
        .to(a, 1.6, {scaleX: 1,transformOrigin: "0% 50%",ease: l["a"].easeInOut}, "-=0.8")
        .addLabel("collapse")
        .to(a, 1, {top: 0,ease: l["a"].easeInOut}, "collapse")
        .set(e, {autoAlpha: 1})
        .set(n, {autoAlpha: 0,display: "none"}), s
      },
      fr = mr,
      vr = function (e) {
        var t = !(arguments.length > 1 && void 0 !== arguments[1]) || arguments[1],
          a = e.querySelector(".black-mask__filler"),
          n = e.querySelector(".black-mask__inner"),
          s = document.querySelector(".side-headline"),
          r = e.querySelector("footer .container"),
          i = new l["d"]({
            paused: t
          });
        return i.addLabel("start").to(r, .8, {
          autoAlpha: 0,
          ease: l["a"].easeInOut
        }, "start").to(a, .8, {
          width: window.innerWidth,
          ease: l["a"].easeOut
        }, "start").set(n, {
          autoAlpha: 0
        }).to(a, .8, {
          height: window.innerHeight,
          ease: l["a"].easeInOut
        }, "-=0.2").to(a, .8, {
          yPercent: -100,
          ease: l["a"].easeInOut
        }, "-=0.2").to(s, 1, {
          xPercent: 100,
          ease: l["a"].easeInOut
        }, "start"), i
      },
      _r = vr,
      gr = new Map;
    gr.set("loader", fr), gr.set("home-in", Ys), gr.set("portfolio-in", Us), gr.set("studio-in", Ws), gr.set("press-in", Qs), gr.set("contact-in", Js), gr.set("project-in", tr), gr.set("home-out", nr), gr.set("portfolio-out", rr), gr.set("studio-out", or), gr.set("press-out", cr), gr.set("contact-out", dr), gr.set("project-out", hr), gr.set("project-next-out", _r);
    var br = function (e, t) {
        var a = arguments.length > 2 && void 0 !== arguments[2] && arguments[2];
        return new Promise(function (n) {
          var s = a ? gr.get("loader")(e).play() : [],
            r = "".concat(e.dataset.transition, "-in"),
            i = gr.get(r)(e, !1, a),
            o = new l["d"]({
              paused: !0
            });
          o.addLabel("start").add(s).add(i).eventCallback("onComplete", function () {
            t.$eventHub.$emit("route-enter-done"), ua.start(), n()
          }).play()
        })
      },
      yr = function (e, t) {
        return new Promise(function (a) {
          ua.pause();
          var n = "".concat(e.dataset.transition, "-out"),
            s = gr.get(n)(e, !1),
            r = ua.animateOut(),
            i = new l["d"]({
              paused: !0
            });
          i.addLabel("start").add(s, "start"), r.reverse().forEach(function (e, t) {
            i.add(e, "start+=".concat(.2 * t))
          }), i.eventCallback("onComplete", function () {
            t.$eventHub.$emit("route-leave-done"), a()
          }).play()
        })
      },
      Sr = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          ref: "loader",
          staticClass: "component loader",
          class: {
            negative: e.$store.state.pageNegative
          }
        }, [a("div", {
          staticClass: "loader__headline",
          domProps: {
            innerHTML: e._s(e.$translate("LOADER_HEADLINE"))
          }
        }), a("div", {
          staticClass: "loader__line"
        })])
      },
      Cr = [],
      Er = {
        name: "Loader",
        data: function () {
          return {}
        },
        mounted: function () {
          var e = this;
          this.$nextTick(function () {
            var t = Array.from(e.$el.querySelectorAll(".loader__headline span")),
              a = e.$el.querySelector(".loader__line");
            l["e"].set(t, {
              y: 20,
              autoAlpha: 0
            }), l["e"].set(a, {
              scaleX: 0
            })
          })
        }
      },
      Or = Er,
      Ar = (a("5160"), a("f17f"), Object(Lt["a"])(Or, Sr, Cr, !1, null, "0468f430", null));
    Ar.options.__file = "Loader.vue";
    var Ir = Ar.exports,
      wr = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("header", {
          class: {
            negative: e.$store.state.header.negative, filled: e.$store.state.header.filled, hidden: e.$store.state.header.hidden, visible: e.$store.state.isLoaderMounted
          },
          on: {
            mouseenter: e.revealFiller,
            mouseleave: e.hideFiller
          }
        }, [a("div", {
          staticClass: "header__filler"
        }, [a("div", {
          ref: "filler",
          staticClass: "header__filler-inner"
        })]), a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "header__content"
        }, [a("div", {
          staticClass: "logo",
          attrs: {
            "data-trapped-mode": "logo"
          },
          on: {
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            }
          }
        }, [a("router-link", {
          attrs: {
            to: "/" + e.$store.state.lang
          }
        }, [a("svg", {
          attrs: {
            viewBox: "0 0 213.9 14",
            "xml:space": "preserve"
          }
        }, [a("path", {
          staticClass: "logo__letter",
          attrs: {
            d: "M4.2,6.2c1.4,0,2.1-0.7,2.1-2l0,0c0-1.4-0.8-1.9-2.1-1.9H2.8v4L4.2,6.2L4.2,6.2z M0,0h4.3\n              C7.3,0,9,1.4,9,4.1v0.1C9,7,7.2,8.3,4.4,8.3H2.8V13H0V0z"
          }
        }), a("polygon", {
          staticClass: "logo__letter",
          attrs: {
            points: "14,0 22.7,0 22.7,2.3 17,2.3 17,5.3 21.6,5.3 21.6,7.5 17,7.5 17,10.7 23,10.7 23,13 14,13 "
          }
        }), a("polygon", {
          staticClass: "logo__letter",
          attrs: {
            points: "28,0 30.9,0 30.9,10.7 36,10.7 36,13 28,13 "
          }
        }), a("polygon", {
          staticClass: "logo__letter",
          attrs: {
            points: "40,13 43,13 43,0 40,0 "
          }
        }), a("polygon", {
          staticClass: "logo__letter",
          attrs: {
            points: "48,12.6 54,2.3 48.7,2.3 48.7,0 58,0 58,0.3 52.1,10.7 57.8,10.7 57.8,13 48,13 "
          }
        }), a("polygon", {
          staticClass: "logo__letter",
          attrs: {
            points: "62,12.6 68,2.3 62.7,2.3 62.7,0 72,0 72,0.3 66.1,10.7 71.8,10.7 71.8,13 62,13 "
          }
        }), a("path", {
          staticClass: "logo__letter",
          attrs: {
            d: "M80.2,8h3.3l-1.7-5.6L80.2,8z M80.2,0h3.7L88,13h-3l-0.9-2.9h-4.6L78.7,13H76L80.2,0z"
          }
        }), a("path", {
          staticClass: "logo__letter",
          attrs: {
            d: "M96.4,6c1.4,0,2.1-0.6,2.1-1.9V4c0-1.3-0.8-1.8-2.1-1.8h-1.6V6H96.4z M92,0h4.5 c2.9,0,4.8,1.3,4.8,3.9V4c0,2-1.1,3-2.6,3.5L102,13h-3l-3-5h-1.2v5H92V0z"
          }
        }), a("polygon", {
          staticClass: "logo__letter",
          attrs: {
            points: "107,13 110,13 110,0 107,0 "
          }
        }), a("path", {
          staticClass: "logo__letter",
          attrs: {
            d: "M122,9.5h2.7c0.1,1.2,0.6,2.2,2.5,2.2c1.2,0,2.1-0.7,2.1-1.8s-0.5-1.5-2.4-1.8 c-3.2-0.5-4.5-1.5-4.5-4.1c0-2.3,1.8-4.1,4.6-4.1c2.8,0,4.5,1.4,4.7,4.1H129c-0.2-1.2-0.8-1.8-2.1-1.8c-1.2,0-1.9,0.6-1.9,1.5 c0,1,0.4,1.4,2.3,1.7c3,0.4,4.6,1.3,4.6,4.1c0,2.4-1.8,4.3-4.8,4.3C123.7,14,122.2,12.2,122,9.5"
          }
        }), a("polygon", {
          staticClass: "logo__letter",
          attrs: {
            points: "138.5,2.3 135,2.3 135,0 145,0 145,2.3 141.5,2.3 141.5,13 138.5,13 "
          }
        }), a("path", {
          staticClass: "logo__letter",
          attrs: {
            d: "M150,7.8V0h3v7.6c0,2.1,0.6,3.1,2.5,3.1c1.8,0,2.5-0.8,2.5-3.2V0h3v7.7c0,3.5-2,5.3-5.6,5.3 C152,13,150,11.2,150,7.8"
          }
        }), a("path", {
          staticClass: "logo__letter",
          attrs: {
            d: "M170.1,10.7c2.6,0,3.8-1.5,3.8-4.2V6.4c0-2.7-1.1-4.1-3.9-4.1h-1.2v8.4H170.1z M166,0h4.2 c4.5,0,6.8,2.5,6.8,6.4v0.1c0,3.9-2.3,6.5-6.8,6.5H166V0z"
          }
        }), a("polygon", {
          staticClass: "logo__letter",
          attrs: {
            points: "182,13 185,13 185,0 182,0 "
          }
        }), a("path", {
          staticClass: "logo__letter",
          attrs: {
            d: "M200,7L200,7c0-2.7-1.3-4.6-3.5-4.6S193,4.1,193,6.9v0.2c0,2.8,1.5,4.5,3.5,4.5 C198.7,11.5,200,9.8,200,7 M190,7.1V6.9c0-4.1,2.8-6.9,6.5-6.9c3.8,0,6.5,2.8,6.5,6.8V7c0,4.1-2.6,7-6.5,7 C192.5,14,190,11.1,190,7.1"
          }
        }), a("path", {
          staticClass: "logo__letter",
          attrs: {
            d: "M207.2,3.3c0-1.9,1.5-3.3,3.4-3.3l0,0c1.8,0,3.3,1.6,3.3,3.4s-1.6,3.3-3.4,3.3C208.7,6.6,207.3,5.1,207.2,3.3 L207.2,3.3z M213.4,3.3c0-1.5-1.2-2.8-2.8-2.8c-1.5,0-2.8,1.2-2.8,2.8s1.2,2.8,2.8,2.8S213.4,4.9,213.4,3.3z M209.4,1.7h1.3 c0.9,0,1.3,0.3,1.3,1l0,0c0,0.4-0.2,0.8-0.6,0.9l0.8,1.3h-1l-0.7-1.2h-0.3v1.2h-0.9L209.4,1.7z M210.6,3.2c0.3,0,0.5-0.1,0.5-0.4 l0,0c0-0.3-0.2-0.4-0.5-0.4h-0.3v0.8C210.3,3.2,210.6,3.2,210.6,3.2z"
          }
        })])])], 1), a("nav", {
          staticClass: "desktop-menu"
        }, [a("div", {
          staticClass: "menu"
        }, [a("MenuLink", {
          staticClass: "first",
          attrs: {
            label: e.$translate("PORTFOLIO"),
            to: "" + e.$store.getters.getFirstLevelPageUrl("portfolio")
          }
        }), a("MenuLink", {
          attrs: {
            label: e.$translate("STUDIO"),
            to: "" + e.$store.getters.getFirstLevelPageUrl("studio")
          }
        }), a("MenuLink", {
          attrs: {
            label: e.$translate("PRESS"),
            to: "" + e.$store.getters.getFirstLevelPageUrl("press")
          }
        }), a("MenuLink", {
          staticClass: "last",
          attrs: {
            label: e.$translate("CONTACTS"),
            to: "" + e.$store.getters.getFirstLevelPageUrl("contact")
          }
        })], 1)]), a("div", {
          staticClass: "mobile-menu__icon"
        }, [a("div", {
          staticClass: "mobile-menu__icon-line"
        }), a("div", {
          staticClass: "mobile-menu__icon-line"
        }), a("div", {
          staticClass: "mobile-menu__icon-cliccable",
          on: {
            click: function (t) {
              e.mobileOpen = !e.mobileOpen
            }
          }
        })])])]), a("transition", {
          attrs: {
            css: !1
          },
          on: {
            enter: e.mobileMenuEnter,
            leave: e.mobileMenuLeave
          }
        }, [e.mobileOpen ? a("nav", {
          staticClass: "mobile-menu"
        }, [a("div", {
          staticClass: "mobile-menu__menu"
        }, [a("MenuLink", {
          attrs: {
            label: e.$translate("PORTFOLIO"),
            to: "" + e.$store.getters.getFirstLevelPageUrl("portfolio"),
            "has-dot": !0
          }
        }), a("MenuLink", {
          attrs: {
            label: e.$translate("STUDIO"),
            to: "" + e.$store.getters.getFirstLevelPageUrl("studio"),
            "has-dot": !0
          }
        }), a("MenuLink", {
          attrs: {
            label: e.$translate("PRESS"),
            to: "" + e.$store.getters.getFirstLevelPageUrl("press"),
            "has-dot": !0
          }
        }), a("MenuLink", {
          attrs: {
            label: e.$translate("CONTACTS"),
            to: "" + e.$store.getters.getFirstLevelPageUrl("contact"),
            "has-dot": !0
          }
        })], 1), a("div", {
          staticClass: "mobile-menu__footer"
        }, [a("div", {
          staticClass: "mobile-menu__footer-langs"
        }, [a("div", {
          staticClass: "mobile-menu__footer-lang",
          class: {
            active: "it" === e.$store.state.lang
          }
        }, [a("a", {
          attrs: {
            href: "/it"
          }
        }, [a("span", [e._v("ItÂ .Â ")])])]), a("div", {
          staticClass: "mobile-menu__footer-lang",
          class: {
            active: "en" === e.$store.state.lang
          }
        }, [a("a", {
          attrs: {
            href: "/en"
          }
        }, [a("span", [e._v("EnÂ .Â ")])])])]), a("div", {
          staticClass: "mobile-menu__footer-socials"
        }, [a("a", {
          attrs: {
            href: "https://www.facebook.com/pelizzaristudio/",
            target: "_blank",
            rel: "noopener"
          }
        }, [a("div", {
          staticClass: "mobile-menu__footer-social facebook"
        })]), a("a", {
          attrs: {
            href: "https://www.instagram.com/pelizzaristudio/",
            target: "_blank",
            rel: "noopener"
          }
        }, [a("div", {
          staticClass: "mobile-menu__footer-social instagram"
        })])])])]) : e._e()])], 1)
      },
      xr = [],
      Tr = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "menu__link",
          attrs: {
            "data-trapped-mode": "menu-link"
          },
          on: {
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            }
          }
        }, [a("router-link", {
          attrs: {
            to: "/" + e.$store.state.lang + e.to,
            title: e.label
          }
        }, [a("span", [e._v(e._s(e.label))]), e.hasDot ? a("div", {
          staticClass: "dot"
        }) : e._e()])], 1)
      },
      $r = [],
      Lr = {
        name: "MenuLink",
        props: ["label", "to", "has-dot"]
      },
      kr = Lr,
      Pr = (a("f619"), Object(Lt["a"])(kr, Tr, $r, !1, null, "687ea2e8", null));
    Pr.options.__file = "MenuLink.vue";
    var Dr = Pr.exports,
      Rr = {
        name: "TopHeader",
        components: {
          MenuLink: Dr
        },
        data: function () {
          return {
            mobileOpen: !1,
            id: null,
            isMenuHidden: !1
          }
        },
        mounted: function () {
          var e = this;
          this.$eventHub.$on("navigation", function () {
            e.mobileOpen = !1
          }), this.id = R.add(this.scrollStarted)
        },
        methods: {
          mobileMenuEnter: function (e, t) {
            L.commit("SET_HEADER_NEGATIVE", !0), bt.get("mobile-menu")(e, !1).eventCallback("onComplete", t).play(), L.commit("SET_IS_MOBILE_MENU_OPEN", !0)
          },
          mobileMenuLeave: function (e, t) {
            var a = this;
            bt.get("mobile-menu")(e, !0).eventCallback("onReverseComplete", function () {
              "Home" !== a.$route.name && L.commit("SET_HEADER_NEGATIVE", !1), L.commit("SET_IS_MOBILE_MENU_OPEN", !1), t()
            }).progress(1).reverse()
          },
          scrollStarted: function () {
            if (!zs.isTouchDevice() || !this.mobileOpen) {
              var e = this.$root.$children[0].scroll,
                t = e.nonLerpDirection,
                a = e.amount,
                n = this.$el.querySelector(".header__content");
              "down" === t && a > 0 ? (l["e"].to(n, 1.2, {
                delay: .3,
                yPercent: -100,
                ease: l["b"].easeOut
              }), this.isMenuHidden = !0, a > 0 && L.commit("SET_HEADER_FILLED", !1)) : (l["e"].to(n, 1.2, {
                delay: .3,
                yPercent: 0,
                ease: l["b"].easeOut
              }), this.isMenuHidden = !1, zs.isTouchDevice() && a > 0 && L.commit("SET_HEADER_FILLED", !0));
              var s = this.$root.$children[0].mouse,
                r = s.x,
                i = s.y,
                o = this.$el.querySelector(".header__filler-inner");
              !zs.isInRect({
                x: r,
                y: i
              }, o.getBoundingClientRect()) && L.state.isInHeader && L.commit("SET_IS_IN_HEADER", !1)
            }
          },
          revealFiller: function () {
            if (!zs.isTouchDevice()) {
              var e = this.$root.$children[0].scroll.amount;
              e <= 0 || "Home" !== this.$route.name && (this.isMenuHidden || (L.commit("SET_HEADER_FILLED", !0), L.commit("SET_IS_IN_HEADER", !0)))
            }
          },
          hideFiller: function () {
            if (!zs.isTouchDevice()) {
              var e = this.$root.$children[0].scroll.amount;
              e <= 0 && !L.state.isInHeader && !L.state.header.filled || "Home" !== this.$route.name && (this.isMenuHidden || (L.commit("SET_HEADER_FILLED", !1), L.commit("SET_IS_IN_HEADER", !1)))
            }
          }
        }
      },
      Nr = Rr,
      qr = (a("e934"), Object(Lt["a"])(Nr, wr, xr, !1, null, "5a51142d", null));
    qr.options.__file = "TopHeader.vue";
    var Mr = qr.exports,
      jr = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "side-headline"
        }, [a("div", {
          staticClass: "headline__anchor"
        }, [a("div", {
          ref: "headline",
          staticClass: "headline"
        }, [a("h1", [e._v(e._s(e.$store.state.headline.content))])])])])
      },
      Hr = [],
      Fr = {
        name: "SideHeadline",
        data: function () {
          return {
            translateAmount: 0
          }
        },
        mounted: function () {
          var e = this;
          l["e"].set(this.$el, {
            autoAlpha: 0
          }), R.add(this.renderHeadline), this.$eventHub.$on("route-leave-done", function () {
            l["e"].set(e.$el, {
              autoAlpha: 0
            })
          })
        },
        methods: {
          renderHeadline: function () {
            var e = this.$root.$children[0];
            if ("undefined" !== typeof e.$refs.view) {
              var t = this.$refs.headline.getBoundingClientRect().height,
                a = e.scroll,
                n = a.amount,
                s = a.lerpedAmount,
                r = e.$refs.view.$el.getBoundingClientRect().height,
                i = window.innerHeight,
                o = Math.abs(i - t),
                c = o / (r - i);
              this.translateAmount = -1 * o + c * n;
              var u = L.state.headline.firstPaint;
              l["e"].killTweensOf(this.$refs.headline), l["e"].to(this.$refs.headline, u ? 0 : 2, {
                x: this.translateAmount,
                ease: l["b"].easeOut,
                onComplete: function () {
                  u && L.commit("REMOVE_HEADLINE_FIRST_PAINT")
                }
              }), s < window.innerHeight && null !== L.state.currentPageChildId ? l["e"].set(this.$el, {
                mixBlendMode: "unset"
              }) : l["e"].set(this.$el, {
                mixBlendMode: "screen"
              })
            }
          }
        }
      },
      Br = Fr,
      zr = (a("c2cb"), Object(Lt["a"])(Br, jr, Hr, !1, null, "79a283c4", null));
    zr.options.__file = "SideHeadline.vue";
    var Gr = zr.exports,
      Yr = function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          ref: "filters",
          staticClass: "filters"
        }, [a("div", {
          staticClass: "container"
        }, [a("div", {
          staticClass: "filters__filler"
        }, [a("div", {
          staticClass: "filters__line"
        }), a("div", {
          staticClass: "filters__label"
        }, [a("span", {
          domProps: {
            innerHTML: e._s(e.$translate("FILTERS_LABEL"))
          }
        })]), a("router-link", {
          staticClass: "filter",
          class: {
            active: "#all" === e.hash || "" === e.hash
          },
          attrs: {
            to: "#all"
          }
        }, [a("div", {
          staticClass: "filter__inner",
          attrs: {
            "data-trapped-mode": "filter-label"
          },
          on: {
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            }
          }
        }, [a("span", [e._v(e._s(e.$translate("FILTERS_ALL")))])])]), a("router-link", {
          staticClass: "filter",
          class: {
            active: "#residential" === e.hash
          },
          attrs: {
            to: "#residential"
          }
        }, [a("div", {
          staticClass: "filter__inner",
          attrs: {
            "data-trapped-mode": "filter-label"
          },
          on: {
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            }
          }
        }, [a("span", [e._v(e._s(e.$translate("FILTERS_RESIDENZE_PRIVATE")))])])]), a("router-link", {
          staticClass: "filter",
          class: {
            active: "#retail" === e.hash
          },
          attrs: {
            to: "#retail"
          }
        }, [a("div", {
          staticClass: "filter__inner",
          attrs: {
            "data-trapped-mode": "filter-label"
          },
          on: {
            mouseover: function (t) {
              e.$eventHub.$emit("pointerin", t)
            },
            mouseout: function (t) {
              e.$eventHub.$emit("pointerout", t)
            }
          }
        }, [a("span", [e._v(e._s(e.$translate("FILTERS_OSPITALITY_AND_RETAIL")))])])])], 1)])])
      },
      Vr = [],
      Ur = {
        name: "PortfolioFilters",
        data: function () {
          return {
            hash: document.location.hash,
            isHidden: !1
          }
        },
        mounted: function () {
          var e = this;
          this.$eventHub.$on("scroll-started", this.scrollStarted), this.$eventHub.$on("scroll-stopped", this.scrollStopped), this.$eventHub.$on("route-leave-done", function () {
            l["e"].set(e.$el, {
              autoAlpha: 0
            })
          }), this.$eventHub.$on("route-leave-done", function () {
            e.hash = document.location.hash
          })
        },
        beforeDestroy: function () {
          this.$eventHub.$off("scroll-started", this.scrollStarted), this.$eventHub.$off("scroll-stopped", this.scrollStopped)
        },
        methods: {
          scrollStarted: function () {
            var e = this.$refs.filters;
            l["e"].to(e, 1.2, {
              delay: .3,
              y: this.$el.getBoundingClientRect().height,
              ease: l["b"].easeOut
            })
          },
          scrollStopped: function () {
            var e = this.$refs.filters;
            l["e"].to(e, 1.2, {
              delay: .3,
              y: 0,
              ease: l["b"].easeOut
            })
          }
        }
      },
      Xr = Ur,
      Wr = (a("2fa6"), a("cb02"), Object(Lt["a"])(Xr, Yr, Vr, !1, null, "7b979263", null));
    Wr.options.__file = "PortfolioFilters.vue";
    var Zr = Wr.exports,
      Qr = function () {
        var e = this,
          t = e.$createElement;
        e._self._c;
        return e._m(0)
      },
      Kr = [function () {
        var e = this,
          t = e.$createElement,
          a = e._self._c || t;
        return a("div", {
          staticClass: "component landscape-error"
        }, [a("div", {
          staticClass: "container"
        }, [a("span", [e._v("Please turn your phone")])])])
      }],
      Jr = {
        name: "LandscapeError"
      },
      ei = Jr,
      ti = (a("9a1e"), Object(Lt["a"])(ei, Qr, Kr, !1, null, "4a5c95ac", null));
    ti.options.__file = "LandscapeError.vue";
    var ai = ti.exports,
      ni = a("d2d0"),
      si = a.n(ni),
      ri = {
        name: "app",
        components: {
          Loader: Ir,
          TopHeader: Mr,
          SideHeadline: Gr,
          PortfolioFilters: Zr,
          LandscapeError: ai,
          CookieLaw: si.a
        },
        data: function () {
          return {
            virtualScroll: null,
            isNavigating: !1,
            scroll: {
              amount: 0,
              lerpedAmount: 0,
              prevAmount: 0,
              prevLerpedAmount: 0,
              strength: 1,
              lerpedStrength: 1,
              direction: "down",
              nonLerpDirection: "down",
              isImediate: !1,
              prevStartStopAmount: 0,
              stoppedNeedDispatch: !1,
              startedNeedDispatch: !0
            },
            mouse: {
              x: 0,
              y: 0,
              lerpedX: 0,
              lerpedY: 0,
              baseFill: "#1E1E1E",
              baseFillNegative: "white",
              baseStroke: "rgba(0, 0, 0, 0)",
              baseStrokeNegative: "rgba(0, 0, 0, 0)",
              lerpedScale: 0,
              lerpedRadius: 0,
              firstPaint: !0,
              trapped: !1,
              trappedElement: null
            }
          }
        },
        created: function () {
          u.a.enable()
        },
        mounted: function () {
          var e = this;
          this.virtualScroll = new o.a({
            passive: !0,
            mouseMultiplier: .3,
            touchMultiplier: 2.5,
            firefoxMultiplier: 80
          }), this.virtualScroll.on(this.updateScroll), this.virtualScroll.on(this.checkScrollEvents), this.$el.addEventListener("mousemove", this.updateMouse), R.add(this.renderScroll), zs.isTouchDevice() || R.add(this.renderMouse), R.add(this.checkScrollStartAndStop, "scroll-statart-stop", !0), R.start(), this.$eventHub.$on("pointerin", this.pointerIn), this.$eventHub.$on("pointerout", this.pointerOut), this.$eventHub.$on("route-leave-done", this.betweenNavigations), this.$eventHub.$on("navigation", function () {
            e.isNavigating = !0, e.$el.querySelector(".view-container").style.pointerEvents = "none"
          }), this.$eventHub.$on("route-enter-done", function () {
            e.isNavigating = !1, e.$el.querySelector(".view-container").style.pointerEvents = "auto"
          }), zs.isTouchDevice() && zs.screenIsUntil("ipadL") && zs.isSafari() && (document.body.style.height = "".concat(window.innerHeight, "px"), document.body.classList.add("ios--fix"))
        },
        methods: {
          updateScroll: function (e) {
            if (!this.isNavigating && ("keydown" !== e.originalEvent.type || 32 !== e.originalEvent.keyCode) && "undefined" !== typeof this.$refs.view && !L.state.isInHeader && (!zs.isTouchDevice() || !L.state.isMobileMenuOpen)) {
              var t = this.$refs.view.$el.getBoundingClientRect().height,
                a = t - window.innerHeight,
                n = -1 * e.deltaY,
                s = this.scroll.amount + n;
              this.scroll.amount = Math.max(Math.min(s, a), 0);
              var r = Math.abs(this.scroll.amount - this.scroll.prevAmount);
              this.scroll.direction = this.scroll.lerpedAmount - this.scroll.prevLerpedAmount > 0 ? "down" : "up", this.scroll.nonLerpDirection = this.scroll.amount - this.scroll.prevAmount > 0 ? "down" : "up", this.scroll.prevAmount = this.scroll.amount;
              var i = Math.min(r, 90) / 100;
              this.scroll.strength = 1 + .6 * i
            }
          },
          forceScroll: function (e) {
            var t = arguments.length > 1 && void 0 !== arguments[1] && arguments[1];
            this.scroll.isImediate = t, this.scroll.amount = e
          },
          checkScrollEvents: function (e) {
            zs.isTouchDevice() || "Home" !== this.$route.name || (e.deltaY > 0 ? this.$eventHub.$emit("slide-next") : this.$eventHub.$emit("slide-prev"))
          },
          updateMouse: function (e) {
            this.mouse.x = e.clientX, this.mouse.y = e.clientY
          },
          renderScroll: function () {
            if ("undefined" !== typeof this.$refs.view) {
              var e = this.$refs.view.$el;
              l["e"].killTweensOf(this.scroll), l["e"].to(this.scroll, this.scroll.isImediate ? 0 : 2, {
                lerpedAmount: -1 * this.scroll.amount,
                lerpedStrength: this.scroll.strength,
                ease: l["b"].easeOut
              }), l["e"].set(e, {
                y: this.scroll.lerpedAmount
              }), this.scroll.prevLerpedAmount = this.scroll.lerpedAmount, this.scroll.isImediate = !1
            }
          },
          checkScrollStartAndStop: function () {
            this.scroll.amount.toFixed(2) === this.scroll.prevStartStopAmount.toFixed(2) ? this.scroll.stoppedNeedDispatch && (this.$eventHub.$emit("scroll-stopped"), L.commit("SET_IS_SCROLLING", !1), this.scroll.stoppedNeedDispatch = !1, this.scroll.startedNeedDispatch = !0) : this.scroll.startedNeedDispatch && (this.$eventHub.$emit("scroll-started"), L.commit("SET_IS_SCROLLING", !0), this.scroll.startedNeedDispatch = !1, this.scroll.stoppedNeedDispatch = !0), this.scroll.prevStartStopAmount = this.scroll.amount
          },
          renderMouse: function () {
            var e = this.$refs.mouseDot;
            if (this.mouse.trapped) J.get(this.mouse.trapped)(e, this, this.mouse);
            else {
              l["e"].to(this.mouse, this.mouse.firstPaint ? 0 : 1, {
                lerpedX: this.mouse.x,
                lerpedY: this.mouse.y,
                ease: l["c"].ease.config(.1, .7, !1),
                delay: .2
              }), l["e"].set(e, {
                x: this.mouse.lerpedX,
                y: this.mouse.lerpedY,
                transformOrigin: "50% 50%"
              });
              var t = L.state.pageNegative;
              l["e"].to(e, 1, {
                fill: t ? this.mouse.baseFillNegative : this.mouse.baseFill,
                stroke: this.mouse.baseStroke,
                ease: l["b"].easeOut
              });
              var a = Math.abs(this.mouse.lerpedX - this.mouse.x) < 1 || Math.abs(this.mouse.lerpedY - this.mouse.y) < 1;
              l["e"].to(this.mouse, .4, {
                lerpedRadius: a ? 0 : 2.5
              }), this.mouse.firstPaint = !1
            }
          },
          pointerIn: function (e) {
            var t = e.currentTarget || e.target;
            this.mouse.trapped = t.dataset.trappedMode, this.mouse.trappedElement = t
          },
          pointerOut: function (e) {
            this.mouse.trapped = !1
          },
          routerLeave: function (e, t) {
            var a = this,
              n = L.dispatch("LOAD_PAGE").then(function () {
                a.$eventHub.$emit("dataloaded"), zs.setPageMeta()
              }),
              s = yr(e, this);
            Promise.all([n, s]).then(t)
          },
          routerEnter: function (e, t) {
            var a = this,
              n = new Promise(function (e) {
                e()
              });
            L.state.hasLoader && (n = L.dispatch("LOAD_PAGE"));
            var s = new p.a("Spectral", {
                weight: 700
              }),
              r = s.load();
            Promise.all([n, r]).then(function () {
              a.$eventHub.$emit("dataloaded"), zs.setPageMeta(), a.$nextTick(function () {
                br(e, a, L.state.hasLoader).then(function () {
                  L.commit("SET_IS_IN_TRANSITION", !1), t()
                }), L.commit("SET_HAS_LOADER", !1), setTimeout(function () {
                  return L.commit("SET_LOADER_MOUNTED")
                }, 400)
              })
            })
          },
          betweenNavigations: function () {
            this.forceScroll(0, !0)
          },
          filterLeave: function (e, t) {
            bt.get("filter-enter")(e).eventCallback("onComplete", t).play()
          }
        }
      },
      ii = ri,
      oi = (a("5c0b"), Object(Lt["a"])(ii, s, r, !1, null, null, null));
    oi.options.__file = "App.vue";
    var li = oi.exports,
      ci = a("9483");
    Object(ci["a"])("".concat("/", "service-worker.js"), {
      ready: function () {
        console.log("App is being served from cache by a service worker.\nFor more details, visit https://goo.gl/AFskqB")
      },
      cached: function () {
        console.log("Content has been cached for offline use.")
      },
      updated: function () {
        console.log("New content is available; please refresh.")
      },
      offline: function () {
        console.log("No internet connection found. App is running in offline mode.")
      },
      error: function (e) {
        console.error("Error during service worker registration:", e)
      }
    });
    var ui = {
        LOADER_HEADLINE: "<span>Interior Design</span><span>&nbsp;.&nbsp;</span><span>Progettazione Architettonica</span><span>&nbsp;.&nbsp;</span><span>Product Design</span>",
        FILTERS_LABEL: "Filtra <span>per.</span>",
        FILTERS_ALL: "All",
        FILTERS_RESIDENZE_PRIVATE: "Residenze Private",
        FILTERS_OSPITALITY_AND_RETAIL: "Hospitality & Retail",
        COMPANY_PRESENTATION: "Company Presentation",
        FORM_REQUEST_TYPE: "Scegli richiesta",
        FORM_NAME: "Nome e Cognome",
        FORM_AZIENDA: "Azienda",
        FORM_EMAIL: "Email",
        FORM_TEL: "Telefono",
        FORM_OGGETTO: "Oggetto",
        FORM_MESSAGE: "Messaggio",
        FORM_FILE: "Allega un file",
        FORM_SEND: "Invia Messaggio",
        FORM_SENDING: "Invio in corso",
        FORM_SENDED: "Inviato",
        FORM_THX: "Messaggio inviato, grazie per averci contattato.",
        SIGNUP_FOR_NEWSLETTER: "Iscriviti alla Newsletter",
        NEWSLETTER_THX: "Grazie per esserti iscritto alla newsletter!",
        CLICK_TO_SELECT: "Clicca per selezionare",
        GENERAL_INFORMATION: "Informazioni generali",
        COOKIE_MESSAGE: 'Questo sito utilizza i cookie. <a href="/static/privacy_policy.pdf" target="_blank">Scopri di piÃ¹</a>',
        PORTFOLIO: "Portfolio",
        STUDIO: "Studio",
        PRESS: "Press",
        CONTACTS: "Contatti"
      },
      di = ui,
      pi = {
        LOADER_HEADLINE: "<span>Interior Design</span><span>&nbsp;.&nbsp;</span><span>Architectural Design</span><span>&nbsp;.&nbsp;</span><span>Product Design</span>",
        FILTERS_LABEL: "Filter <span>by.</span>",
        FILTERS_ALL: "All",
        FILTERS_RESIDENZE_PRIVATE: "Private Residences",
        FILTERS_OSPITALITY_AND_RETAIL: "Hospitality & Retail",
        COMPANY_PRESENTATION: "Company Presentation",
        FORM_REQUEST_TYPE: "Choose Request",
        FORM_NAME: "Name and Surname",
        FORM_AZIENDA: "Company",
        FORM_EMAIL: "Email",
        FORM_TEL: "Telephone",
        FORM_OGGETTO: "Object",
        FORM_MESSAGE: "Message",
        FORM_FILE: "Attach a file",
        FORM_SEND: "Send Message",
        FORM_SENDING: "Sending",
        FORM_SENDED: "Sent",
        FORM_THX: "Message sent, thank for getting in touch.",
        SIGNUP_FOR_NEWSLETTER: "Subscribe to Newsletter",
        NEWSLETTER_THX: "Thanks for subscribing our newsletter!",
        CLICK_TO_SELECT: "Click to select",
        GENERAL_INFORMATION: "General Informations",
        COOKIE_MESSAGE: 'This website employs cookies. <a href="/static/privacy_policy.pdf" target="_blank">Discover more</a>',
        PORTFOLIO: "Portfolio",
        STUDIO: "Studio",
        PRESS: "Press",
        CONTACTS: "Contact"
      },
      hi = pi,
      mi = {},
      fi = mi,
      vi = function (e) {
        var t;
        switch (L.state.lang) {
          case "it":
            t = di[e];
            break;
          case "en":
            t = hi[e];
            break;
          case "ru":
            t = fi[e];
            break;
          default:
            t = "UNDEFINED_LABEL"
        }
        return t
      },
      _i = vi;
    n["a"].prototype.$translate = _i, n["a"].prototype.$eventHub = new n["a"], n["a"].prototype.$utils = zs, n["a"].config.productionTip = !1, A.init(), new n["a"]({
      router: Fs,
      store: L,
      render: function (e) {
        return e(li)
      }
    }).$mount("#app")
  },
  5802: function (e, t, a) {},
  "5b3b": function (e, t, a) {},
  "5b8b": function (e, t, a) {
    "use strict";
    var n = a("b63d"),
      s = a.n(n);
    s.a
  },
  "5ba6": function (e, t, a) {
    "use strict";
    var n = a("05f3"),
      s = a.n(n);
    s.a
  },
  "5c0b": function (e, t, a) {
    "use strict";
    var n = a("5e27"),
      s = a.n(n);
    s.a
  },
  "5e27": function (e, t, a) {},
  6204: function (e, t, a) {
    "use strict";
    var n = a("8b79"),
      s = a.n(n);
    s.a
  },
  "662f": function (e, t, a) {
    "use strict";
    var n = a("303c"),
      s = a.n(n);
    s.a
  },
  "6a64": function (e, t, a) {},
  "6aa5": function (e, t, a) {
    "use strict";
    var n = a("37af"),
      s = a.n(n);
    s.a
  },
  "6ac3": function (e, t, a) {
    "use strict";
    var n = a("234d"),
      s = a.n(n);
    s.a
  },
  "6dcd": function (e, t, a) {
    "use strict";
    var n = a("c653"),
      s = a.n(n);
    s.a
  },
  "73e2": function (e, t, a) {},
  "82d6": function (e, t, a) {
    "use strict";
    var n = a("12e8"),
      s = a.n(n);
    s.a
  },
  "85ad": function (e, t, a) {},
  "880e": function (e, t, a) {
    "use strict";
    var n = a("85ad"),
      s = a.n(n);
    s.a
  },
  "8b79": function (e, t, a) {},
  "8bb9": function (e, t, a) {},
  9262: function (e, t, a) {
    "use strict";
    var n = a("6a64"),
      s = a.n(n);
    s.a
  },
  "99a5": function (e, t, a) {},
  "9a1e": function (e, t, a) {
    "use strict";
    var n = a("73e2"),
      s = a.n(n);
    s.a
  },
  "9d60": function (e, t, a) {},
  "9e44": function (e, t, a) {
    "use strict";
    var n = a("0510"),
      s = a.n(n);
    s.a
  },
  a1f3: function (e, t, a) {},
  a838: function (e, t, a) {},
  ae89: function (e, t, a) {
    "use strict";
    var n = a("5802"),
      s = a.n(n);
    s.a
  },
  b63d: function (e, t, a) {},
  bc84: function (e, t, a) {
    "use strict";
    var n = a("0b3e"),
      s = a.n(n);
    s.a
  },
  c227: function (e, t, a) {},
  c2cb: function (e, t, a) {
    "use strict";
    var n = a("4e01"),
      s = a.n(n);
    s.a
  },
  c653: function (e, t, a) {},
  c68d: function (e, t, a) {},
  c9ea: function (e, t, a) {},
  cb02: function (e, t, a) {
    "use strict";
    var n = a("8bb9"),
      s = a.n(n);
    s.a
  },
  cf35: function (e, t, a) {},
  cf7c: function (e, t, a) {
    "use strict";
    var n = a("2470"),
      s = a.n(n);
    s.a
  },
  d038: function (e, t, a) {
    "use strict";
    var n = a("00c6"),
      s = a.n(n);
    s.a
  },
  d0bf: function (e, t, a) {
    "use strict";
    var n = a("dc6c"),
      s = a.n(n);
    s.a
  },
  d163: function (e, t, a) {
    "use strict";
    var n = a("0e49"),
      s = a.n(n);
    s.a
  },
  d1ed: function (e, t, a) {
    "use strict";
    var n = a("a1f3"),
      s = a.n(n);
    s.a
  },
  d2ef: function (e, t, a) {
    "use strict";
    var n = a("1fd6"),
      s = a.n(n);
    s.a
  },
  d778: function (e, t, a) {
    "use strict";
    var n = a("f95d"),
      s = a.n(n);
    s.a
  },
  da04: function (e, t, a) {
    "use strict";
    var n = a("ed16"),
      s = a.n(n);
    s.a
  },
  dc6c: function (e, t, a) {},
  ded6: function (e, t, a) {},
  e2d9: function (e, t, a) {},
  e55b: function (e, t, a) {
    "use strict";
    var n = a("c9ea"),
      s = a.n(n);
    s.a
  },
  e819: function (e, t, a) {
    "use strict";
    var n = a("e9ab"),
      s = a.n(n);
    s.a
  },
  e934: function (e, t, a) {
    "use strict";
    var n = a("eff8"),
      s = a.n(n);
    s.a
  },
  e9ab: function (e, t, a) {},
  ed16: function (e, t, a) {},
  eefd: function (e, t, a) {},
  eff8: function (e, t, a) {},
  f17f: function (e, t, a) {
    "use strict";
    var n = a("1a77"),
      s = a.n(n);
    s.a
  },
  f619: function (e, t, a) {
    "use strict";
    var n = a("ded6"),
      s = a.n(n);
    s.a
  },
  f6e5: function (e, t, a) {},
  f868: function (e, t, a) {},
  f95d: function (e, t, a) {}
});
//# sourceMappingURL=app.034b03db.js.map