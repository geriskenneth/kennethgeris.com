

export const state = () => ({
    animationScreen: true,
    n: 1
})
export const mutations = {
    endAnimationScreen (state, payload) {
        state.animationScreen = payload
    }
}
export const getters = {
    getN: state => () => state.n,
    getAnimationScreen: state =>() => state.animationScreen
  }
